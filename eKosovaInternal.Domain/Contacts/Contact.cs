﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Contacts
{
    public partial class Contact : BaseEntity
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public DateTime InsertionDate { get; set; }
        public string Answer { get; set; }
        public int IDInternalUser { get; set; }
    }
}
