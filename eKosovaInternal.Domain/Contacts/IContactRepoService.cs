﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.Contacts
{
    public interface IContactRepoService
    {
        void AddContact(Contact _);
        Task<Contact> GetContactById(int id);
        IEnumerable<Contact> GetAllContacts();
        IEnumerable<Contact> GetActiveContacts();

        Contact UpdateContact(Contact _);

    }
}
