﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Notifications
{
    public partial class NotificationQueue : BaseEntity
    {
        public string Message { get; set; }
        public string ToEmail { get; set; }
        public string ToPhone { get; set; }
        public bool IsSent { get; set; }
        public DateTime SentDate { get; set; }
        public bool HasError { get; set; }
        public string ErrorMessage { get; set; }
        public string Identification { get; set; }
        public bool IsRead { get; set; }
        public DateTime ReadDate { get; set; }
        public int IDNotificationQueueType { get; set; }
        public DateTime InsertionDate { get; set; }


    }
}
