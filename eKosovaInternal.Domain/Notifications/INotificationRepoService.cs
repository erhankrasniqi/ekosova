﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Notifications
{
    public interface INotificationRepoService
    {
        void AddNotificationQueue(NotificationQueue _);
    }
}
