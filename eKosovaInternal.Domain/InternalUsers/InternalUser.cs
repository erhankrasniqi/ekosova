﻿using eKosovaInternal.Domain.SeedWork;
using System.Collections.Generic;

namespace eKosovaInternal.Domain.InternalUsers
{
	public class InternalUser : BaseEntity
	{
		public string First { get; set; }
		public string Last { get; set; }
		public string Account { get; set; }
		public string PasswordHash { get; set; }
		public string PasswordSalt { get; set; }
		public System.DateTime InsertionDate { get; set; }
		public string PhoneNumber { get; set; }
		public string Email { get; set; }
		public string PersonalNumber { get; set; }
		public string AccessToken { get; set; }
		public string RefreshToken { get; set; }
		public bool IsActive { get; set; }
		public int IDRole { get; set; }
		public int IDHierarchy { get; set; }
		public int IDUserAuthorizationType { get; set; }
		public int IDActiveDirectoryDomain { get; set; }
		public string Identification { get; set; }
		public virtual UserAuthorizationType IdUserAuthorizationTypeNavigation { get; set; }
		public virtual IEnumerable<UserAuthorization> UserAuthorizations { get; set; }
	}
}
