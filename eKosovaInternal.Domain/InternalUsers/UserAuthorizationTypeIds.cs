﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.InternalUsers
{
    public enum UserAuthorizationTypeIds
    {
        SystemAccount = 1,
        ActiveDirectoryAccount = 2
    }
}
