﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.InternalUsers
{
    public enum UserAuthorizationStatus
    {
        SignIn = 1,
        SignOut = 2
    }
}
