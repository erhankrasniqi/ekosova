﻿namespace eKosovaInternal.Domain.InternalUsers
{
	public class InternalUserCreate
	{
		public string First { get; set; }
		public string Last { get; set; }
		public string Account { get; set; }
		public string PhoneNumber { get; set; }
		public string Password { get; set; }
		public int IDRole { get; set; }
		public int IDHierarchy { get; set; }
		public int IDUserAuthorizationType { get; set; }
		public int IDActiveDirectoryDomain { get; set; }
	}
}
