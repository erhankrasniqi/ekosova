﻿using eKosovaInternal.Domain.Modules;
using eKosovaInternal.Domain.Roles;
using eKosovaInternal.Domain.SeedWork;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace eKosovaInternal.Domain.InternalUsers
{
    public partial class UserAuthorization : DeleteClass, IEntryEntity
    {
        public int IDInternalUser { get; set; }
        public int IdRoleAuthorizationType { get; set; }
        public int IDModule { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDate { get; set; }
        public int IdEntryUser { get; set; }
		public int IDEntryUser { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
		public DateTime? UpdateDate { get; set; }
        public int? IdUpdateUser { get; set; }
        public string UpdateUser { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual InternalUser IDInternalUserNavigation { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual RoleAuthorizationType IdRoleAuthorizationTypeNavigation { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Module IdModuleNavigation { get; set; }

    }
}
