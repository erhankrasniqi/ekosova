﻿using eKosovaInternal.Domain.Hierarchies;
using System.Collections.Generic;

namespace eKosovaInternal.Domain.InternalUsers
{
	public class InternalUserIntro
	{
		public IEnumerable<Roles.RoleInternalRegister> ViewRoles { get; set; }
		public IEnumerable<ADDomains.ADDomainViewModel> ViewADDomains { get; set; }
		public IEnumerable<UserAuthorizationType> ViewAuthorizationTypes { get; set; }
		public IEnumerable<HierarchyViewModel> ViewHierarchy { get; set; }
	}
}
