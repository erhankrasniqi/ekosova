﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.InternalUsers
{
    public class UserAuthorizationType : BaseEntity, IEntryEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime EntryDate { get; set; }
        public int IDEntryUser { get; set; }
		public string EntryUser { get; set; }

        public virtual ICollection<InternalUser> InternalUsers { get; set; }
    }
}
