﻿using System.Collections.Generic;
using eKosovaInternal.Domain.SeedWork;
using System.Threading.Tasks;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Domain.ADDomains;

namespace eKosovaInternal.Domain.InternalUsers
{
	public interface IInternalUserRepoService
	{
		Task<InternalUser> Add(InternalUser _);
		void Update(InternalUser _);
		Task<InternalUser> GetById(int id);
		Task<InternalUser> GetByPersonalNumber(string personalNumber);
		Task<InternalUser> GetByAccount(string personalNumber);
		Task<InternalUser> GetByIdentification(string identification);
		IList<InternalUser> GetAll();
		IEnumerable<InternalUser> GetActive();
		//IEnumerable<UserAuthorization> GetUserAuthorizations(int idUser);
	}
}
