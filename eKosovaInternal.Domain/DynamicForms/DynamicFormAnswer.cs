﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.DynamicForms
{
    public class DynamicFormAnswer : BaseEntity
    {
        public int IDDynamicFormQuestion { get; set; }
        public int IDDynamicFormResult { get; set; }
        public int IDGrid_XY { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string Answer { get; set; }
    }
}
