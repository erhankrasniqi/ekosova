﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.DynamicForms
{
    public class DynamicFormGridXY : BaseEntity
    {
        public int IDGrid_X_Questions { get; set; }
        public int IDGrid_Y_Questions { get; set; }
        public int IDDynamicFormQuestions { get; set; }
    }
}
