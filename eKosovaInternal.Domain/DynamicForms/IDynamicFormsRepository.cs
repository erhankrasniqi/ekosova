﻿using eKosova.Domain.DynamicForms;
using eKosovaInternal.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.DynamicForms
{
    public interface IDynamicFormsRepository
    {
        Task<int> AddDynamicForm(DynamicForm model);
        Task<int> AddQuestion(DynamicFormQuestions model);
        void AddQuestionOptions(DynamicFormQuestionsOptions model);
        Task<int> AddDynamicFormQuestionGridX(DynamicFormGridXQuestions model);
        Task<int> AddDynamicFormQuestionGridY(DynamicFormGridYQuestions model);
        void AddDynamicFormGridXY(DynamicFormGridXY model);
        Task<IEnumerable<DynamicForm>> GetDynamicForms();
    }
}
