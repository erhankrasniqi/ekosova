﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.DynamicForms
{
    public class DynamicFormResults : BaseEntity
    {
        public int IDDynamicFormQuestions { get; set; }
        public DateTime InsertionDate { get; set; }
        public bool IsDone { get; set; }
        public DateTime DoneDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
