﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.DynamicForms
{
    public class DynamicForm : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime InsertionDate { get; set; }
        public int IDInternalUser { get; set; }
        public int IDServiceCategory { get; set; }
    }
}
