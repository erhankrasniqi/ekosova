﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Hierarchies
{
	public class HierarchyViewModel : BaseEntity
	{
		public int IDParentHierarchy { get; set; }
		public string Title { get; set; }
	}
}
