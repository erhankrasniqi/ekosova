﻿using eKosovaInternal.Domain.SeedWork;
using System;

namespace eKosovaInternal.Domain.Hierarchies
{
	public class Hierarchy : DeleteClass
	{
		public int IDParentHierarchy { get; set; }
		public string Title { get; set; }
		public DateTime EntryDate { get; set; }
		public int IDEntryUser { get; set; }
		public string EntryUser { get; set; }
		public DateTime UpdateDate { get; set; }
		public int IDUpdateUser { get; set; }
		public string UpdateUser { get; set; }
	}
}
