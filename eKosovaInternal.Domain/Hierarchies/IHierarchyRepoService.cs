﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.Hierarchies
{
	public interface IHierarchyRepoService
	{
		IList<Hierarchy> GetAll();
	}
}
