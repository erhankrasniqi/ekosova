﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.Emails
{
    public interface IEmailSender
    {
        Task SendEmailAsync(MailRequest emailInfo);

    }
}
