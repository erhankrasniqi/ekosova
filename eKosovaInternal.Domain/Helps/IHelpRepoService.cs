﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Helps
{
    public interface IHelpRepoService
    {
        void AddHelp(Help _);
        Help UpdateHelp(Help _);
        Help GetHelpById(int id);
        IEnumerable<Help> GetAllHelps();
        IEnumerable<Help> GetActiveHelps();
    }
}
