﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.Faqs
{
   public interface IFaqRepoService
    {
        void AddFaq(Faq _);
        void UpdateFaq(UpdateFaqModel _);
        void DeleteFaq(int id);
        Faq GetFaqByID(int id);
        IEnumerable<Faq> GetAllFaqs();
        IEnumerable<Faq> GetActiveFaqs();

    }
}
