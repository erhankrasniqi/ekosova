﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Faqs
{
   public class UpdateFaqModel
    {
        public int ID { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int InternalUser { get; set; }
    }
}
