﻿using eKosovaInternal.Domain.SeedWork;

namespace eKosovaInternal.Domain.Roles
{
	public class RoleInternalRegister : BaseEntity
	{
		public string Title { get; set; }
	}
}
