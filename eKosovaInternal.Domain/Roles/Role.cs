﻿using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Roles
{
    public partial class Role : DeleteClass
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime EntryDate { get; set; }
        public int IdEntryUser { get; set; }
        public string EntryUser { get; set; }
        public bool IsActive { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? IdUpdateUser { get; set; }
        public string UpdateUser { get; set; }
        public bool WithPasswordPolicy { get; set; }
        public int? PasswordValidityDays { get; set; }
        public virtual IEnumerable<RoleAuthorization> RoleAuthorizations { get; set; }
        public virtual IEnumerable<PublicUser> Users { get; set; }
    }
}
