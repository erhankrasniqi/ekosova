﻿using eKosovaInternal.Domain.InternalUsers;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace eKosovaInternal.Domain.Roles
{
    public interface IRoleRepository
    {
        IEnumerable<Role> GetRoles();
        IEnumerable<Role> GetRolesWithCriteria(Func<Role, bool> criteria);
        Role GetRole(int id);


        //RoleAuthorizationType
        IEnumerable<RoleAuthorizationType> GetRoleAuthTypes();
        RoleAuthorizationType GetRoleAuthTypeByRoleAndModule(int idRole, int idModule);


        //RoleAuthorization
        void AddRoleAuthorization(RoleAuthorization _);
        void AddRangeRoleAuthorization(List<RoleAuthorization> _);
        RoleAuthorization GetRoleAuthorization(int id);
        IEnumerable<RoleAuthorization> GetRoleAuthorizationByRole(int idRole);
        IEnumerable<UserAuthorization> GetRoleAuthorizationByUser(int idUser);
    }
}
