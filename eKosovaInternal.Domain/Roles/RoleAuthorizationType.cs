﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Roles
{
    public partial class RoleAuthorizationType : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime EntryDate { get; set; }
        public int IdEntryUser { get; set; }
        public string EntryUser { get; set; }

        public virtual IEnumerable<RoleAuthorization> RoleAuthorizations { get; set; }
        //public virtual IEnumerable<UserAuthorization> UserAuthorizations { get; set; }
    }
}
