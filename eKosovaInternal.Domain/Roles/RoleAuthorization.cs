﻿using eKosovaInternal.Domain.Modules;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Roles
{
    public partial class RoleAuthorization : BaseEntity
    {
        public int IDRole { get; set; }
        public int IDRoleAuthorizationType { get; set; }
        public int IDModule { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDate { get; set; }
        public int IDEntryUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? IDUpdateUser { get; set; }
        public string UpdateUser { get; set; }
        public virtual Module IDModuleNavigation { get; set; }
        public virtual RoleAuthorizationType IDRoleAuthorizationTypeNavigation { get; set; }
        public virtual Role IDRoleNavigation { get; set; }
    }
}
