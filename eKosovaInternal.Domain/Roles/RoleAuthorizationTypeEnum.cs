﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Roles
{
    public enum RoleAuthorizationTypeEnum
    {
        FullAccess = 1,
        ReadAccess = 2,
        NoAccess = 3,
    }
}
