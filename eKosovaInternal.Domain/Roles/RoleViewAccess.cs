﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Roles
{
    public class RoleViewAccess
    {
        public int IdModule { get; set; }
        public int IdRoleAuthorizationType { get; set; }
    }
}
