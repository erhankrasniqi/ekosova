﻿using eKosovaInternal.Domain.SeedWork;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain
{
	public class Base64Images
	{
		public string FrontID { get; set; }
		public string BackID { get; set; }
		public string Selfie { get; set; }
		public string BizDokument { get; set; }
	}
}
