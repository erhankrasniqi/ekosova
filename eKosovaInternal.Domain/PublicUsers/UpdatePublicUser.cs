﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.PublicUsers
{
    public class UpdatePublicUser
    {
        public int ID { get; set; }
        public bool IDFront { get; set; }
        public bool IDBack { get; set; }
        public bool IDSelfie { get; set; }
        public string Reason { get; set; }
    }
}
