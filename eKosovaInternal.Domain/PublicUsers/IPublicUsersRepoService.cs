﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.PublicUsers
{
    public interface IPublicUsersRepoService
    {
        void AddPublicUser(PublicUser _);
        void UpdatePublicUser(UpdatePublicUser _);

        Task<PublicUser> GetPublicUserById(int id);
        
        IEnumerable<PublicUser> GetAllPublicUser();
        IEnumerable<PublicUser> GetActivePublicUser();
        Task<PublicUser> GetPublicUserByPersonalNumber(string identification);
    }
}
