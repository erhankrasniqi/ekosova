﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.UserAuthorizations
{
    public enum UserAuthorizationStatus
    {
        SignIn = 1,
        SignOut = 2
    }
}
