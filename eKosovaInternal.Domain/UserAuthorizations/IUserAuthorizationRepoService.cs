﻿using eKosovaInternal.Domain.InternalUsers;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.UserAuthorizations
{
	public interface IUserAuthorizationRepoService
	{
		IList<UserAuthorizationType> GetAll();
	}
}
