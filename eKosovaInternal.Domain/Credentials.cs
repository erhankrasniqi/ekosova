﻿using FluentValidation;

namespace eKosovaInternal.Domain
{
	public class Credentials
	{
		public string Account { get; set; }
		public string Password { get; set; }
	}
	public class CredentialsValidator : AbstractValidator<Credentials>
	{
		public CredentialsValidator()
		{
			RuleFor(x => x.Account).NotNull().NotEmpty();//.Length(10);
			RuleFor(x => x.Password).NotNull().NotEmpty();
		}
	}
}
