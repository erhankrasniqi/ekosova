﻿using eKosova.Domain.DynamicForms;
using eKosovaInternal.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.DynamicForms
{
    public interface IServicesRepository
    {
        IEnumerable<ServiceCategory> GetServiceCategories();
    }
}
