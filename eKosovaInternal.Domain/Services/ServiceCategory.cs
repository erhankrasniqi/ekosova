﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Services
{
    public class ServiceCategory : BaseEntity
    {
        public string Name { get; set; }
        public DateTime InsertionDate { get; set; }
        public string IconPath { get; set; }
        public int IDInternalUser { get; set; }
    }
}
