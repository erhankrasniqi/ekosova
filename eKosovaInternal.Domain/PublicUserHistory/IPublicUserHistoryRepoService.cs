﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.PublicUserHistory
{
   public interface IPublicUserHistoryRepoService
    {
        IEnumerable<PublicUserHistory> GetAllPublicUserHistory(int publicUserID);
    }
}
