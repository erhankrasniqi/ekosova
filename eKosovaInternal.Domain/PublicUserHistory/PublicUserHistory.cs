﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.PublicUserHistory
{
  public  class PublicUserHistory : BaseEntity
    {
        public DateTime InsertionDate  { get; set; } 
        public string Reason { get; set; }
        public string Status { get; set; }
    }
}
