﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Medias
{
    public interface IMediaRepoService
    {
        void AddMedia(Media _);
        void UpdateMedia(Media _);
        Media GetMediaById(int id);
        IEnumerable<Media> GetAllMedias();
        IEnumerable<Media> GetActiveMedias();



        
        
    }
}
