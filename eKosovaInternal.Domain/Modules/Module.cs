﻿using eKosovaInternal.Domain.Roles;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Modules
{
    public partial class Module : BaseEntity, IEntryEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime EntryDate { get; set; }
        public int IDEntryUser { get; set; }
		public string EntryUser { get; set; }
        public int? IDParent { get; set; }
        public bool? IsPublic { get; set; }
        public virtual IEnumerable<RoleAuthorization> RoleAuthorizations { get; set; }
    }
}
