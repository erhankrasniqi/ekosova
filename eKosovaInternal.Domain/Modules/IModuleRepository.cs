﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Modules
{
    public interface IModuleRepository
    {
        IEnumerable<Module> GetModules();
        //IEnumerable<Module> GetModulesWithAuthorization();
        //IEnumerable<Module> GetModulesWithUserAuthorization();
        IEnumerable<Module> GetModules(Func<Module, bool> criteria);
    }
}
