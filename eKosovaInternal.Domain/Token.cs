﻿namespace eKosovaInternal.Domain
{
	public class Token
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string SigningKey { get; set; }
        public int ValidTimeInMinutes { get; set; }
    }
}
