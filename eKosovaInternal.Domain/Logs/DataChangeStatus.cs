﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Logs
{
    public enum DataChangeStatus
    {
        Edit = 1,
        Delete = 2,
        Insert = 3
    }
}
