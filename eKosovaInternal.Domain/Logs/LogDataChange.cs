﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Logs
{
    public partial class LogDataChange : BaseEntity
    {
        public DateTime EntryDate { get; set; }
        public int IDEntryUser { get; set; }
        public string EntryUser { get; set; }
        public byte[] Before { get; set; }
        public byte[] After { get; set; }
        public string ComputerName { get; set; }
        public string IPAddress { get; set; }
        public int IDLogBrowserType { get; set; }
        public int IDLogOperatingSystemType { get; set; }
        public bool IsMobileDevice { get; set; }
        public int IDLogDataChangeStatus { get; set; }
        public int IDTable { get; set; }
        public virtual LogInternetBrowserType IDLogBrowserTypeNavigation { get; set; }
        public virtual LogDataChangeStatus IDLogDataChangeStatusNavigation { get; set; }
        public virtual LogOperatingSystemType IDLogOperatingSystemTypeNavigation { get; set; }
        public virtual Tables IDtableNavigation { get; set; }
    }
}
