﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Logs
{
    public enum InternetBrowserType
    {
        Chrome = 1,
        Edge = 2,
        Explorer = 3,
        Other = 4
    }
}
