﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Logs
{
    public partial class LogUserAuthorizationStatus : BaseEntity, IEntryEntity
    {
        public LogUserAuthorizationStatus()
        {
            LogUserAuthorizations = new HashSet<LogUserAuthorization>();
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime EntryDate { get; set; }
        public int IDEntryUser { get; set; }
        public string EntryUser { get; set; }

        public virtual ICollection<LogUserAuthorization> LogUserAuthorizations { get; set; }
    }
}
