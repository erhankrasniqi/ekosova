﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Logs
{
    public interface ILogRepository
    {
        void AddLogUserAuthorization(LogUserAuthorization _);
        IEnumerable<LogUserAuthorization> GetLogUserAuthorizationsByIdUser(int idUser);
        IEnumerable<LogUserAuthorization> GetLogUserAuthorizations();
        void AddLogFailedAuthentication(LogFailedAuthentication _);
        IEnumerable<LogFailedAuthentication> GetLogFailedAuthentications(DateTime? entryDate);
        IEnumerable<LogFailedAuthentication> GetLogFailedAuthenticationsAll();
        void AddLogDataChange(LogDataChange _);
        IEnumerable<LogDataChange> GetLogDataChanges(Func<LogDataChange, bool> criteria);
        void AddLogUserActivity(LogUserActivity _);
        IEnumerable<LogUserActivity> GetLogUserActivitiesByIdUser(int idUser);
        IEnumerable<LogUserActivity> GetLogUserActivities();
        int GetTableByName(string TableName);
        IEnumerable<Tables> GetTables();
        IEnumerable<LogUserAuthorizationStatus> GetLogUserAuthorizationStatus();
        IEnumerable<LogUserActivityStatus> GetLogUserActivityStatus();
    }
}
