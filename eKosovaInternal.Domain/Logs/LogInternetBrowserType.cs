﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Logs
{
    public partial class LogInternetBrowserType : IEntryEntity
    {
        public LogInternetBrowserType()
        {
            LogDataChanges = new HashSet<LogDataChange>();
            LogFailedAuthentications = new HashSet<LogFailedAuthentication>();
            LogUserActivities = new HashSet<LogUserActivity>();
            LogUserAuthorizations = new HashSet<LogUserAuthorization>();
        }

        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime EntryDate { get; set; }
        public int IDEntryUser { get; set; }
        public string EntryUser { get; set; }

        public virtual ICollection<LogDataChange> LogDataChanges { get; set; }
        public virtual ICollection<LogFailedAuthentication> LogFailedAuthentications { get; set; }
        public virtual ICollection<LogUserActivity> LogUserActivities { get; set; }
        public virtual ICollection<LogUserAuthorization> LogUserAuthorizations { get; set; }
    }
}
