﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Logs
{
    public enum OperatingSystemType
    {
        Windows = 1,
        Android = 2,
        IOS = 3,
        MacOS = 4,
        Other = 5
    }
}
