﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Logs
{
    public class Tables : BaseEntity
    {
        public Tables()
        {
            LogDataChange = new HashSet<LogDataChange>();
        }

        public string Title { get; set; }

        public virtual ICollection<LogDataChange> LogDataChange { get; set; }
    }
}
