﻿using System;

namespace eKosovaInternal.Domain
{
	public class LoginResponse
	{
		public string Identification { get; set; }
		public string Token { get; set; }
		public string RefreshToken { get; set; }
		public string First { get; set; }
		public string Last { get; set; }
		public int Role { get; set; }
		public string Username { get; set; }
		public int ValidTokenTimeInMinutes { get; set; }
		public DateTime ValidDateTimeToken { get; set; }
	}
}
