﻿using eKosovaInternal.Domain.PublicUsers;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.SeedWork
{
	public interface IRepository<T> where T : BaseEntity
	{
		Task<T> Create(T model, string query, object param);
		T Add(string query, object param);
		void Delete(T model);
		int DeleteId(int id);
		IEnumerable<T> GetAll();
		IEnumerable<T> GetAll(string query);
		IEnumerable<T> GetAll(string query, object param);
		T ReadById(string query, object parameters);
		Task<IEnumerable<T>> GetAllQuery(string query);
		Task<T> Read(string query, object param);
		T Update(string query, object param);
		List<T> ListByCriteria(Func<T, bool> criteria, params string[] includes);

		T GetSingleByCriteria(Func<T, bool> criteria);
		T GetSingleByCriteria(Func<T, bool> criteria, params string[] includes);
		bool Any(Func<T, bool> criteria);
		int CountByCriteria(Func<T, bool> criteria);
	}
}
