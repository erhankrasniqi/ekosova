﻿using System.ComponentModel.DataAnnotations;

namespace eKosovaInternal.Domain.SeedWork
{
	public class BaseIdentification
	{
		[Required]
		public string Identification { get; set; }
	}
}
