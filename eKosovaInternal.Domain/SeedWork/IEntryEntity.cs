﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.SeedWork
{
    public interface IEntryEntity
    {
        int IDEntryUser { get; set; }
        string EntryUser { get; set; }
        DateTime EntryDate { get; set; }
    }
}
