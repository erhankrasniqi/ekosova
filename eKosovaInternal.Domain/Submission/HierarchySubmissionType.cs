﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Submission
{
    public class HierarchySubmissionType : BaseEntity
    {
        public int IDHierarchy { get; set; }
        public string Title { get; set; }
    }
}
