﻿using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Domain.Submission
{
    public class InsertSubmission  : BaseEntity
    {
        public string Title { get; set; }
        public string Subject { get; set; }
        public int HierarchyCategory { get; set; }
        public string Content { get; set; }
        public int PersonalNumber { get; set; }
    }
}
