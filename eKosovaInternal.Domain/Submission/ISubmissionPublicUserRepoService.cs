﻿using eKosovaInternal.Domain.Submission;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.Edergesa
{
    public interface ISubmissionPublicUserRepoService
    {
        IEnumerable<SubmissionPublicUser> GetPublicUserByPersonalNumber(int personalNumber);
        IEnumerable<HierarchySubmissionType> GetHierarchyList();

        void InserSubmission(InsertSubmission _);
        void InserSubmissionDocument(InsertSubmissionDocument _);



    }
}
