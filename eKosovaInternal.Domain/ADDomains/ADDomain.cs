﻿using eKosovaInternal.Domain.SeedWork;
using System;

namespace eKosovaInternal.Domain.ADDomains
{
	public class ADDomain : BaseEntity
	{
		public string Title { get; set; }
		public string Description { get; set; }
		public DateTime EntryDate { get; set; }
		public int IDEntryUser { get; set; }
		public string EntryUser { get; set; }
		public bool IsActive { get; set; }
		public DateTime? UpdateDate { get; set; }
		public int? IDUpdateUser { get; set; }
		public string UpdateUser { get; set; }
		public bool IsDeleted { get; set; }
		public DateTime? DeleteDate { get; set; }
		public int? IDDeleteUser { get; set; }
	}
}
