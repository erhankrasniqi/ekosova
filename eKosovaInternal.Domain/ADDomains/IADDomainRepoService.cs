﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternal.Domain.ADDomains
{
	public interface IADDomainRepoService
	{
		Task<ADDomain> GetADDomain(int id);
		Task<IList<ADDomain>> GetAllDomains();
	}
}
