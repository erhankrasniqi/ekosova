#pragma checksum "F:\Team Projects\eKosova\eKosovaInternal\Views\Internal\Test.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1ea9dffffdc9bb101cf49d95283b02e9b1576e3a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Internal_Test), @"mvc.1.0.view", @"/Views/Internal/Test.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "F:\Team Projects\eKosova\eKosovaInternal\Views\_ViewImports.cshtml"
using eKosovaInternal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "F:\Team Projects\eKosova\eKosovaInternal\Views\_ViewImports.cshtml"
using eKosovaInternal.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1ea9dffffdc9bb101cf49d95283b02e9b1576e3a", @"/Views/Internal/Test.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"539606e52241c9d74c32d1bc06a5f93020a855ca", @"/Views/_ViewImports.cshtml")]
    public class Views_Internal_Test : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "F:\Team Projects\eKosova\eKosovaInternal\Views\Internal\Test.cshtml"
  
	Layout = "~/Views/Shared/_AdministrationLayout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div id=""example"">
	<div class=""demo-section"">
		<div class=""settings-head""></div>
		<ul>
			<li>Notifications <input type=""checkbox"" id=""notifications-switch"" aria-label=""Notifications Switch"" checked=""checked"" /></li>
			<li>Send notifications <input id=""mail-switch"" aria-label=""Mail Switch"" /></li>
			<li>Always visible <input id=""visible-switch"" aria-label=""Visible Switch"" /></li>
			<li>Display real name <input id=""name-switch"" aria-label=""Name Switch"" /></li>
		</ul>
	</div>

	<style>
		.demo-section ul {
			margin: 0;
			padding: 0;
		}

			.demo-section ul li {
				list-style-type: none;
				margin: 0;
				padding: 10px 10px 10px 20px;
				min-height: 28px;
				line-height: 28px;
				vertical-align: middle;
				border-top: 1px solid rgba(128,128,128,.5);
			}

		.demo-section {
			min-width: 220px;
			margin-top: 50px;
			padding: 0;
		}

			.demo-section ul li .k-switch {
				float: right;
			}

		.settings-head {
			height: 66px;
			background: url('../conten");
            WriteLiteral(@"t/web/switch/settings-title.png') no-repeat 20px 50% #2db245;
		}
	</style>
</div>

<script>
	$(function () {
		$(""#notifications-switch"").kendoSwitch();

		$(""#mail-switch"").kendoSwitch({
			messages: {
				checked: ""YES"",
				unchecked: ""NO""
			}
		});

		$(""#visible-switch"").kendoSwitch({
			checked: true
		});

		$(""#name-switch"").kendoSwitch();
	});
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
