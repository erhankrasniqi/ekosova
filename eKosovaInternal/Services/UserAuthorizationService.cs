﻿using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
	public class UserAuthorizationService : IUserAuthorizationService
	{
		public async Task<IEnumerable<UserAuthorizationType>> GetAuthorizations()
		{
			try
			{
				var result = await ApiRequestHelper<UserAuthorizationType>.Get("api/internal/getAuthorizationTypes");
				if (result.Status == 0)
					return result.Data;
				else
					throw new Exception("noData");
			}
			catch
			{
				return null;
			}
		}
	}
}
