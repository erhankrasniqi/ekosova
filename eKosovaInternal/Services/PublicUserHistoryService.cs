﻿using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.PublicUserHistory;
using eKosovaInternal.Interfaces;
using eKosovaInternal.SharedModel.PublicUserHistory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
   public class PublicUserHistoryService : IPublicUserHistoryService
    {
        public async Task<IEnumerable<PublicUserHistoryModel>> GetPublicUserHistory(int publicUserID)
        {
            try
            {
                var url = $"api/PublicUserHistory/getPublicUsersHistory/{publicUserID}";
                var response = await ApiRequestHelper<PublicUserHistoryModel>.Get(url);
                return response.Data;
            }
            catch (Exception e)
            {
                throw;
            }
           
        }
    }
}
