﻿using eKosovaInternal.Business;
using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Interfaces;
using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.Faqs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
	public class FaqService : IFaqService
	{
		public async Task<IEnumerable<FaqModel>> GetFaqs()
		{
			try
			{
				var url = "api/Faq/getFaqs/";
				var response = await ApiRequestHelper<FaqModel>.Get(url);
				return response.Data;
			}
			catch (Exception e)
			{
				throw;
			}
		}

		public async Task<ApiResponse<FaqModel>> CreateFaq(CreateFaqModel _)
		{
			try
			{
				var url = "api/Faq/create";
				var content = JsonConvert.SerializeObject(_);
				var response = await ApiRequestHelper<FaqModel>.Post(url, content);
				return response;
			}
			catch (Exception e)
			{
				throw;
			}
		}

		public async Task<Response<bool>> DeleteFaq(int id)
		{
			try
			{
				var url = "api/Faq/delete/";
				var param = JsonConvert.SerializeObject(new BaseEntity { ID = id });
				await ApiRequestHelper<FaqModel>.Post(url, param);
				return new Response<bool>(PublicResultStatusCodes.Done, true);

			}
			catch (Exception e)
			{
				return new Response<bool>(PublicResultStatusCodes.QueryHasError);
			}
		}

		//public Task<ApiResponse<FaqModel>> EditFaq(EditFaqModel _)
		//{
		//    try
		//    {
		//        var url = "api/Faq/edit/";
		//        var param = JsonConvert.SerializeObject(new BaseEntity { ID = id });
		//        await ApiRequestHelper<FaqModel>.Put(url, param);
		//        return new Response<bool>(PublicResultStatusCodes.Done, true);
		//    }
		//    catch (Exception e)
		//    {
		//        return new Response<bool>(PublicResultStatusCodes.QueryHasError);
		//    }
		//}

		public async Task<ApiResponse<FaqModel>> EditFaq(EditFaqModel _)
		{
			var contactResult = await ApiRequestHelper<FaqModel>.Post("api/Faq/edit", JsonConvert.SerializeObject(_));
			return contactResult;
		}
	}

}
