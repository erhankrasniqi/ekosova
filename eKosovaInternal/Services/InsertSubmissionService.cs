﻿using AutoMapper;
using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.Edergesa;
using eKosovaInternal.Interfaces;
using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.Edergesa;
using eKosovaInternal.SharedModel.Submission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
    public class InsertSubmissionService : IInsertSubmissionService
    { 

        public async Task<ApiResponse<SubmissionPersonalUserModel>> GetSubmissionByPersonalnumber(int personalNumber)
        {
            try
            {
                var url = $"api/Submission/getSubmissionUser/{personalNumber}";
                var response = await ApiRequestHelper<SubmissionPersonalUserModel>.Get(url);
                return response;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
