﻿using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.ADDomains;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Interfaces;
using eKosovaInternal.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
	public class InternalUserService : IInternalUserService
	{
		public async Task<IEnumerable<InternalUserViewModel>> GetAll()
		{
			try
			{
				var result = await ApiRequestHelper<InternalUserViewModel>.Get("api/internal/getAll");
				if (result.Status == 0)
					return result.Data;
				else
					throw new System.Exception("noData");
			}
			catch
			{
				return null;
			}
		}

		public async Task<IEnumerable<ADDomainViewModel>> GetDomains()
		{
			try
			{
				var result = await ApiRequestHelper<ADDomainViewModel>.Get("api/internal/getDomains");
				if (result.Status == 0)
					return result.Data;
				else
					throw new System.Exception("noData");
			}
			catch
			{
				return null;
			}
		}

		public async Task<ApiResponse<InternalUserViewModel>> Create(InternalUserCreate _)
		{
			var url = "api/internal/create";
			var param = JsonConvert.SerializeObject(_);
			return await ApiRequestHelper<InternalUserViewModel>.Post(url, param);
		}

		public async Task<ApiResponse<bool>> Delete(int id)
		{
			var url = "api/internal/delete";
			var param = JsonConvert.SerializeObject(new BaseEntity { ID = id });
			return await ApiRequestHelper<bool>.Post(url, param);
		}

		public async Task<ApiResponse<bool>> Update(InternalUser _)
		=> await ApiRequestHelper<bool>.Post("api/internal/update", JsonConvert.SerializeObject(_));

		public async Task<ApiResponse<bool>> Update(InternalUserViewModel _)
		{
			return await ApiRequestHelper<bool>.Post("api/internal/updateAdmin", JsonConvert.SerializeObject(_));
		}

		public async Task<ApiResponse<InternalUser>> Read(string identification)
		{
			var url = "api/internal/read";
			var param = JsonConvert.SerializeObject(new BaseIdentification { Identification = identification });
			return await ApiRequestHelper<InternalUser>.Post(url, param);
		}

		public async Task<ApiResponse<InternalUser>> Read(int ID)
		{
			var url = "api/internal/readID";
			var param = JsonConvert.SerializeObject(new BaseEntity { ID = ID });
			return await ApiRequestHelper<InternalUser>.Post(url, param);
		}

		public async Task<ApiResponse<bool>> CheckAccount(string username)
		{
			var url = "api/internal/check";
			var param = JsonConvert.SerializeObject(new BaseString { Value = username });
			var result = await ApiRequestHelper<InternalUserViewModel>.Post(url, param);
			if (result.Status == 0)
			{
				var data = result.Data.FirstOrDefault();
				var exists = data != null;
				return new ApiResponse<bool> { Status = 0, SingleData = exists };
			}
			else
			{
				return new ApiResponse<bool> { Status = 19, SingleData = false };
			}
		}

		//public async Task<IEnumerable<UserAuthorizationType>> GetAuthorizationTypes()
		//{
		//	try
		//	{
		//		var url = "api/internal/authorizationTypes";
		//		var result = await ApiRequestHelper<UserAuthorizationType>.Get(url);
		//		if (result.Status == 0)
		//		{
		//			return result.Data;
		//		}
		//		else
		//			throw new System.Exception("noData");
		//	}
		//	catch (System.Exception e)
		//	{
		//		return null;
		//	}
		//}
	}
}
