﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Custom.Models;
using eKosovaInternal.Domain.Logs;
using eKosovaInternal.Interfaces;
using eKosovaInternal.KendoFilter;
using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.General;
using eKosovaInternal.SharedModel.Logs;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
	public class LogService : ILogService
	{
		private readonly IMapper _mapper;
		private readonly IHttpContextAccessor _contextAccessor;
		public LogService(IMapper mapper,
			   IHttpContextAccessor contextAccessor)
		{
			_mapper = mapper;
			_contextAccessor = contextAccessor;
		}
		public async Task<ApiResponse<Pagination<LogDataChange>>> GetDataChanges(DataSourceFilter data)
		{

			//var url = "Log/dataChanges?IdUser=" + LogedUserHelper.GetLoggedUserId();
			var url = "api/Log/dataChanges";
			var list = new List<KeyValuePair<string, string>>();
			list.Add(new KeyValuePair<string, string>("PageSize", data.PageSize.ToString()));
			list.Add(new KeyValuePair<string, string>("Page", data.Page.ToString()));

			if (data.Filter != null)
			{
				if (data.Filter.Filters != null)
				{
					var filters = data.Filter.Filters;
					foreach (var item in filters)
					{
						switch (item.Field)
						{
							case "entryDate":
								list.Add(new KeyValuePair<string, string>("EntryDate", DateTime.ParseExact(item.Value, "dd.MM.yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
								break;
							case "entryUser":
								list.Add(new KeyValuePair<string, string>("EntryUser", item.Value));
								break;
							case "computerName":
								list.Add(new KeyValuePair<string, string>("ComputerName", item.Value));
								break;
							case "ipAddress":
								list.Add(new KeyValuePair<string, string>("IpAddress", item.Value));
								break;
							case "isMobileDevice":
								list.Add(new KeyValuePair<string, string>("isMobileDevice", item.Value));
								break;
							case "logOperatingSystemType":
								list.Add(new KeyValuePair<string, string>("logOperatingSystemType", item.Value));
								break;
							case "logBrowserType":
								list.Add(new KeyValuePair<string, string>("LogBrowserType", item.Value));
								break;
							case "logDataChangeStatus":
								list.Add(new KeyValuePair<string, string>("LogDataChangeStatus", item.Value));
								break;
							case "table":
								list.Add(new KeyValuePair<string, string>("IdTable", item.Value));
								break;

						}
					}
				}
			}
			var content = new FormUrlEncodedContent(list);
			var urlEncodedString = await content.ReadAsStringAsync();
			url += "?" + urlEncodedString;
			var response = await ApiRequestHelper<Pagination<LogDataChange>>.Get(url);
			//var result = _mapper.Map<PaginationView>(response.Data.FirstOrDefault());
			return response;
		}
		public async Task<ApiResponse<LogUserAuthorization>> GetLogUserAuthorization(DataSourceFilter data)
		{

			//var url = "Log/dataChanges?IdUser=" + LogedUserHelper.GetLoggedUserId();
			var url = "api/Log/getUserAuthorizations";
			/*var list = new List<KeyValuePair<string, string>>();
			list.Add(new KeyValuePair<string, string>("PageSize", data.PageSize.ToString()));
			list.Add(new KeyValuePair<string, string>("Page", data.Page.ToString()));

			if (data.Filter != null)
			{
				if (data.Filter.Filters != null)
				{
					var filters = data.Filter.Filters;
					foreach (var item in filters)
					{
						switch (item.Field)
						{
							case "entryDate":
								list.Add(new KeyValuePair<string, string>("EntryDate", DateTime.ParseExact(item.Value, "dd.MM.yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
								break;
							case "entryUser":
								list.Add(new KeyValuePair<string, string>("EntryUser", item.Value));
								break;
							case "computerName":
								list.Add(new KeyValuePair<string, string>("ComputerName", item.Value));
								break;
							case "ipAddress":
								list.Add(new KeyValuePair<string, string>("IpAddress", item.Value));
								break;
							case "isMobileDevice":
								list.Add(new KeyValuePair<string, string>("isMobileDevice", item.Value));
								break;
							case "logOperatingSystemType":
								list.Add(new KeyValuePair<string, string>("logOperatingSystemType", item.Value));
								break;
							case "logBrowserType":
								list.Add(new KeyValuePair<string, string>("LogBrowserType", item.Value));
								break;
							case "logUserAuthorizationStatus":
								list.Add(new KeyValuePair<string, string>("IdLogUserAuthorizationStatus", item.Value));
								break;

						}
					}
				}
			}
			var content = new FormUrlEncodedContent(list);
			var urlEncodedString = await content.ReadAsStringAsync();
			url += "?" + urlEncodedString;*/
			var response = await ApiRequestHelper<LogUserAuthorization>.Get(url);
			//var result = _mapper.Map<PaginationView>(response.Data.FirstOrDefault());
			return response;
		}
		public async Task<ApiResponse<LogFailedAuthentication>> GetLogFailedAuthentication(DataSourceFilter data)
		{
			//var url = "Log/dataChanges?IdUser=" + LogedUserHelper.GetLoggedUserId();GetCurrentLocation()
			var url = "api/Log/getAllFailedAuthorizations";
			/*var list = new List<KeyValuePair<string, string>>();
			list.Add(new KeyValuePair<string, string>("PageSize", data.PageSize.ToString()));
			list.Add(new KeyValuePair<string, string>("Page", data.Page.ToString()));

			if (data.Filter != null)
			{
				if (data.Filter.Filters != null)
				{
					var filters = data.Filter.Filters;
					foreach (var item in filters)
					{
						switch (item.Field)
						{
							case "entryDate":
								list.Add(new KeyValuePair<string, string>("EntryDate", DateTime.ParseExact(item.Value, "dd.MM.yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
								break;
							case "computerName":
								list.Add(new KeyValuePair<string, string>("ComputerName", item.Value));
								break;
							case "ipAddress":
								list.Add(new KeyValuePair<string, string>("IpAddress", item.Value));
								break;
							case "isMobileDevice":
								list.Add(new KeyValuePair<string, string>("isMobileDevice", item.Value));
								break;
							case "logOperatingSystemType":
								list.Add(new KeyValuePair<string, string>("logOperatingSystemType", item.Value));
								break;
							case "logBrowserType":
								list.Add(new KeyValuePair<string, string>("LogBrowserType", item.Value));
								break;
							case "account":
								list.Add(new KeyValuePair<string, string>("Account", item.Value));
								break;

						}
					}
				}
			}
			var content = new FormUrlEncodedContent(list);
			var urlEncodedString = await content.ReadAsStringAsync();
			url += "?" + urlEncodedString;*/
			var response = await ApiRequestHelper<LogFailedAuthentication>.Get(url);
			//var result = _mapper.Map<PaginationView>(response.Data.FirstOrDefault());
			return response;
		}
		public async Task<ApiResponse<Pagination<LogUserActivity>>> GetLogUserActivity(DataSourceFilter data)
		{
			//var url = "Log/dataChanges?IdUser=" + LogedUserHelper.GetLoggedUserId();
			var url = "api/Log/dataLogUserActivity";
			var list = new List<KeyValuePair<string, string>>();
			list.Add(new KeyValuePair<string, string>("PageSize", data.PageSize.ToString()));
			list.Add(new KeyValuePair<string, string>("Page", data.Page.ToString()));

			if (data.Filter != null)
			{
				if (data.Filter.Filters != null)
				{
					var filters = data.Filter.Filters;
					foreach (var item in filters)
					{
						switch (item.Field)
						{
							case "entryDate":
								list.Add(new KeyValuePair<string, string>("EntryDate", DateTime.ParseExact(item.Value, "dd.MM.yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
								break;
							case "entryUser":
								list.Add(new KeyValuePair<string, string>("EntryUser", item.Value));
								break;
							case "computerName":
								list.Add(new KeyValuePair<string, string>("ComputerName", item.Value));
								break;
							case "ipAddress":
								list.Add(new KeyValuePair<string, string>("IpAddress", item.Value));
								break;
							case "isMobileDevice":
								list.Add(new KeyValuePair<string, string>("isMobileDevice", item.Value));
								break;
							case "logOperatingSystemType":
								list.Add(new KeyValuePair<string, string>("logOperatingSystemType", item.Value));
								break;
							case "logBrowserType":
								list.Add(new KeyValuePair<string, string>("LogBrowserType", item.Value));
								break;
							case "module":
								list.Add(new KeyValuePair<string, string>("IdModule", item.Value));
								break;
							case "logUserActivityStatus":
								list.Add(new KeyValuePair<string, string>("IdLogUserActivityStatus", item.Value));
								break;

						}
					}
				}
			}
			var content = new FormUrlEncodedContent(list);
			var urlEncodedString = await content.ReadAsStringAsync();
			url += "?" + urlEncodedString;
			var response = await ApiRequestHelper<Pagination<LogUserActivity>>.Get(url);
			//var result = _mapper.Map<PaginationView>(response.Data.FirstOrDefault());
			return response;
		}

		public async Task<bool> SetLogActivity(int idModule, int activityStatus)
		{
			var _ = new LogActivityModel
			{
				Host = LogedUser.GetCurrentLocation(),
				IDModule = idModule,
				ActivityStatus = activityStatus
			};
			var url = "api/Log/userActivity";
			var parameters = JsonConvert.SerializeObject(_);
			var response = await ApiRequestHelper<LogActivityModel>.Post(url, parameters);
			if (response.Status == (int)PublicResultStatusCodes.Done)
				return true;
			return false;
		}
	}
}
