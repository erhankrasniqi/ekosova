﻿using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.Hierarchies;
using eKosovaInternal.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
	public class HierarchyService : IHierarchyService
	{
		public async Task<IEnumerable<HierarchyViewModel>> GetHierarchy()
		{
			try
			{
				var result = await ApiRequestHelper<HierarchyViewModel>.Get("api/internal/getHierarchy");
				if (result.Status == 0)
					return result.Data;
				else
					throw new Exception("noData");
			}
			catch
			{
				return null;
			}
		}
	}
}
