﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Interfaces;
using eKosovaInternal.SharedModel.PublicUsers;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
    public class PublicUserService : IPublicUserService
    {
        public async Task<IEnumerable<PublicUser>> GetPublicUser()
        {
            var url = "api/PublicUsers/getPublicUsers";
            var response = await ApiRequestHelper<PublicUser>.Get(url);
            if (response.Status == 0)
                return response.Data;
             return null;
        }

        public async Task<IEnumerable<PublicUser>> GetPublicUserPersonalNumber(string personalNumber)
        {
            var url = "api/PublicUsers/getPublicUser";
            var parameters = new { Identification = personalNumber };
            var stringified = JsonConvert.SerializeObject(parameters);
            var response = await ApiRequestHelper<PublicUser>.Post(url,stringified);
            if(response.Status==0 && response!=null)
                return response.Data;
            return null;
        }

        public async Task<Response<PublicUser>> UpdatePublicUsers(UpdatePublicUserModel _)
        {
            try
            {
                var url = "api/PublicUsers/edit";
                var parameters = JsonConvert.SerializeObject(_);
                var response = await ApiRequestHelper<PublicUser>.Post(url, parameters);
                return new Response<PublicUser>(PublicResultStatusCodes.Done);
            }
            catch (System.Exception e) {
                return new Response<PublicUser>(PublicResultStatusCodes.QueryHasError);
            }
        }
    }
}
