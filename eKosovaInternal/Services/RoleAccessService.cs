﻿using eKosovaInternal.Business;
using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Interfaces;
using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.Modules;
using eKosovaInternal.SharedModel.Roles;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
	public class RoleAccessService : IRoleAccessService
    {
        public async Task<ApiResponse<ModuleModel>> GetModules(int ID)
        {
            var url = "api/Module/getModulesByRole/" + ID;
            var response = await ApiRequestHelper<ModuleModel>.Get(url);
            return response;
        }
        public async Task<ApiResponse<ModuleModel>> GetModulesForUser(int ID)
        {
            var url = "api/Module/getModulesByUser/" + ID;
            var response = await ApiRequestHelper<ModuleModel>.Get(url);
            return response;
        }
		public async Task<List<ModuleModel>> GetModulesTree(List<ModuleModel> list)
		{
			var data = new List<ModuleModel>();
			foreach (var item in list)
			{
				data.Add(new ModuleModel()
				{
					FullName = GetName(item.ID, list),
					MarginDistance = GetDistance(item.ID, list, 0),
					Title = item.Title,
					ID = item.ID,
					IDParent = item.IDParent,
					IDRoleAuthorizationType = item.IDRoleAuthorizationType,
					IDRoleAuthorization = item.IDRoleAuthorization,
					IDs = GetIdsChildren(item.ID, list, new List<int>())

				});
			}
			return data.OrderBy(_ => _.FullName).ToList();
		}
		private string GetName(int? id, List<ModuleModel> list)
        {
            var listItem = list.Where(_ => _.ID == id).FirstOrDefault();
            if (listItem.IDParent == null)
            {

                return listItem.Title;
            }
            else
            {
                return GetName(listItem.IDParent, list) + " >> " + listItem.Title;
            }
        }
        public int GetDistance(int? id, List<ModuleModel> list, int distance)
        {

            var listItem = list.Where(_ => _.ID == id).FirstOrDefault();
            if (listItem.IDParent == null)
            {
                return distance;
            }
            else
            {
                distance += 40;
                return GetDistance(listItem.IDParent, list, distance);
            }
        }
        public List<int> GetIdsChildren(int? id, List<ModuleModel> list, List<int> ids)
        {
            foreach (var item in list.Where(x => x.IDParent == id))
            {
                ids.Add(item.IDRoleAuthorization);
                if (list.Where(x => x.IDParent == item.ID).ToList().Count() > 0)
                {
                    return GetIdsChildren(item.ID, list, ids);
                }
            }
            return ids;
        }

        public async Task<ApiResponse<RoleModel>> CreateRole(NewRoleModel _)
        {
            var url = "api/Role/createRole";
            var parameters = JsonConvert.SerializeObject(_);
            var response = await ApiRequestHelper<RoleModel>.Post(url, parameters);
            return response;
        }
        public async Task<bool> UpdateRole(EditRoleModel _, int id)
        {
            var url = "api/Role/editRole/" + id;
            var parameters = JsonConvert.SerializeObject(_);
            var response = await ApiRequestHelper<RoleModel>.Put(url, parameters);
            return response.Status == (int)PublicResultStatusCodes.Done ? true : false;
        }
        public async Task<ApiResponse<RoleModel>> GetRoles()
        {
            var url = "api/Role/getRoles";
            var response = await ApiRequestHelper<RoleModel>.Get(url);
            return response;
        }

        public async Task<bool> UpdateRoleAuthorization(EditRoleAuthorizationModel _, int id)
        {
            var url = "api/Role/editRoleAuthorization/" + id;
            var parameters = JsonConvert.SerializeObject(_);
            var response = await ApiRequestHelper<RoleAuthorizationModel>.Put(url, parameters);
            return response.Status == (int)PublicResultStatusCodes.Done ? true : false;
        }

        public async Task<bool> DeleteRole(string TableName, int id)
        {
            var url = "Administration/deleteTableRow/" + id + "/" + TableName;
            var response = await ApiRequestHelper<RoleModel>.Delete(url);
            return response.Status == (int)PublicResultStatusCodes.Done ? true : false;
        }

		//public async Task<bool> UpdateUserAuthorization(EditUserAuthorizationModel _, int id)
		//{
		//    var url = "User/editUserAuthorization/" + id;
		//    var parameters = JsonConvert.SerializeObject(_);
		//    var response = await ApiRequestHelper<RoleAuthorizationModel>.Put(url, parameters, true);
		//    return response.Status == (int)PublicResultStatusCodes.Done ? true : false;
		//}

		//public async Task<bool> SetLanguage(string culture)
		//{
		//    var Language = new LanguageSettings()
		//    {
		//        Language = culture
		//    };
		//    var parameters = JsonConvert.SerializeObject(Language);

		//    var url = "Administration/SetLanguage";
		//    var response = await ApiRequestHelper<LanguageSettings>.Post(url, parameters, false);
		//    return response.Status == (int)PublicResultStatusCodes.Done ? true : false;
		//}
	}
}
