﻿using eKosova.Domain.DynamicForms;
using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Interfaces;
using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.DynamicForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
    public class DynamicFormService : IDynamicFormService
    {
        public async Task<ApiResponse<CreateDynamicForms>> CreateDynamicForm(CreateDynamicForms model)
        {
            try
            {
                var url = "api/DynamicForm/createDynamicForm";
                var content = JsonConvert.SerializeObject(model);
                var response = await ApiRequestHelper<CreateDynamicForms>.Post(url, content, true);
                return response;
            }
            catch (Exception e)
            {
                throw;
            }


        }

        public async Task<ApiResponse<MainServices>> MainCategories()
        {
            try
            {
                var url = "api/DynamicForm/getMainServices";
                //var content = JsonConvert.SerializeObject();
                var response = await ApiRequestHelper<MainServices>.Get(url, true);
                return response;
            }
            catch (Exception e)
            {
                throw;
            }


        }

        public async Task<ApiResponse<DynamicForm>> GetDynamicForms()
        {
            try
            {
                var url = "api/DynamicForm/getDynamicForms/";
                //var content = JsonConvert.SerializeObject();
                var response = await ApiRequestHelper<DynamicForm>.Get(url, true);
                return response;
            }
            catch(Exception e)
            {
                throw;
            }
        }


    }
}
