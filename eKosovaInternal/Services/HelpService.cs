﻿using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.Helps;
using eKosovaInternal.Interfaces;
using eKosovaInternal.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
    public class HelpService : IHelpService
    {

        public async Task<ApiResponse<Help>> CreateHelpNotification(Help _)
        {
            object help = new
            {
                ID = _.ID,
                Answer = _.Answer,
                Email = _.Email,
                IDInternalUser = _.IDInternalUser
            };
            var helpResult = await ApiRequestHelper<Help>.Post("api/Help/edit", JsonConvert.SerializeObject(help));
            return helpResult;
        }

        public async Task<IEnumerable<Help>> GetActiveHelps()
        {
            var url = "api/Help/getActiveHelps";
            var response = await ApiRequestHelper<Help>.Get(url);
            return response.Data;
        }

        public async Task<IEnumerable<Help>> GetHelps()
        {
            var url = "api/Help/getHelps";
            var response = await ApiRequestHelper<Help>.Get(url);
            return response.Data;
        }
    }
}
