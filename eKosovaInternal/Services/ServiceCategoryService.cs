﻿using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.Services;
using eKosovaInternal.Interfaces;
using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.DynamicForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
    public class ServiceCategoryService : IServiceCategoryService
    {
        public async Task<ApiResponse<ServiceCategory>> GetServiceCategories()
        {
            try
            {
                var url = "api/Service/getServiceCategories/";
                //var content = JsonConvert.SerializeObject(model);
                var response = await ApiRequestHelper<ServiceCategory>.Get(url);
                return response;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
