﻿using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.Contacts;
using eKosovaInternal.Interfaces;
using eKosovaInternal.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Services
{
    public class ContactService : IContactService
    {
        public async Task<IEnumerable<Contact>> GetContacts()
        {
            var url = "api/Contact/getContacts";
            var response = await ApiRequestHelper<Contact>.Get(url);


            return response.Data;
        }

        public async Task<IEnumerable<Contact>> GetActiveContacts()
        {
            var url = "api/Contact/getActiveContacts";
            var response = await ApiRequestHelper<Contact>.Get(url);


            return response.Data;
        }

        public async Task<ApiResponse<Contact>> CreateContactNotification(Contact _)
        {
            object contact = new
            {
                ID = _.ID,
                Answer = _.Answer,
                Email = _.Email,
                IDInternalUser = _.IDInternalUser
            };
            var contactResult = await ApiRequestHelper<Contact>.Post("api/Contact/edit", JsonConvert.SerializeObject(contact));
            return contactResult;

        }

    }
}
