using AutoMapper;
using eKosovaInternal.Interfaces;
using eKosovaInternal.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace eKosovaInternal
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
			StaticConfig = configuration;
		}

		public IConfiguration Configuration { get; private set; }

		public static IConfiguration StaticConfig { get; private set; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddAuthentication("CookieAuthentication").AddCookie("CookieAuthentication", config =>
				   {
					   config.Cookie.Name = "UserLoginCookie";
					   config.LoginPath = "/Security/Index";
				   });

			services.AddRazorPages().AddRazorRuntimeCompilation();
			services.AddMvc(_ => _.EnableEndpointRouting = false);

			services.AddHttpContextAccessor();
			//services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
			services.AddScoped<IContactService, ContactService>();
			services.AddScoped<IHelpService, HelpService>();
			services.AddScoped<IPublicUserService, PublicUserService>();
			services.AddScoped<IDynamicFormService, DynamicFormService>();
			services.AddScoped<IServiceCategoryService, ServiceCategoryService>();
			services.AddScoped<IRoleAccessService, RoleAccessService>();
			services.AddScoped<IInternalUserService, InternalUserService>();
			services.AddScoped<IHierarchyService, HierarchyService>();
			services.AddScoped<IUserAuthorizationService, UserAuthorizationService>();
			services.AddScoped<ILogService, LogService>();

			services.AddScoped<IFaqService, FaqService>();
			services.AddScoped<IInsertSubmissionService, InsertSubmissionService>();

			services.AddScoped<IPublicUserHistoryService, PublicUserHistoryService>();


			services.AddSession(options =>
			{
				options.IdleTimeout = TimeSpan.FromMinutes(30);
				options.Cookie.HttpOnly = true;
				options.Cookie.IsEssential = true;
			});

			services.AddAutoMapper(typeof(Startup));

			services.AddControllersWithViews();

		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}

			app.UseSession();

			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthentication();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Security}/{action=Index}/{id?}");
			});
		}
	}
}
