﻿using System.Collections.Generic;

namespace eKosovaInternal.Models
{
	public class ApiResponse<T>
    {
        public int Status { get; set; }
        public IList<T> Data { get; set; }
        public T SingleData { get; set; }
    }
}
