﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Models
{
    public class CheckAccess
    {
        public CheckAccess(int IDAccessType)
        {
            switch (IDAccessType)
            {
                case 1:
                    {
                        CanWrite = true;
                        CanSee = true;
                        NoAccess = false;
                    }
                    break;
                case 2:
                    {
                        CanWrite = false;
                        CanSee = true;
                        NoAccess = false;
                    }
                    break;
                case 3:
                    {
                        CanWrite = false;
                        CanSee = false;
                        NoAccess = true;
                    }
                    break;
                default:
                    {
                        CanWrite = false;
                        CanSee = false;
                        NoAccess = true;
                    }
                    break;
            }
        }
        public bool CanWrite = false;
        public bool CanSee = false;
        public bool NoAccess = true;
    }
}
