﻿using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.SeedWork;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Custom
{
	public static class InternalUserHelper
	{
		public async static Task<bool> ClearTokens(string identification)
		{
			var parameters = JsonConvert.SerializeObject(new BaseIdentification { Identification = identification });

			var response = await ApiRequestHelper<bool>.Post("api/internal/clearTokens", parameters);

			return response.Status == 0;
		}
	}
}
