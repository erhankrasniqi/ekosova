﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using eKosovaInternal.Models;
using eKosovaInternal.Custom.Models;

namespace eKosovaInternal.Custom.Helpers
{
	public class ApiRequestHelper<T>
	{
		public static readonly string URL = Startup.StaticConfig.GetSection("Backend_URL").Value;

		public static bool HasError = false;
		public static string ErrorMessage = string.Empty;

		public static async Task<ApiResponse<T>> Get(string url, bool isAuthorize = false)
		{
			try
			{
				var client = new HttpClient
				{
					BaseAddress = new Uri(URL)
				};

				client.DefaultRequestHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue("application/json"));

				client.DefaultRequestHeaders.UserAgent.ParseAdd(LogedUser._contextAccessor.HttpContext.Request.Headers["User-Agent"].ToString());
				//client.DefaultRequestHeaders.Add("client-ip", LogedUserHelper._contextAccessor.HttpContext.Connection.RemoteIpAddress.ToString());

				if (isAuthorize)
					client.DefaultRequestHeaders.Authorization =
					new AuthenticationHeaderValue("Bearer", LogedUser.GetLoggedUserToken());

				var response = await client.GetAsync(url);

				var jsonString = await response.Content.ReadAsStringAsync();

				return JsonConvert.DeserializeObject<ApiResponse<T>>(jsonString);
			}
			catch (Exception error)
			{
				ErrorMessage = error.ToString();
				HasError = true;
				return null;
			}
		}

		public static async Task<ApiResponse<T>> Post(string url, string parameters, bool isAuthorize = false, KeyValuePair<string, string> headerParam = (default(KeyValuePair<string, string>)))
		{
			try
			{
				var client = new HttpClient { BaseAddress = new Uri(URL) };

				client.DefaultRequestHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue("application/json"));

				if (!headerParam.Equals(default(KeyValuePair<string, string>)))
					client.DefaultRequestHeaders.Add(headerParam.Key, headerParam.Value);

				if (isAuthorize)
					client.DefaultRequestHeaders.Authorization =
					new AuthenticationHeaderValue("Bearer", LogedUser.GetLoggedUserToken());

				var content = new StringContent(parameters, Encoding.UTF8, "application/json");

				var response = await client.PostAsync(url, content);

				var jsonString = await response.Content.ReadAsStringAsync();

				return JsonConvert.DeserializeObject<ApiResponse<T>>(jsonString);
			}
			catch (Exception error)
			{
				ErrorMessage = error.ToString();
				HasError = true;
				return null;
			}
		}

		public static async Task<ApiResponse<T>> Post(string url, MultipartFormDataContent content, bool isAuthorize = false)
		{
			try
			{
				var client = new HttpClient { BaseAddress = new Uri(URL) };

				client.DefaultRequestHeaders.UserAgent.ParseAdd(LogedUser._contextAccessor.HttpContext.Request.Headers["User-Agent"].ToString());
				//client.DefaultRequestHeaders.Accept.Add(
				//new MediaTypeWithQualityHeaderValue("application/json"));


				if (isAuthorize)
					client.DefaultRequestHeaders.Authorization =
					new AuthenticationHeaderValue("Bearer", LogedUser.GetLoggedUserToken());

				var response = await client.PostAsync(url, content);

				var jsonString = await response.Content.ReadAsStringAsync();

				return JsonConvert.DeserializeObject<ApiResponse<T>>(jsonString);
			}
			catch (Exception error)
			{
				ErrorMessage = error.ToString();
				HasError = true;
				return null;
			}
		}

		public static async Task<ApiResponse<T>> Put(string url, string parameters, bool isAuthorize = false)
		{
			try
			{
				var client = new HttpClient { BaseAddress = new Uri(URL) };

				client.DefaultRequestHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue("application/json"));

				client.DefaultRequestHeaders.UserAgent.ParseAdd(LogedUser._contextAccessor.HttpContext.Request.Headers["User-Agent"].ToString());

				if (isAuthorize)
					client.DefaultRequestHeaders.Authorization =
					new AuthenticationHeaderValue("Bearer", LogedUser.GetLoggedUserToken());

				var content = new StringContent(parameters, Encoding.UTF8, "application/json");

				var response = await client.PutAsync(url, content);

				var jsonString = await response.Content.ReadAsStringAsync();

				return JsonConvert.DeserializeObject<ApiResponse<T>>(jsonString);
			}
			catch (Exception error)
			{
				ErrorMessage = error.ToString();
				HasError = true;
				return null;
			}
		}

		public static async Task<ApiResponse<T>> Delete(string url, bool isAuthorize = false)
		{
			try
			{
				var client = new HttpClient { BaseAddress = new Uri(URL) };

				client.DefaultRequestHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue("application/json"));

				client.DefaultRequestHeaders.UserAgent.ParseAdd(LogedUser._contextAccessor.HttpContext.Request.Headers["User-Agent"].ToString());

				if (isAuthorize)
					client.DefaultRequestHeaders.Authorization =
					new AuthenticationHeaderValue("Bearer", LogedUser.GetLoggedUserToken());

				var response = await client.DeleteAsync(url);

				var jsonString = await response.Content.ReadAsStringAsync();

				return JsonConvert.DeserializeObject<ApiResponse<T>>(jsonString);
			}
			catch (Exception error)
			{
				ErrorMessage = error.ToString();
				HasError = true;
				return null;
			}
		}

	}
}
