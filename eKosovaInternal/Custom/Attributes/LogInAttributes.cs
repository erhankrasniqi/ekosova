﻿using eKosovaInternal.Custom.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace eKosovaInternal.Custom.Attributes
{
	internal class LoggedInAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			if (!LogedUser.LoggedIn())
				context.Result = new RedirectToActionResult("Index", "Security", null);
		}
	}
	internal class AlreadyLoggedInAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			if (LogedUser.LoggedIn())
				context.Result = new RedirectToActionResult("Index", "Home", null);
		}
	}
}