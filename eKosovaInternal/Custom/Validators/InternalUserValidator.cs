﻿using eKosovaInternal.Domain.InternalUsers;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Custom.Validators
{
	public class InternalUserValidator : AbstractValidator<InternalUserCreate>
	{
		public InternalUserValidator()
		{
			RuleFor(x => x.PhoneNumber).Length(8);
			RuleFor(x => x.IDHierarchy).NotNull().NotEmpty();
			RuleFor(x => x.IDRole).NotNull().NotEmpty();
			RuleFor(x => x.IDUserAuthorizationType).NotNull().NotEmpty();
			RuleFor(x => x.First).NotNull().NotEmpty();
			RuleFor(x => x.Last).NotNull().NotEmpty();
			RuleFor(x => x.Account).NotNull().NotEmpty();
			RuleFor(x => x.Password).NotNull().NotEmpty().When(x => x.IDUserAuthorizationType == 1);
			RuleFor(x => x.IDActiveDirectoryDomain).NotNull().NotEmpty().When(x => x.IDUserAuthorizationType == 2);
		}
	}
}
