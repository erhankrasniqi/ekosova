﻿using eKosovaInternal.Domain.Roles;
using eKosovaInternal.SharedModel.InternalUsers;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Helpers
{
    public static class LoggedUserHelper
    {
        public static IHttpContextAccessor _contextAccessor;
        public static void SetHttpContextAccessor(IHttpContextAccessor accessor)
        {
            _contextAccessor = accessor;
        }
        public static void SetUserSession(LoginResponse _)
        {
            _contextAccessor.HttpContext.Session.SetComplexData("logged_user", _);
        }
        public static void SetRoleAccessSession(List<RoleViewAccess> _)
        {
            _contextAccessor.HttpContext.Session.SetComplexData("role_view", _);
        }

        public static LoginResponse GetLoggedUser()
        {
            return _contextAccessor.HttpContext.Session.GetComplexData<LoginResponse>("logged_user");
        }
        public static List<RoleViewAccess> GetRoleAccess()
        {
            return _contextAccessor.HttpContext.Session.GetComplexData<List<RoleViewAccess>>("role_view");
        }
        public static string GetCurrentLocation()
        {
            var request = _contextAccessor.HttpContext.Request;
            var absoluteUri = string.Concat(
                        request.Scheme,
                        "://",
                        request.Host.ToUriComponent(),
                        request.PathBase.ToUriComponent(),
                        request.Path.ToUriComponent(),
                        request.QueryString.ToUriComponent());
            return absoluteUri;
        }

        public static void DeleteUserSession()
        {
            _contextAccessor.HttpContext.Session.Clear();
        }

        public static int GetLoggedUserId()
        {
            return GetLoggedUser().ID;
        }

        public static string GetLoggedUsername()
        {
            return GetLoggedUser().Username;
        }

        public static string GetLoggedUserFirstLast()
        {
            return GetLoggedUser().FirstLast;
        }

        public static string GetLoggedUserToken()
        {
            return GetLoggedUser()?.Token;
        }

        public static string GetLoggedUserRefreshToken()
        {
            return GetLoggedUser().RefreshToken;
        }

        public static DateTime GetLoggedUserTokenValidTime()
        {
            return GetLoggedUser().ValidDateTimeToken;
        }
    }
}
