﻿using eKosovaInternal.Domain.Roles;
using eKosovaInternal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Helpers
{
    public class AuthorizeHelper
    {
        private List<RoleViewAccess> ViewsAuthorization;
        public AuthorizeHelper() => ViewsAuthorization = (new AuthorizeAccessInternalHelper().Get()).ToList();
        public CheckAccess Check(int IDView) => new CheckAccess(ViewsAuthorization.FirstOrDefault(_ => _.IdModule == IDView).IdRoleAuthorizationType);
    }
}
