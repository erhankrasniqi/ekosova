﻿using eKosovaInternal.Domain.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Helpers
{
    public class AuthorizeAccessInternalHelper
    {
        public List<RoleViewAccess> Get()
        {
            try
            {
                return LoggedUserHelper.GetRoleAccess().ToList();
            }
            catch (Exception ex)
            {
                return new List<RoleViewAccess>();
            }
        }
    }
}
