﻿function checkCache(e) {
	if (window.localStorage.getItem(`${e}Cache`) === null)
		loadCache(e);
	else if (Math.floor((Date.now() - JSON.parse(window.localStorage.getItem(`${e}Cache`)).DateTime) / (24 * 60 * 60 * 1000))>1)
		loadCache(e);
}

function loadCache(e) {
	var cache = {
		Data: GetSyncWithOutLoader(`/${e}/Data`).responseText,
		DateTime: Date.now()
	};
	window.localStorage.setItem(`${e}Cache`, JSON.stringify(cache));
}

function GetSyncWithOutLoader(URL) {
	return jQuery.ajax({
		url: URL,
		type: "GET",
		cache: false,
		async: false,
		statusCode: {
			401: function () {
				Redirect('/Security/Index');
			},
			302: function () {
				alert('Nuk keni qasje ne kete sherbim!!!');
			}
		},
		beforeSend: function () {
		},
		complete: function () {
		},
		error: function () {
		}
	});
}
