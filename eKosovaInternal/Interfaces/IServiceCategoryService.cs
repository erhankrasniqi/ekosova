﻿using eKosovaInternal.Domain.Services;
using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.DynamicForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
    public interface IServiceCategoryService
    {
        Task<ApiResponse<ServiceCategory>> GetServiceCategories();
    }
}
