﻿using eKosovaInternal.Domain.Helps;
using eKosovaInternal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
    public interface IHelpService
    {
        Task<IEnumerable<Help>> GetHelps();
        Task<ApiResponse<Help>> CreateHelpNotification(Help _);
        Task<IEnumerable<Help>> GetActiveHelps();
    }
}
