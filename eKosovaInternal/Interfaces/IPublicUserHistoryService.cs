﻿using eKosovaInternal.Domain.PublicUserHistory;
using eKosovaInternal.SharedModel.PublicUserHistory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
   public interface IPublicUserHistoryService
    {
        Task<IEnumerable<PublicUserHistoryModel>> GetPublicUserHistory(int publicUserID);
    }
}
