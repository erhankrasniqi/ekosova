﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.ADDomains;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
	public interface IInternalUserService
	{
		Task<IEnumerable<InternalUserViewModel>> GetAll();
		Task<ApiResponse<InternalUserViewModel>> Create(InternalUserCreate _);
		Task<ApiResponse<InternalUser>> Read(string identification);
		Task<ApiResponse<bool>> CheckAccount(string username);
		Task<ApiResponse<bool>> Delete(int id);
		Task<ApiResponse<bool>> Update(InternalUser _);
		Task<ApiResponse<bool>> Update(InternalUserViewModel _);
		Task<IEnumerable<ADDomainViewModel>> GetDomains();
	}
}
