﻿using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.Edergesa;
using eKosovaInternal.SharedModel.Submission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
   public interface IInsertSubmissionService
    {
        Task<ApiResponse<SubmissionPersonalUserModel>> GetSubmissionByPersonalnumber(int personalNumber);
    }
}
