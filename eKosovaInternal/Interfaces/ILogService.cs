﻿using eKosovaInternal.Domain.Logs;
using eKosovaInternal.KendoFilter;
using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
	public interface ILogService
	{
		Task<ApiResponse<Pagination<LogDataChange>>> GetDataChanges(DataSourceFilter data);
		Task<ApiResponse<LogUserAuthorization>> GetLogUserAuthorization(DataSourceFilter data);
		Task<ApiResponse<LogFailedAuthentication>> GetLogFailedAuthentication(DataSourceFilter data);
		Task<ApiResponse<Pagination<LogUserActivity>>> GetLogUserActivity(DataSourceFilter data);
		Task<bool> SetLogActivity(int idModule, int idActivityStatus);
	}
}
