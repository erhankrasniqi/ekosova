﻿using eKosova.Domain.DynamicForms;
using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.DynamicForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
    public interface IDynamicFormService
    {
        Task<ApiResponse<CreateDynamicForms>> CreateDynamicForm(CreateDynamicForms model);
        Task<ApiResponse<DynamicForm>> GetDynamicForms();
    }
}
