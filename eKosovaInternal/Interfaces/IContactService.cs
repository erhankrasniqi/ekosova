﻿using eKosovaInternal.Domain.Contacts;
using eKosovaInternal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
    public interface IContactService
    {
        Task<IEnumerable<Contact>> GetContacts();
        Task<ApiResponse<Contact>> CreateContactNotification(Contact _);
        Task<IEnumerable<Contact>> GetActiveContacts();
    }
}
