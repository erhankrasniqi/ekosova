﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.Faqs;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.Faqs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
   public interface IFaqService
    {
        Task<IEnumerable<FaqModel>> GetFaqs();
        Task<ApiResponse<FaqModel>> CreateFaq(CreateFaqModel _);
        Task<ApiResponse<FaqModel>> EditFaq(EditFaqModel _);
         
        Task<Response<bool>> DeleteFaq(int id);

    }
}
