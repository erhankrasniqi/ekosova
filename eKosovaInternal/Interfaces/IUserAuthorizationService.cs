﻿using eKosovaInternal.Domain.InternalUsers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
	public interface IUserAuthorizationService
	{
		Task<IEnumerable<UserAuthorizationType>> GetAuthorizations();
	}
}
