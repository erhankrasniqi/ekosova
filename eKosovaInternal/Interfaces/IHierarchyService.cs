﻿using eKosovaInternal.Domain.Hierarchies;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
	public interface IHierarchyService
	{
		Task<IEnumerable<HierarchyViewModel>> GetHierarchy();
	}
}
