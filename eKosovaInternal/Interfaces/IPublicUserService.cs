﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.SharedModel.PublicUsers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
    public interface IPublicUserService
    {
        Task<IEnumerable<PublicUser>> GetPublicUser();
        Task<IEnumerable<PublicUser>> GetPublicUserPersonalNumber(string personalNumber);

        Task<Response<PublicUser>> UpdatePublicUsers(UpdatePublicUserModel _);

         
    }
}
