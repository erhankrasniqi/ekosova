﻿using eKosovaInternal.Models;
using eKosovaInternal.SharedModel.Modules;
using eKosovaInternal.SharedModel.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Interfaces
{
    public interface IRoleAccessService
    {
        Task<ApiResponse<RoleModel>> GetRoles();
        Task<ApiResponse<ModuleModel>> GetModules(int ID);
        Task<ApiResponse<ModuleModel>> GetModulesForUser(int ID);
        Task<List<ModuleModel>> GetModulesTree(List<ModuleModel> list);
        Task<ApiResponse<RoleModel>> CreateRole(NewRoleModel _);
        Task<bool> UpdateRole(EditRoleModel _, int id);
        Task<bool> UpdateRoleAuthorization(EditRoleAuthorizationModel _, int id);
        //Task<bool> UpdateUserAuthorization(EditUserAuthorizationModel _, int id);
        Task<bool> DeleteRole(string TableName, int id);
        //Task<bool> SetLanguage(string culture);
    }
}
