﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Domain.Services;
using eKosovaInternal.Interfaces;
using eKosovaInternal.SharedModel.DynamicForms;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternal.Controllers
{
	//[Route("api/[controller]")]
	//[ApiController]
	public class DynamicFormController : Controller
	{

		private readonly IDynamicFormService _dynamicFormService;
		private readonly IServiceCategoryService _serviceCategoryService;

		public DynamicFormController(IDynamicFormService dynamicFormService, IServiceCategoryService serviceCategoryService)
		{
			_dynamicFormService = dynamicFormService;
			_serviceCategoryService = serviceCategoryService;
		}

		[HttpGet]
		public IActionResult CreateDynamicForm()
			=> View();

		[HttpGet]
		public IActionResult DynamicFormList()
			=> View();

		[HttpPost]
		public async Task<IActionResult> AddDynamicForm(CreateDynamicForms model)
		{
			await _dynamicFormService.CreateDynamicForm(model);
			return RedirectToAction("CreateDynamicForm", "DynamicForm");
		}

		[HttpGet]
		public async Task<IActionResult> _CreateMetadataDynamicForm()
		{
			IList<ServiceCategory> services;
			services = (await _serviceCategoryService.GetServiceCategories()).Data;
			return PartialView(services);
		}

		[HttpGet]
		public async Task<IActionResult> ListOfDynamicPlan()
		{
			var x = await _dynamicFormService.GetDynamicForms();
			return Ok(x);
		}

		[HttpGet]
		public IActionResult _QuestionDynamicForm()
		=> PartialView();

		[HttpGet]
		public IActionResult AnswerType(int ID)
		{
			switch (ID)
			{
				case 1:
					return PartialView("_InputTextField");
				case 2:
					return PartialView("_TextArea");
				case 3:
					return PartialView("_Dropdown");
				case 4:
					return PartialView("_Dropdown");
				case 5:
					return PartialView("_RadioButton");
				case 6:
					return PartialView("_Checkbox");
				case 7:
					return PartialView("_Grid");
				case 8:
					return PartialView("_InputTextField");
				default:
					return PartialView("_InputTextField");
			}
		}

	}
}
