﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using eKosovaInternal.Domain;
using eKosovaInternal.Custom.Models;
using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Custom.Attributes;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;

namespace eKosovaInternal.Controllers
{
	[AlreadyLoggedIn]
	public class SecurityController : Controller
	{
		public SecurityController(IHttpContextAccessor contextAccessor)
		{ LogedUser.SetHttpContextAccessor(contextAccessor); }

		public IActionResult Index() => View();

		[HttpPost]
		public async Task<IActionResult> Login(Credentials credentials)
		{
			try
			{
				if (!new CredentialsValidator().Validate(credentials).IsValid)
					throw new System.Exception("MISSING_ACCOUNT/PW");

				var url = "authenticate";
				var parameters = JsonConvert.SerializeObject(credentials);
				var response = await ApiRequestHelper<LoginResponse>.Post(url, parameters);

				switch (response.Status)
				{
					case 0:
						var responseObject = response.Data.First();

						var claims = new[] {
									new Claim(ClaimTypes.Name, credentials.Account),
									new Claim("IDRole", responseObject.Role.ToString()),
									new Claim("Identification", responseObject.Identification),
									new Claim("AccessToken", responseObject.Token),
									new Claim("RefreshToken", responseObject.RefreshToken),
									new Claim("FullName",responseObject.First + " " + responseObject.Last)
								};
						var identity = new ClaimsIdentity(claims, "User Identity");
						var userPrincipal = new ClaimsPrincipal(new[] { identity });
						await HttpContext.SignInAsync(userPrincipal);

						// LogedUser.SetUserSession(responseObject);
						return RedirectToAction("Index", "Contact"); // replace with redirect to dashboard?
					case 1:
					case 11:
						ModelState.AddModelError("Credentials", "Përdoruesi ose fjalëkalimi janë gabim");
						return View("Index");
					default:
						throw new System.Exception("OTHER_STATUS_CODES: " + response.Status.ToString());
				}
			}
			catch
			{
				ModelState.AddModelError("Credentials", "Ju lutem provoni më vonë");
				return View("Index");
			}
		}
	}
}