﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Interfaces;
using eKosovaInternal.SharedModel.DynamicForms;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternal.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class ServiceController : Controller
    {

        private readonly IServiceCategoryService _serviceCategoryService;

        public ServiceController(IServiceCategoryService serviceCategoryService)
        {
            _serviceCategoryService = serviceCategoryService;
        }
    }
}
