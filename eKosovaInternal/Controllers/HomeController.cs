﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using eKosovaInternal.Models;
using eKosovaInternal.Custom.Models;
using Microsoft.AspNetCore.Http;
using eKosovaInternal.Custom;
using eKosovaInternal.Business.Helpers;
using Microsoft.AspNetCore.Authentication;

namespace eKosovaInternal.Controllers
{
	public class HomeController : Controller
	{

		public HomeController(IHttpContextAccessor contextAccessor)
		{
			LogedUser.SetHttpContextAccessor(contextAccessor);
		}

		public IActionResult Index() => View();


		public IActionResult Privacy() => View();

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		=> View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });

		public async Task<IActionResult> Logout()
		{
			await InternalUserHelper.ClearTokens(LogedUser.GetLoggedUserId());
			await HttpContext.SignOutAsync();
			return RedirectToAction("Index", "Security");
		}
	}
}
