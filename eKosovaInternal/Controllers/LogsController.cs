﻿using System.Threading.Tasks;
using eKosovaInternal.Interfaces;
using eKosovaInternal.KendoFilter;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternal.Controllers
{
	public class LogsController : Controller
	{
		private readonly ILogService _logService;

		public LogsController(ILogService logService)
		{ _logService = logService; }

		[HttpGet]
		public IActionResult Index() => View();

		[HttpGet]
		public IActionResult LogUserAuthorization() => PartialView();

		[HttpGet]
		public IActionResult LogFailedAuthentication() => PartialView();

		[HttpGet]
		public async Task<IActionResult> Read_LogUserAuthorization(DataSourceFilter data)
		{
			var dataResult = await _logService.GetLogUserAuthorization(data);
			if (dataResult.Status == 0)
				return Ok(dataResult.Data);
			else return Ok();
		}

		[HttpGet]
		public async Task<IActionResult> Read_LogFailedAuthentication(DataSourceFilter data)
		{
			var dataResult = await _logService.GetLogFailedAuthentication(data);
			if (dataResult.Status == 0)
				return Ok(dataResult.Data);
			else return Ok();
		}
	}
}
