﻿

using eKosovaInternal.Interfaces;
using MailKit;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace eKosovaInternal.Controllers
{
    
    public class PublicUserHistoryController : Controller
    {
        private readonly IPublicUserHistoryService _userHistoryService;

        public PublicUserHistoryController(IPublicUserHistoryService userHistoryService)
        {
            _userHistoryService = userHistoryService;
        }

		[HttpGet]
		public ActionResult Index() => View();


        [HttpGet]
		public async Task<IActionResult> Data(int publicUserID)
		=> Ok(await _userHistoryService.GetPublicUserHistory(publicUserID));
	}
}
