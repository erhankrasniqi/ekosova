﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Domain;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Interfaces;
using eKosovaInternal.SharedModel.PublicUsers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternal.Controllers
{
	public class PublicUserController : Controller
	{
		private readonly IPublicUserService _userService;

		public PublicUserController(IPublicUserService userService)
		{
			_userService = userService;
		}
		[HttpGet]
		public IActionResult Index() => View();

		[HttpGet]
		public async Task<IActionResult> Data()
		{
			//C:\Users\Admin\Documents\stage\eKosova\eKosovaBackend\Images\
			var users = await _userService.GetPublicUser();

			return Ok(users);
		}

		[HttpPost]
		public async Task<IActionResult> UserPictures([FromBody] BaseIdentification personalNumber) {
			var nrPersonal = personalNumber.Identification;
			var result = await _userService.GetPublicUserPersonalNumber(personalNumber.Identification);
			var user = result.First();
			var base64 = new Base64Images
			{
				FrontID = DecryptBase64(user.PathIDFront),
				BackID = DecryptBase64(user.PathIDBack),
				Selfie = DecryptBase64(user.PathSelfie),
				BizDokument = DecryptBase64(user.PathBusinessCertificate)
			};
			return Ok(base64);
		}

		private string DecryptBase64(string path)
		{
			var tempPath = "F:\\Team Projects\\eKosova\\eKosovaBackend\\";
			// definohet ma von ne appsettings.json dhe bahet load
			byte[] fileBytes;
			try
			{
				var storedFile = System.IO.File.ReadAllBytes(tempPath + path);
				fileBytes = eKosovaInternal.Domain.ImageEncryption.DecryptBytes(storedFile);
			}
			catch
			{
				fileBytes = System.IO.File.ReadAllBytes(tempPath + "Images\\Users\\404-unenc.png");
			}
			return Convert.ToBase64String(fileBytes);
		}

		//Change later from form
		[HttpPost]
		public ActionResult Update([FromBody] UpdatePublicUserModel _) => Ok(_userService.UpdatePublicUsers(_).Result);


	}
}
