﻿using eKosovaInternal.Domain.Helps;
using eKosovaInternal.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Controllers
{
    public class HelpController : Controller
    {
        private readonly IHelpService _service;
        public HelpController(IHelpService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> Data()
        {
            var model = await _service.GetActiveHelps();
            return Ok(model);
        }

        [HttpGet]
        public ActionResult Index() => View();

        public async Task<IActionResult> Edit([FromBody]Help _)
        {
            await _service.CreateHelpNotification(_);
            return RedirectToAction("Index", "Help");
        }
    }
}
