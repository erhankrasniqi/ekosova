﻿using System.Threading.Tasks;
using eKosovaInternal.Custom.Helpers;
using eKosovaInternal.Domain.Contacts;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternal.Controllers
{
	[Authorize]
	public class ContactController : Controller
	{

		private readonly IContactService _service;
		private readonly IHttpContextAccessor _contextAccessor;

		public ContactController(IContactService service, IHttpContextAccessor contextAccessor)
		{
			_service = service;
			_contextAccessor = contextAccessor;
		}

		[HttpGet]
		public async Task<IActionResult> Data()
			=> Ok(await _service.GetActiveContacts());

		[HttpGet]
		public ActionResult Index()
		{
			var entryUser = _contextAccessor.HttpContext.User.Identity.Name;
			return View();
		}

		[HttpGet]
		public async Task<IActionResult> Test()
		{
			var eu = await ApiRequestHelper<bool>.Get("api/internal/hajt", true);
			return Ok(eu.Data);
		}

		public IActionResult EditC(int ID)
			=> View("EditContactModal", ID);

		public async Task<IActionResult> Edit([FromBody] Contact _)
		{
			await _service.CreateContactNotification(_);
			return RedirectToAction("Index", "Contact");
		}
	}
}
