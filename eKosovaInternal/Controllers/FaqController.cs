﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Faqs;
using eKosovaInternal.Interfaces;
using eKosovaInternal.SharedModel.Faqs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eKosovaInternal.Domain.SeedWork;

namespace eKosovaInternal.Controllers
{
	public class FaqController : Controller
	{
		private readonly IFaqService _faqService;

		public FaqController(IFaqService faqService)
		{
			_faqService = faqService;
		}

		[HttpGet]
		public IActionResult Index() => View();

		[HttpGet]
		public async Task<IActionResult> Data()
		=> Ok(await _faqService.GetFaqs());

		public IActionResult AddQuestion() => View();

		//[HttpPost]
		//public ActionResult Create([FromBody] FaqModel _) => Ok(_faqService.CreateFaq(_).Result);


		[HttpPost]
		public async Task<IActionResult> AddFaqs([FromForm] CreateFaqModel faq)
		{
			await _faqService.CreateFaq(faq);
			//faq.Answer = faq.Answer.Replace("&gt;", ">").Replace("&lt;", "<");
			return RedirectToAction("Index", "Faq");
		}

		[HttpPost]
		public async Task<IActionResult> EditFaqs([FromBody] EditFaqModel faq)
		{
			try
			{
				await _faqService.EditFaq(faq);
				return Ok();
				///RedirectToAction("Index", "Faq");
			}
			catch (Exception e)
			{
				throw;
			}
		}

		//public async Task<IActionResult> EditFaqs([FromBody] EditFaqModel faq)
		//{
		//    await _faqService.EditFaq(faq);
		//    return RedirectToAction("Index", "Faq");
		//}

		public async Task<IActionResult> Delete(BaseEntity baseEntity) => Ok(await _faqService.DeleteFaq(baseEntity.ID));
	}
}
