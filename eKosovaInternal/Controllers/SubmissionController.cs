﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternal.Controllers
{
    public class SubmissionController : Controller
    {
        private readonly IInsertSubmissionService _getSubmission;

        public SubmissionController(IInsertSubmissionService getSubmission)
        {
            _getSubmission = getSubmission;
        }
		[HttpGet]
		public IActionResult Index() => View();

		[HttpGet]
        public async Task<IActionResult> Data(int personalNumber)
        {
           var result = await _getSubmission.GetSubmissionByPersonalnumber(personalNumber);
           // return Ok( new { Data = result.Data.FirstOrDefault(), result.Status });
             
            //foreach (var unit in result)
            //{
            //    unit.Base64 = new eKosovaInternal.Domain.Base64Images
            //    {
            //        FrontID = DecryptBase64(unit.PathIDFront),
            //        BackID = DecryptBase64(unit.PathIDBack),
            //        Selfie = DecryptBase64(unit.PathSelfie),
            //        BizDokument = DecryptBase64(unit.PathBusinessCertificate)
            //    };
            //}
            return Ok(new { Data = result.Data.FirstOrDefault(), result.Status });
        }
        private string DecryptBase64(string path)
        {
            var tempPath = "F:\\Team Projects\\eKosova\\eKosovaBackend\\";
            // definohet ma von ne appsettings.json dhe bahet load
            byte[] fileBytes;
            try
            {
                var storedFile = System.IO.File.ReadAllBytes(tempPath + path);
                fileBytes = eKosovaInternal.Domain.ImageEncryption.DecryptBytes(storedFile);
            }
            catch
            {
                fileBytes = System.IO.File.ReadAllBytes(tempPath + "Images\\Users\\404-unenc.png");
            }
            return Convert.ToBase64String(fileBytes);
        }
    }
}
