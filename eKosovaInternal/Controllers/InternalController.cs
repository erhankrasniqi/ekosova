﻿using eKosovaInternal.Custom.Validators;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Domain.Roles;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Controllers
{
	public class InternalController : Controller
	{
		private readonly IInternalUserService _intService;

		private readonly IRoleAccessService _roleService;

		private readonly IHierarchyService _hierachyService;

		private readonly IUserAuthorizationService _authorizationTypesService;

		private static InternalUserIntro _intro;

		public InternalController(IInternalUserService internalUserService, IRoleAccessService roleService, IHierarchyService hierachyService, IUserAuthorizationService authorizationTypesService)
		{
			_intService = internalUserService;
			_roleService = roleService;
			_hierachyService = hierachyService;
			_authorizationTypesService = authorizationTypesService;
			_intro = new InternalUserIntro
			{
				ViewADDomains = _intService.GetDomains().Result,
				ViewRoles = _roleService.GetRoles().Result.Data.Select(_ => new RoleInternalRegister { ID = _.Id, Title = _.Title }),
				ViewHierarchy = _hierachyService.GetHierarchy().Result,
				ViewAuthorizationTypes = _authorizationTypesService.GetAuthorizations().Result
			};
		}

		public IActionResult Index() => View(_intro);

		public async Task<IActionResult> Data() => Ok(await _intService.GetAll());

		public IActionResult Create() => View(_intro);

		public IActionResult Test() => View();

		public async Task<IActionResult> Update([FromBody] InternalUserViewModel model)
		{
			await _intService.Update(model);
			
			return Ok(model);
		}

		[HttpPost]
		public async Task<IActionResult> UserExists([FromBody] BaseString hello)
			=> Ok((await _intService.CheckAccount(hello.Value)).SingleData);

		public async Task<IActionResult> CreateInternal(InternalUserCreate internalUser)
		{
			string msg;
			try
			{
				var validator = new InternalUserValidator().Validate(internalUser);
				if (validator.IsValid)
				{
					var result = await _intService.Create(internalUser);
					msg = result.Status == 0 ? "Zyrtari është shtuar me sukses" : "Shtimi i zyrtarit ka dështuar";
				}
				else
					throw new System.Exception("INVALID MODEL");
			}
			catch
			{
				msg = "Të dhënat nuk janë valide";
			}
			ModelState.AddModelError("Info", msg);
			return View("Index");
		}
	}
}
