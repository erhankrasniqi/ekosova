﻿using eKosovaInternal.Domain.Roles;
using eKosovaInternal.Interfaces;
using eKosovaInternal.SharedModel.Roles;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Controllers
{
	public class AdministrationController : Controller
	{
		private readonly IRoleAccessService _service;
		public AdministrationController(IRoleAccessService service)
		{
			_service = service;
		}
		public IActionResult Index()
		{
			return View();
		}
		public async Task<IActionResult> GetModules(int ID, bool? isFrom = true)
		{
			ViewBag.IsFromRole = isFrom;
			var result = (await _service.GetModules(ID)).Data.ToList();
			var data = await _service.GetModulesTree(result);
			return PartialView(data);
		}
		public async Task<IActionResult> GetModulesForUser(int ID)
		{
			var result = (await _service.GetModulesForUser(ID)).Data.ToList();
			var data = await _service.GetModulesTree(result);
			return PartialView(data);
		}

		public async Task<IActionResult> Roles()
		{
			var result = await _service.GetRoles();
			return View(result.Data.OrderBy(_ => _.Id).ToList());
		}

		public async Task<IActionResult> EditRole(EditRoleModel data, int id)
		{
			data.IsActive = true;
			var result = await _service.UpdateRole(data, id);
			return Ok(result);
		}

		public async Task<IActionResult> DeleteRole(int ID)
		{
			var result = await _service.DeleteRole("Role", ID);
			return Ok(result);
		}

		public async Task<IActionResult> AddRole(NewRoleModel data)
		{
			var result = await _service.CreateRole(data);
			return Ok(result.Data.FirstOrDefault());
		}

		public async Task<IActionResult> SetRoleAccess(EditRoleAuthorizationModel data, int id)
		{			
			var result = await _service.UpdateRoleAuthorization(data, id);
			return PartialView(result);
		}
	}
}
