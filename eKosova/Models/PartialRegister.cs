﻿using eKosova.Domain.SeedWork;

namespace eKosova.Models
{
	public class PartialRegister : BaseIdentification
	{
		public string Phone { get; set; }
		public string PersonalNumber { get; set; }
		public string Password { get; set; }
	}

	public class ResetPassword : BaseIdentification
	{
		public string OldPassword { get; set; }
		public string Password { get; set; }
	}
}
