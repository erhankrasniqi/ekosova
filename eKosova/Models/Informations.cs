﻿namespace eKosova.Models
{
	public class Informations
    {
        public string Category { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
