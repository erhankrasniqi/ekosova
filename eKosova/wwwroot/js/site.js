﻿//custom dropdown
//example $("#Profile").setDataSource([{ text: "option 1", value: 2, disabled: true }]);
jQuery.fn.extend({
	setDataSource: function (datasource) {
		if (!this.is("select"))
			throw new Error("Element is not a select element");
		if (!datasource)
			throw new Error("No datasource parameter was given, if empty datsource was intended add an empty array as a parameter");
		if (!datasource.length) {
			this.next().next().html('');
			this.html('');
			return;
		}
		let options = '';
		let selectOptions = '';
		for (let item of datasource) {
			if (!(item.text && item.value))
				throw new Error("Datasource item must have defined text and value properties");
			options += `<span class='option ${item.disabled ? 'disabled' : ''} ${item.selected ? 'selected' : ''}' data-value=${item.value}>${item.text}</span>`;
			selectOptions += `<option value=${item.value} ${item.selected ? 'selected' : ''}></option>`;
		}
		this.html(selectOptions);
		this.next().next().html(options);
	}
});

const isMobileDevice = () => (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) == true;

const isImage = file => file['type'].includes('image');

$(document).click(e => !$(e.target).hasClass('disabled') ? $(`.custom-dropdown:not([data-id=${e.target.parentElement.getAttribute('data-id')}])`).removeClass('open') : null);

$('.btn-option').click(e => {
	$(e.target.parentElement).toggleClass('open');
	if (isMobileDevice())
		$('.btn-option').next().bind('click', function () {
			$(this).parent().removeClass('open');
			$(this).unbind('click');
		});
});

$(".custom-dropdown .options span.option").not('.disabled').click(function () {
	if ($(this).hasClass('disabled'))
		return;
	$('.custom-dropdown .options span.option').removeClass('selected');
	$(this).addClass('selected');
	$(this).parent().prev().text($(this).text());
	$(this).parent().parent().find(`select`).val($(this).attr('data-value')).trigger('change');
});

//service filter
$('input.search').keyup(function () {
	if ($(this).val() == "") {
		$('.sv-content a.btn-service').each(function (e) {
			$(this).removeClass('hidden');
		});
		return;
	}
	let value = $(this).val().toLowerCase();
	$('.sv-content a.btn-service').each(function (e) {
		if (!$(this).find('p').text().toLowerCase().trim().includes(value))
			$(this).addClass('hidden');
		else $(this).removeClass('hidden');
	});
});

//underservices filter
$('input.search').keyup(function () {
	if ($(this).val() == "") {
		$('.mf-content div').each(function (e) {
			$(this).removeClass('hidden');
		});
		return;
	}
	let value = $(this).val().toLowerCase();
	$('.mf-content div').each(function (e) {
		if (!$(this).find('a').find('h1').text().toLowerCase().trim().includes(value))
			$(this).addClass('hidden');
		else $(this).removeClass('hidden');
	});
});

//loader show
$("a").not("[href='#'], [href^='tel'], [href^='mailto'], [href='javascriopt(void:0)'], [target='_blank']").click(() => $('.loader').show());

//file change
$(".file input[type='file']").change(function () {
	let files = $(this).get(0).files;
	if (files.length > 0) {
		$(this).prev().text("");
		[...files].forEach(f => $(this).prev().text($(this).prev().text() + ", " + f.name));
		$(this).prev().text($(this).prev().text().substr(1));
		$(this).parent().addClass('done');
	}
	else {
		if ($(this).attr('data-default-text'))
			$(this).prev().text($(this).attr('data-default-text'));
		else
			$(this).prev().text("Kliko këtu për të ngarkuar nje fajll");
		$(this).parent().removeClass('done');
	}
});

function initMap() {
	// Create a new StyledMapType object, passing it an array of styles,
	// and the name to be displayed on the map type control.
	var styledMapType = new google.maps.StyledMapType(
		[
			{
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#7c92c9"
					}
				]
			},
			{
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#8ec3b9"
					}
				]
			},
			{
				"elementType": "labels.text.stroke",
				"stylers": [
					{
						"color": "#1a3646"
					}
				]
			},
			{
				"featureType": "administrative.country",
				"elementType": "geometry.stroke",
				"stylers": [
					{
						"color": "#4b6878"
					}
				]
			},
			{
				"featureType": "administrative.land_parcel",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#64779e"
					}
				]
			},
			{
				"featureType": "administrative.province",
				"elementType": "geometry.stroke",
				"stylers": [
					{
						"color": "#4b6878"
					}
				]
			},
			{
				"featureType": "landscape.man_made",
				"elementType": "geometry.stroke",
				"stylers": [
					{
						"color": "#ffffff"
					}
				]
			},
			{
				"featureType": "landscape.natural",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#023e58"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#283d6a"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#ffffff"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "labels.text.stroke",
				"stylers": [
					{
						"color": "#1d2c4d"
					}
				]
			},
			{
				"featureType": "poi.business",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "geometry.fill",
				"stylers": [
					{
						"color": "#023e58"
					}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "labels.text",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#3C7680"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#304a7d"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#98a5be"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "labels.text.stroke",
				"stylers": [
					{
						"color": "#1d2c4d"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#2c6675"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "geometry.stroke",
				"stylers": [
					{
						"color": "#255763"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#b0d5ce"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "labels.text.stroke",
				"stylers": [
					{
						"color": "#023e58"
					}
				]
			},
			{
				"featureType": "transit",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#98a5be"
					}
				]
			},
			{
				"featureType": "transit",
				"elementType": "labels.text.stroke",
				"stylers": [
					{
						"color": "#1d2c4d"
					}
				]
			},
			{
				"featureType": "transit.line",
				"elementType": "geometry.fill",
				"stylers": [
					{
						"color": "#283d6a"
					}
				]
			},
			{
				"featureType": "transit.station",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#3a4762"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
					{
						"color": "#0e1626"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#4e6d70"
					}
				]
			}
		],
		{ name: 'Styled Map' });

	// Create a map object, and include the MapTypeId to add
	// to the map type control.
	var map = new google.maps.Map(document.querySelector('.map'), {
		center: { lat: 42.659786, lng: 21.1551374 },
		zoom: 18,
		mapTypeControlOptions: {
			mapTypeIds: ['styled_map']
		}
	});

	//Associate the styled map with the MapTypeId and set it to display.
	map.mapTypes.set('styled_map', styledMapType);
	map.setMapTypeId('styled_map');
}

$(document).ready(function () {
	// profile menu redirecter
	$("#Profile").change(function () {
		var selected = $(this).val();
		switch (selected) {
			case '0':
				window.location.href = "/Profile";
				break;
			case '1':
				window.location.href = "/History";
				break;
			case '2':
				window.location.href = "/Profile/Logout";
				break;
		}
	});

	// show notifications
	$("#btnNotification").click(function () {
		$("#notificationContainer").fadeToggle(300);
		$("#notification_count").fadeOut("slow");
		return false;
	});

	// hide on document click
	$(document).click(function () {
		$("#notificationContainer").hide();
	});

	// popup
	$("#notificationContainer").click(function () {
		return false;
	});

	$("#notificationFooter").on('click', function () { window.location.href = '/Profile/Notifications'; });
});

String.prototype.isNumber = function () { return /^\d+$/.test(this); }

// only albanian letters & space
function alphaOnly(r) { return r.charCode >= 65 && r.charCode <= 90 || r.charCode >= 97 && r.charCode <= 122 || 199 === r.charCode || 203 === r.charCode || 231 === r.charCode || 235 === r.charCode || 32 === r.charCode }

jQuery.validator.addMethod("onlyLetters", function () {
	return /^([a-zA-Z]+\s)*[a-zA-Z]+$/.test($('#FullName').val());
});

jQuery.validator.addMethod("modulo11", function () {
	var personalNumber = $('#PersonalNumber').val();
	var result = false;
	if (personalNumber.length == 10 && personalNumber.isNumber()) {
		var newValue = personalNumber.substring(0, 9);
		var checkDigit = parseInt(personalNumber.substring(9, 10));
		var sum = 0;
		var j = 2;
		for (var i = newValue.length - 1; i >= 0; i--) {
			var temp = parseInt(newValue[i].toString());
			sum += temp * j;
			j++;
			if (j > 7)
				j = 2;
		}
		var reminder = 11 - (sum % 11);
		result = parseInt(reminder.toString().substring(reminder.toString().Length - 1, 1)) == checkDigit;
	}
	return result;
});

jQuery.validator.addMethod("phoneFormat", function () {
	return /^\+383\s\(\d{2}\)\s\d{3}-\d{3}$/.test($('#Phone').val());
});

var True = true; var False = false;