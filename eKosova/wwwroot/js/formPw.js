﻿$('#pwd-result').hide();

$("input[type=password]").on('keyup', function () {
	$("input[type=password]").css('border', '0');
	$('#pwd-result').toggle($('#Password').val() != '');
	var passwordStrength = checkStrength($('#Password').val());
	$('#pwd-result').html(translateStrength(passwordStrength));
});

function checkStrength(password) {
	var strength = 0
	if (password.length > 7) strength += 1
	if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
	if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
	if (password.match(/([!,%,&,#,$,^,*,?,_,~])/)) strength += 1
	if (password.match(/(.*[!,%,&,#,$,^,*,?,_,~].*[!,%,&,#,$,^,*,?,_,~])/)) strength += 1
	if (strength < 2)
		return 1
	else if (strength == 2)
		return 2
	else
		return 3
}

function translateStrength(e) {
	switch (e) {
		case 0: return '<div id="pwd-weak">Fjalëkalimet nuk përputhen</div>';
		case 1: return '<div id="pwd-weak">Fjalëkalimi i dobtë</div>';
		case 2: return '<div id="pwd-ok">Fjalëkalimi i mjaftueshëm</div>';
		case 3: return '<div id="pwd-strong">Fjalëkalimi i sigurtë</div>';
	}
}

jQuery.validator.addMethod("strongEnough", function () {
	return checkStrength($('#Password').val()) >= 2;
});

jQuery.validator.addMethod("passwordsMatch", function () {
	if ($('#Password').val() == $('#ConfirmPassword').val())
		return true;
	else {
		$("input[type=password]").css('border', '1px solid red');
		$('#pwd-result').html(translateStrength(0));
		return false;
	};
});
