﻿function count(seconds, id, done, alternative) {
	document.getElementById(done).setAttribute('style', 'display: none');
	document.getElementById(id).parentElement.setAttribute('style', 'display: block');
	var counter = new Countdown({
		seconds: seconds || 40,
		onUpdateStatus: function (sec) { document.getElementById(id).innerHTML = (sec > 60 ? Math.floor(sec / 60) + 'm' + sec % 60 + 's' : sec + 's'); },
		onCounterEnd: function () {
			document.getElementById(id).parentElement.setAttribute('style','display: none');
			document.getElementById(done).setAttribute('style', 'display: block');
		} || alternative
	});
	counter.start();
}

function Countdown(options) {
	var timer,
		instance = this,
		seconds = options.seconds || 10,
		updateStatus = options.onUpdateStatus || function () { },
		counterEnd = options.onCounterEnd || function () { };

	function decrementCounter() {
		updateStatus(seconds);
		if (seconds === 0) {
			counterEnd();
			instance.stop();
		}
		seconds--;
	}

	this.start = function () {
		clearInterval(timer);
		timer = 0;
		seconds = options.seconds;
		timer = setInterval(decrementCounter, 1000);
	};

	this.stop = function () {
		clearInterval(timer);
	};
}