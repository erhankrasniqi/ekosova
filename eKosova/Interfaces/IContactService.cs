﻿using eKosova.Domain;
using eKosova.SharedModel.Contacts;
using System.Threading.Tasks;

namespace eKosova.Interfaces
{
	public interface IContactService
    {
        Task<ApiResponse<CreateContactModel>> CreateContact(CreateContactModel _);
    }
}
