﻿using eKosova.Domain;
using eKosova.SharedModel.Helps;
using System.Threading.Tasks;

namespace eKosova.Interfaces
{
	public interface IHelpService
    {
        Task<ApiResponse<CreateHelpModel>> CreateHelp(CreateHelpModel _);

    }
}
