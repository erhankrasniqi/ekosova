﻿using eKosova.Domain;
using eKosova.Domain.Informations;
using System.Threading.Tasks;

namespace eKosova.Interfaces
{
	public interface IInformationViewService
    {
        Task<ApiResponse<Information>> GetInformations();
    } 
}
