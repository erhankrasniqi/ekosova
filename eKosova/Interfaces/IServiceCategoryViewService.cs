﻿using eKosova.Domain;
using eKosova.Domain.ServiceCategories;
using System.Threading.Tasks;

namespace eKosova.Interfaces
{
	public interface IServiceCategoryViewService
	{
		Task<ApiResponse<ServiceCategory>> GetServices();
		Task<ApiResponse<ServiceSubCategory>> GetSubServices(int ID);
	}
}
