﻿using eKosova.Domain;
using eKosova.SharedModel.Faqs;
using System.Threading.Tasks;

namespace eKosova.Interfaces
{
	public interface IFaqViewService
    {
        Task<ApiResponse<FaqModel>> GetFaqs(); 
    }
}
