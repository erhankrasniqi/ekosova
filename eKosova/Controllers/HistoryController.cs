﻿using eKosova.Custom.Attributes;
using eKosova.Custom.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosova.Controllers
{
	[Authorize]
	public class HistoryController : Controller
	{
		public IActionResult Index() => View();
	}
}
