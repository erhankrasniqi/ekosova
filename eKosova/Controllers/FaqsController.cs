﻿using System.Collections.Generic;
using System.Threading.Tasks;
using eKosova.Custom.Helpers;
using eKosova.Interfaces;
using eKosova.SharedModel.Faqs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace eKosova.Controllers
{
	public class FaqsController : Controller
	{
		private readonly IFaqViewService _faqViewService;

		private readonly IMemoryCache _cache;
		public FaqsController(IFaqViewService faqViewService, IHttpContextAccessor contextAccessor, IMemoryCache cache)
		{
			_faqViewService = faqViewService;
			LogedUser.SetHttpContextAccessor(contextAccessor);
			_cache = cache;
		}

		[HttpGet]
		public IActionResult Index() => View();

		[HttpGet]
		public async Task<IActionResult> Data()
		{
			try
			{
				if (!_cache.TryGetValue(CacheKeys.Faqs, out IList<FaqModel> faq))
				{
					faq = (await _faqViewService.GetFaqs()).Data;
					_cache.Set(CacheKeys.Faqs, faq, CacheKeys.Options);
				}
				return Ok(faq);
			}
			catch
			{ return Ok(); }
		}
	}
}