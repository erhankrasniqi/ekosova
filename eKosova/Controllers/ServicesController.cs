﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using eKosova.Custom.Helpers;
using eKosova.Domain.ServiceCategories;
using eKosova.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection.Services;

namespace eKosova.Controllers
{
	public class ServicesController : Controller
	{
		private readonly IMemoryCache _cache;

		private readonly IServiceCategoryViewService _viewService;

		private readonly IDetectionService _detectionService;

		private readonly bool isMobile;

		public ServicesController(IDetectionService detectionService, IServiceCategoryViewService viewService, IMemoryCache memoryCache, IHttpContextAccessor contextAccessor)
		{
			_detectionService = detectionService;
			isMobile = _detectionService.Device.Type == Wangkanai.Detection.Models.Device.Mobile;
			_viewService = viewService;
			_cache = memoryCache;
			LogedUser.SetHttpContextAccessor(contextAccessor);
		}

		public ActionResult Index() => View();

		[Route("Service/{service}")]
		public ActionResult Service(int service) => View($"~/Views/Services/SubServices/SubServices{(isMobile ? ".mobile" : "")}.cshtml", service);

		public async Task<IActionResult> Data()
		{
			try
			{
				if (!_cache.TryGetValue(CacheKeys.Services, out IList<ServiceCategory> services))
				{
					services = (await _viewService.GetServices()).Data;
					_cache.Set(CacheKeys.Services, services, CacheKeys.Options);
				}
				return Ok(services);
			}
			catch { return Ok(); }
		}

		[Route("Service/{service}/Data")]
		public async Task<IActionResult> SubServicesData(int service)
		{
			if (!_cache.TryGetValue(CacheKeys.SubServices + service, out IList<ServiceSubCategory> subCategories))
			{
				subCategories = (await _viewService.GetSubServices(service)).Data;
				_cache.Set(CacheKeys.SubServices + service, subCategories, CacheKeys.Options);
			}
			return Ok(subCategories);
		}

		public ActionResult PaymentInfo() => View($"~/Views/Services/PaymentInfo/PaymentInfo{(isMobile ? ".mobile" : "")}.cshtml");
		public ActionResult OnlinePayment() => View($"~/Views/Services/OnlinePayment/OnlinePayment{(isMobile ? ".mobile" : "")}.cshtml");
		public ActionResult ConfirmPayment() => View($"~/Views/Services/ConfirmPayment/ConfirmPayment{(isMobile ? ".mobile" : "")}.cshtml");
	}
}