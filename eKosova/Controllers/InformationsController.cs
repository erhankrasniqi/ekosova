﻿using System.Collections.Generic;
using System.Threading.Tasks;
using eKosova.Custom.Helpers;
using eKosova.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection.Services;

namespace eKosova.Controllers
{
	//[RequireHttps]
	public class InformationsController : Controller
	{
		private readonly IInformationViewService _informationViewService;
		private readonly IDetectionService _detectionService;
		private bool isMobile;
		private readonly IMemoryCache _cache;

		public InformationsController(IInformationViewService informationViewService, IDetectionService detectionService, IMemoryCache cache, IHttpContextAccessor contextAccessor)
		{
			_informationViewService = informationViewService;
			_detectionService = detectionService;
			isMobile = _detectionService.Device.Type == Wangkanai.Detection.Models.Device.Mobile;
			_cache = cache;
			LogedUser.SetHttpContextAccessor(contextAccessor);
		}

		public IActionResult Index() => View("Index" + (isMobile ? ".mobile" : ""));

		public IActionResult Open(int ID) => View("Open" + (isMobile ? ".mobile" : ""), ID);

		public async Task<IActionResult> Data()
		{
			try
			{
				if (!_cache.TryGetValue(CacheKeys.Informations, out IList<Domain.Informations.Information> informations))
				{
					informations = (await _informationViewService.GetInformations()).Data;
					_cache.Set(CacheKeys.Informations, informations, CacheKeys.Options);
				}
				return Ok(informations);
			}
			catch { return Ok(); }
		}
	}
}