﻿using System.Threading.Tasks;
using eKosova.Custom.Helpers;
using eKosova.Interfaces;
using eKosova.SharedModel.Contacts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wangkanai.Detection.Services;

namespace eKosova.Controllers
{
	public class ContactController : Controller
	{
		private readonly IDetectionService _detectionService;
		private readonly bool isMobile;
		private readonly IContactService _contact;
		public ContactController(IContactService contact, IHttpContextAccessor contextAccessor, IDetectionService detectionService)
		{
			_detectionService = detectionService;
			_contact = contact;
			isMobile = _detectionService.Device.Type == Wangkanai.Detection.Models.Device.Mobile;
			LogedUser.SetHttpContextAccessor(contextAccessor);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> AddContact([FromForm] CreateContactModel contact)
		{
			string message;
			try
			{
				if (!new CreateContactValidator().Validate(contact).IsValid)
					throw new System.Exception("INVALID");

				var response = await _contact.CreateContact(contact);

				if (response.Status != 0)
					throw new System.Exception("SAVE_FAILED");

				message = "Mesazhi i juaj është pranuar";
			}
			catch
			{
				message = "Mesazhi i juaj nuk është pranuar, ju lutem provoni më vone";
			}
			ModelState.AddModelError("Received", message);
			return View($"~/Views/Home/Contact/Contact{(isMobile ? ".mobile" : "")}.cshtml");
		}
	}
}
