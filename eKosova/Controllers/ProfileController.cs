﻿using eKosova.Custom;
using eKosova.Custom.Attributes;
using eKosova.Custom.Helpers;
using eKosova.Custom.Validators;
using eKosova.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace eKosova.Controllers
{
	[Authorize]
	public class ProfileController : Controller
	{
		public ProfileController(IHttpContextAccessor contextAccessor)
		{
			LogedUser.SetHttpContextAccessor(contextAccessor);
		}

		public ActionResult Index()
			=> View();

		public ActionResult Notifications(int? ID)
			=> View(ID ?? 0);

		public async Task<IActionResult> Update(ResetPassword reset)
		{
			string showMessage;
			try
			{
				if (!new PasswordUpdateValidator().Validate(reset).IsValid)
					throw new System.Exception("INVALID");

				if (!await UserHelper.UpdatePassword(reset))
					throw new System.Exception("WRONGPASS");
				showMessage = "Fjalëkalimi është ndërruar me sukses";
			}
			catch
			{
				showMessage = "Fjalëkalimi i tanishëm është gabim";
			}
			ModelState.AddModelError("Credentials", showMessage);
			return View("Index");
		}

		public async Task<IActionResult> Logout()
		{
			await UserHelper.ClearTokens(LogedUser.GetLoggedUserId());
			await HttpContext.SignOutAsync();
			//LogedUser.DeleteUserSession();
			return RedirectToAction("Index", "Home");
		}
	}
}
