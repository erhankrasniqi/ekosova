﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using eKosova.Models;
using Wangkanai.Detection.Services;
using Microsoft.AspNetCore.Http;
using eKosova.Custom.Helpers;
using eKosova.Interfaces;
using System.Threading.Tasks;
using eKosova.Business;
using System;

namespace eKosova.Controllers
{
	//[RequireHttps]
	public class HomeController : Controller
	{
		private readonly IDetectionService _detectionService;
		private readonly bool isMobile;
		public HomeController(
			IDetectionService detectionService,
			IHttpContextAccessor contextAccessor)
		{
			_detectionService = detectionService;
			isMobile = _detectionService.Device.Type == Wangkanai.Detection.Models.Device.Mobile;
			LogedUser.SetHttpContextAccessor(contextAccessor);
		}
		public IActionResult Index() => View($"~/Views/Home/Index{(isMobile ? ".mobile" : "")}.cshtml");
		public IActionResult FAQ() => View($"~/Views/Home/FAQ/FAQ{(isMobile ? ".mobile" : "")}.cshtml");
		public IActionResult Help() => View($"~/Views/Home/Help/Help{(isMobile ? ".mobile" : "")}.cshtml");
		public IActionResult Contact() => View($"~/Views/Home/Contact/Contact{(isMobile ? ".mobile" : "")}.cshtml");

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error() => View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
	}
}