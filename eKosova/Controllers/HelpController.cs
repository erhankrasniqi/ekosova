﻿using System.Threading.Tasks;
using eKosova.Custom.Helpers;
using eKosova.Interfaces;
using eKosova.SharedModel.Helps;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wangkanai.Detection.Services;

namespace eKosova.Controllers
{
	public class HelpController : Controller
	{
		private readonly IDetectionService _detectionService;
		private readonly bool isMobile;
		private readonly IHelpService _help;
		public HelpController(IHelpService help, IHttpContextAccessor contextAccessor, IDetectionService detectionService)
		{
			_detectionService = detectionService;
			isMobile = _detectionService.Device.Type == Wangkanai.Detection.Models.Device.Mobile;
			_help = help;
			LogedUser.SetHttpContextAccessor(contextAccessor);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> AddHelp([FromForm] CreateHelpModel help)
		{
			string message;
			try
			{
				if (!new CreateHelpValidator().Validate(help).IsValid)
					throw new System.Exception("INVALID");

				var response = await _help.CreateHelp(help);

				if (response.Status != 0)
					throw new System.Exception("SAVE_FAILED");

				message = "Mesazhi i juaj është pranuar";
			}
			catch
			{
				message = "Mesazhi i juaj nuk është pranuar, ju lutem provoni më vone";
			}
			ModelState.AddModelError("Received", message);
			return View($"~/Views/Home/Help/Help{(isMobile ? ".mobile" : "")}.cshtml");
		}
	}
}
