﻿using System;
using System.Linq;
using System.Threading.Tasks;
using eKosova.Custom.Helpers;
using eKosova.Domain;
using eKosova.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wangkanai.Detection.Services;
using eKosova.Custom;
using eKosova.Domain.SeedWork;
using eKosova.Custom.Attributes;
using eKosova.Custom.Validators;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Newtonsoft.Json;
using eKosova.Business;

namespace eKosova.Controllers
{
	//[RequireHttps]
	[AlreadyLoggedIn]
	public class SecurityController : Controller
	{
		private readonly IDetectionService _detectionService;
		private bool isMobileDevice;
		public SecurityController(IDetectionService detectionService, IHttpContextAccessor contextAccessor)
		{
			_detectionService = detectionService;
			isMobileDevice = _detectionService.Device.Type == Wangkanai.Detection.Models.Device.Mobile;
			LogedUser.SetHttpContextAccessor(contextAccessor);
		}

		public ActionResult Index()
			=> View();

		public ActionResult Registration()
			=> View($"~/Views/Security/Registration/Registration{(isMobileDevice ? ".mobile" : "")}.cshtml");

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Verification(PartialRegister partial)
		{
			try
			{
				if (!new PartialRegisterValidator().Validate(partial).IsValid)
				{
					ModelState.AddModelError("RegistrationError", "Të dhënat nuk janë valide");
					return View($"~/Views/Security/Registration/Registration{(isMobileDevice ? ".mobile" : "")}.cshtml");
				}

				var response = await UserHelper.NewPublicUser(partial);

				if (response.Status == 0)
				{
					var repodUser = response.Data.First();
					var state = await UserHelper.AccountState(repodUser.Identification);
					return View($"~/Views/Security/Verification/Verification{(isMobileDevice ? ".mobile" : "")}.cshtml", state);
				}
				else if (response.Status == 7)
				{
					ModelState.AddModelError("Credentials", "Tashmë keni llogari në eKosova dhe mund të kyqeni");
					return View($"~/Views/Security/Index{(isMobileDevice ? ".mobile" : "")}.cshtml");
				}
				throw new Exception("kick to catch"); // every other statusCode
			}
			catch
			{
				ModelState.AddModelError("Credentials", "Regjistrimi ka dështuar. Ju lutemi provoni përsëri");
				return View($"~/Views/Security/Index{(isMobileDevice ? ".mobile" : "")}.cshtml");
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Confirmation(Images images)
		{
			try
			{
				await UserHelper.ConfirmationChecks(images);

				return View($"~/Views/Security/Confirmation/Confirmation.cshtml",
					new SMSTokenModel
					{
						Identification = images.Identification,
						Validity = (int)TimeSpan.FromMinutes(2).TotalSeconds
					});
			}
			catch (MissingFieldException)
			{
				ModelState.AddModelError("Credentials", "Dërgimi i mesazhit për verifikim ka dështuar, ju lutem provoni më vonë");
				return View($"~/Views/Security/Index{(isMobileDevice ? ".mobile" : "")}.cshtml");
			}
			catch (ObjectDisposedException)
			{
				ModelState.AddModelError("Credentials", "Llogaria juaj në eKosova është krijuar dhe ka kaluar në procesin e verifikimit. Përdorni \"Numrin Personal\" dhe \"Fjalëkalimin\" për të vazhduar më tutje.");
				return View($"~/Views/Security/Index{(isMobileDevice ? ".mobile" : "")}.cshtml");
			}
			catch (Exception e)
			{
				var message = e.Message switch
				{
					"ModelState" => "Një apo më shumë nga fotot e zgjedhura nuk janë në formatin e duhur",
					"InvalidBizDoc" => "Fotot e biznesit duhet të ngarkohen në format të pranueshëm",
					"StoringFailed" => "Ngarkimi i fotove dështoj, ju lutem provoni më vonë",
					_ => "Fotot e ngarkuara nuk mund të shfrytëzohen",
				};
				ModelState.AddModelError("Pictures", message);
				return View($"~/Views/Security/Verification/Verification{(isMobileDevice ? ".mobile" : "")}.cshtml",
					new BaseIdentification { Identification = images.Identification });
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Activation(SMSTokenModel model)
		{
			try
			{
				if (!ModelState.IsValid)
					throw new Exception("BadToken");

				var match = await UserHelper.SMSAsync(model);

				if (match)
				{
					ModelState.AddModelError("Credentials", "Llogaria juaj në eKosova është krijuar dhe ka kaluar në procesin e verifikimit. Përdorni \"Numrin Personal\" dhe \"Fjalëkalimin\" për të vazhduar më tutje.");
					return View($"~/Views/Security/Index.cshtml");
				}
				throw new Exception("BadToken"); // code doesn't match db // add counter?
			}
			catch (Exception e)
			{
				ModelState.AddModelError("Token", "Kodi nuk eshte i saktë");
				return View($"~/Views/Security/Confirmation/Confirmation.cshtml", new SMSTokenModel { Identification = model.Identification });
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(Credentials credentials)
		{
			try
			{
				if (!new CredentialsValidator().Validate(credentials).IsValid)
				{
					ModelState.AddModelError("Credentials", "Përdoruesi ose fjalëkalimi janë gabim");
					return View($"~/Views/Security/Index.cshtml");
				}

				var response = await UserHelper.Login(credentials);
				var repodLogin = response.Data.First();
				switch (response.Status)
				{
					case 0: // success
						var claims = new[] {
									new Claim(ClaimTypes.Name, credentials.Account),
									new Claim("Identification", repodLogin.Identification),
									new Claim("AccessToken", repodLogin.Token),
									new Claim("RefreshToken", repodLogin.RefreshToken),
									new Claim("FullName",repodLogin.First+" "+repodLogin.Last),
									new Claim("Email",repodLogin.Email ?? ""),
									new Claim("TokenValidity", repodLogin.ValidDateTimeToken.ToString()),
									new Claim("NotificationsCount",repodLogin.Notifications.Count().ToString()),
									new Claim("Notifications", JsonConvert.SerializeObject(repodLogin.Notifications))
								};
						var identity = new ClaimsIdentity(claims, "User");
						var userPrincipal = new ClaimsPrincipal(new[] { identity });
						await HttpContext.SignInAsync(userPrincipal);
						return RedirectToAction("Index", "Home");
					case 3: // photos need upload
						var state = await UserHelper.AccountState(repodLogin.Identification);
						ModelState.AddModelError("Pictures", "Fotot e cekura më poshtë duhet ngarkuar më qartë ngase nuk e kanë kaluar procesin e verifikimt");
						// add comment from db to modelerror instead of ^
						// save db comment in state
						return View($"~/Views/Security/Verification/Verification{(isMobileDevice ? ".mobile" : "")}.cshtml", state);
					case 13: // sms not verified
						var result = await UserHelper.SetToken(repodLogin.Identification);
						if (result.Validity != 0)
							return View($"~/Views/Security/Confirmation/Confirmation.cshtml",
								new SMSTokenModel { Identification = repodLogin.Identification, Validity = result.Validity });
						else
							throw new Exception("OtherStatusCodes?");
					case 1: // password wrong
					case 11: // user doesn't exist
						ModelState.AddModelError("Credentials", "Përdoruesi ose fjalëkalimi janë gabim");
						return View($"~/Views/Security/Index.cshtml");
					default: throw new Exception("OtherStatusCodes?");
				}
			}
			catch (Exception e)
			{
				ModelState.AddModelError("Credentials", "Ju lutem provoni më vonë");
				return View($"~/Views/Security/Index.cshtml");
			}
		}

		[HttpPost]
		public async Task<IActionResult> RegenSMS(string ID) => Ok(await UserHelper.SetToken(ID));

		[HttpGet]
		public ActionResult RegenPW() {
			var hashsalt = new HashSalt("123123");
			return Ok(Convert.ToBase64String(hashsalt.Salt) + " _-_-_ " + Convert.ToBase64String(hashsalt.Hash));
		}
	}
}