#pragma checksum "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4b59edab9a1179e4102362419014575d5f0ca5d9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Profile_Index), @"mvc.1.0.view", @"/Views/Profile/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "F:\Team Projects\eKosova\eKosova\Views\_ViewImports.cshtml"
using eKosova;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "F:\Team Projects\eKosova\eKosova\Views\_ViewImports.cshtml"
using eKosova.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
using eKosova.Custom.ExtensionMethods;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4b59edab9a1179e4102362419014575d5f0ca5d9", @"/Views/Profile/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"89b5a9ed49901e4956e28bee9d77f00ec9026e1c", @"/Views/_ViewImports.cshtml")]
    public class Views_Profile_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<eKosova.Models.ResetPassword>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "password", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("text-center"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("text-indent: unset"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("autocomplete", new global::Microsoft.AspNetCore.Html.HtmlString("off"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("newPassword"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/formPw.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<section class=\"reset\">\r\n\t<div class=\"sv-header\">\r\n\t\t<div class=\"navigation\">\r\n\t\t\t<div class=\"title\">\r\n\t\t\t\t<h2>Profili Im</h2>\r\n\t\t\t</div>\r\n\t\t\t<button class=\"push btn-option notifications\" id=\"btnNotification\">\r\n\t\t\t\t<span class=\"count\">");
#nullable restore
#line 10 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
                               Write(eKosova.Custom.Helpers.LogedUser.GetNotificationCount());

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n\t\t\t\tNjoftimet\r\n\t\t\t</button>\r\n");
#nullable restore
#line 13 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
             if (eKosova.Custom.Helpers.LogedUser.GetNotificationCount() > 0)
			{

#line default
#line hidden
#nullable disable
            WriteLiteral("\t\t\t\t<div id=\"notificationContainer\">\r\n\t\t\t\t\t<div id=\"notificationTitle\">Njoftimet</div>\r\n");
#nullable restore
#line 17 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
                     foreach (var notification in eKosova.Custom.Helpers.LogedUser.GetNotifications().Take(5))
					{

#line default
#line hidden
#nullable disable
            WriteLiteral("\t\t\t\t\t\t<a");
            BeginWriteAttribute("href", " href=\"", 683, "\"", 707, 2);
            WriteAttributeValue("", 690, "#", 690, 1, true);
#nullable restore
#line 19 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
WriteAttributeValue("", 691, notification.ID, 691, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"notificationsLink\">\r\n\t\t\t\t\t\t\t<div");
            BeginWriteAttribute("class", " class=\"", 748, "\"", 811, 2);
            WriteAttributeValue("", 756, "notificationsBody", 756, 17, true);
#nullable restore
#line 20 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
WriteAttributeValue("", 773, notification.IsRead?"":" highlight", 773, 38, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n\t\t\t\t\t\t\t\t<span class=\"notificationsMessage\">");
#nullable restore
#line 21 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
                                                              Write(notification.Message);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n\t\t\t\t\t\t\t\t<span class=\"notificationsTime\">");
#nullable restore
#line 22 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
                                                           Write(notification.SentDate.ToString(@"HH\:mm, dd\/MM\/yyyy"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</a>\r\n");
#nullable restore
#line 25 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
					}

#line default
#line hidden
#nullable disable
            WriteLiteral("\t\t\t\t\t<div id=\"notificationFooter\"><a href=\"#\">Shiko te gjitha</a></div>\r\n\t\t\t\t</div>\r\n");
#nullable restore
#line 28 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
			}

#line default
#line hidden
#nullable disable
            WriteLiteral("\t\t\t");
#nullable restore
#line 29 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
       Write(Html.CustomDropdown("Profile", new SelectListItem[] {
				new SelectListItem{ Text = "Historiku", Value = "1" },
				new SelectListItem{ Text = "Dalja", Value = "2" } },
				eKosova.Custom.Helpers.LogedUser.GetLoggedUserFull(), "0", "profile"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"text\">\r\n\t\t<p>Për të ndryshuar fjalekalimin ju duhet të plotësoni të dhënat në vazhdim:</p>\r\n\t</div>\r\n");
#nullable restore
#line 38 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
     using (Html.BeginForm("Update", "Profile", FormMethod.Post))
	{

#line default
#line hidden
#nullable disable
            WriteLiteral("\t\t<input type=\"hidden\" name=\"Identification\"");
            BeginWriteAttribute("value", " value=\"", 1616, "\"", 1675, 1);
#nullable restore
#line 40 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
WriteAttributeValue("", 1624, eKosova.Custom.Helpers.LogedUser.GetLoggedUserId(), 1624, 51, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n\t\t<div class=\"register-row\">\r\n\t\t\t<div>\r\n\t\t\t\t<label for=\"confirm-password\">Fjalëkalimi i tanishëm</label>\r\n\t\t\t\t");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "4b59edab9a1179e4102362419014575d5f0ca5d910197", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            BeginWriteTagHelperAttribute();
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("required", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
#nullable restore
#line 44 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => Model.OldPassword);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"register-row\">\r\n\t\t\t<div>\r\n\t\t\t\t<label for=\"password\">Vendosni fjalëkalimin e ri</label>\r\n\t\t\t\t");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "4b59edab9a1179e4102362419014575d5f0ca5d912538", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
#nullable restore
#line 50 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => Model.Password);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("required", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
			</div>
			<div>
				<label for=""confirm-password"">Konfirmojeni fjalëkalimin e ri</label>
				<input type=""password"" id=""ConfirmPassword"" name=""ConfirmPassword"" required class=""newPassword"" autocomplete=""off"">
			</div>
		</div>
		<div id=""pwd-result""></div>
		<div class=""error-message"">");
#nullable restore
#line 58 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
                              Write(Html.ValidationMessage("Credentials"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</div><br />\r\n\t\t<div class=\"text-center\">\r\n\t\t\t<button class=\"btn-cancel\" type=\"submit\"");
            BeginWriteAttribute("formaction", " formaction=\"", 2567, "\"", 2607, 1);
#nullable restore
#line 60 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
WriteAttributeValue("", 2580, Url.Action("Index","Home"), 2580, 27, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Ndërpreje</button>\r\n\t\t\t<button class=\"btn-continue\" type=\"submit\">Vazhdo</button>\r\n\t\t</div>\r\n");
#nullable restore
#line 63 "F:\Team Projects\eKosova\eKosova\Views\Profile\Index.cshtml"
	}

#line default
#line hidden
#nullable disable
            WriteLiteral("</section>\r\n<style>\r\n\t#notificationContainer {\r\n\t\tmargin-right: 5%;\r\n\t}\r\n\r\n\t");
            WriteLiteral("@media only screen and (min-width: 992px) {\r\n\t\t#notificationContainer {\r\n\t\t\tmargin-right: 16%;\r\n\t\t}\r\n\t}\r\n\r\n\t");
            WriteLiteral("@media only screen and (min-width: 1200px) {\r\n\t\t#notificationContainer {\r\n\t\t\tmargin-right: 21%;\r\n\t\t}\r\n\t}\r\n\r\n\t");
            WriteLiteral("@media only screen and (min-width: 1600px) {\r\n\t\t#notificationContainer {\r\n\t\t\tmargin-right: 26%;\r\n\t\t}\r\n\t}\r\n\r\n\t.reset {\r\n\t\tgrid-row-gap: unset;\r\n\t}\r\n\r\n\t#Password-error, #ConfirmPassword-error {\r\n\t\tdisplay: none !important;\r\n\t}\r\n</style>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4b59edab9a1179e4102362419014575d5f0ca5d916676", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
<script>
	$().ready(function () {
		$(""form"").validate({
			rules: {
				CurrentPassword: {
					required: true
				},
				Password: {
					required: true,
					strongEnough: true
				},
				ConfirmPassword: {
					required: true,
					passwordsMatch: true
				}
			},
			messages: {
				CurrentPassword: ""Fjalekalimi i tanishëm duhet shkruar"",
				Password: """",
				ConfirmPassword: """"
			},
			submitHandler: function (form) {
				$('.loader').show();
				form.submit();
			}
		});
	});
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<eKosova.Models.ResetPassword> Html { get; private set; }
    }
}
#pragma warning restore 1591
