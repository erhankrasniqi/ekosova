﻿using eKosova.Custom.Helpers;
using eKosova.Domain;
using eKosova.Domain.ServiceCategories;
using eKosova.Interfaces;
using System.Threading.Tasks;

namespace eKosova.Services
{
	public class ServiceViewService : IServiceCategoryViewService
	{
		public async Task<ApiResponse<ServiceCategory>> GetServices()
			=> await ApiRequestHelper<ServiceCategory>.Get("api/Service/getServices/");


		public async Task<ApiResponse<ServiceSubCategory>> GetSubServices(int ID)
			=> await ApiRequestHelper<ServiceSubCategory>.Get($"api/Service/getSubServices?ID={ID}");
	}
}
