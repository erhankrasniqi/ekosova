﻿using eKosova.Custom.Helpers;
using eKosova.Domain;
using eKosova.Domain.Informations;
using eKosova.Interfaces;
using System.Threading.Tasks;

namespace eKosova.Services
{
	public class InformationViewService : IInformationViewService
	{
		public async Task<ApiResponse<Information>> GetInformations()
			=> await ApiRequestHelper<Information>.Get("api/Information/getInformations");
	}
}
