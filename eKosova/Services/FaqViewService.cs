﻿using eKosova.Custom.Helpers;
using eKosova.Domain;
using eKosova.Interfaces;
using System.Threading.Tasks;
using eKosova.SharedModel.Faqs;

namespace eKosova.Services
{
	public class FaqViewService : IFaqViewService
	{
		public async Task<ApiResponse<FaqModel>> GetFaqs()
			=> await ApiRequestHelper<FaqModel>.Get("api/Faq/getFaqs");
	}
}
