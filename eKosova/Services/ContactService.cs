﻿using eKosova.Custom.Helpers;
using eKosova.Domain;
using eKosova.Interfaces;
using eKosova.SharedModel.Contacts;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace eKosova.Services
{
	public class ContactService : IContactService
	{
		public async Task<ApiResponse<CreateContactModel>> CreateContact(CreateContactModel _)
			=> await ApiRequestHelper<CreateContactModel>.Post("api/Contact/create", JsonConvert.SerializeObject(_));
	}
}
