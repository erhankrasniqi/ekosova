﻿using eKosova.Custom.Helpers;
using eKosova.Domain;
using eKosova.Interfaces;
using eKosova.SharedModel.Helps;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace eKosova.Services
{
	public class HelpService : IHelpService
	{
		public async Task<ApiResponse<CreateHelpModel>> CreateHelp(CreateHelpModel _)
			=> await ApiRequestHelper<CreateHelpModel>.Post("api/Help/create", JsonConvert.SerializeObject(_));
	}
}
