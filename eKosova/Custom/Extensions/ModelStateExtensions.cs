﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace eKosova.Custom.Extensions
{
	public static class ModelStateExtensions
	{
		public static string GetStringErrors(this ModelStateDictionary dictionary)
		{
			var errorMessages = new List<string>();
			foreach (var modelStateDD in dictionary)
			{
				var modelState = modelStateDD.Value;
				foreach (var error in modelState.Errors)
					errorMessages.Add(string.IsNullOrEmpty(error.ErrorMessage)
						? (error.Exception.Message + (error.Exception.InnerException != null ? "(" + error.Exception.InnerException.Message + ")" : ""))
						: error.ErrorMessage);
			}
			return string.Join(",", errorMessages);
		}

		public static List<ErrorResult> GetErrors(this ModelStateDictionary dictionary)
		{
			var Errors = new List<ErrorResult>();
			foreach (var modelStateDD in dictionary)
			{
				var items = modelStateDD.Key.Split('.').ToList();
				items.RemoveAt(0);
				var key = string.Join(".", items);

				var modelState = modelStateDD.Value;

				var errorMessages = new List<string>();

				foreach (var error in modelState.Errors)
					errorMessages.Add(string.IsNullOrEmpty(error.ErrorMessage)
						? (error.Exception.Message +
						(error.Exception.InnerException != null ? "(" + error.Exception.InnerException.Message + ")" : ""))
						: error.ErrorMessage);

				Errors.Add(new ErrorResult
				{
					Key = key,
					Messages = errorMessages
				});
			}
			return Errors;
		}
	}

	public class ErrorResult
	{
		public string Key { get; set; }
		public List<string> Messages { get; set; }

	}
}
