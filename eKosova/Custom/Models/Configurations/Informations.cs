﻿namespace eKosova.Custom.Models.Configurations
{
	public enum Informations
    {
        Benefits = 1,
        Motion = 2,
        Family = 3,
        Security = 4,
        Health = 5,
        Nature = 6,
        Education = 7,
        Diaspore = 8,
        Work = 9,
        Foreigners = 10,
        Property = 11,
        Culture = 12,
        Documents = 13,
        Freedom = 14
    }
}
