﻿namespace eKosova.Custom.Models
{
	public class Information
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
    }
}