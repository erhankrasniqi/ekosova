﻿using eKosova.Business;
using eKosova.Custom.Helpers;
using eKosova.Domain.PublicUsers;
using eKosova.Domain.SeedWork;
using eKosova.Domain;
using eKosova.Models;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Linq;
using eKosova.Custom.Validators;

namespace eKosova.Custom
{
	public static class UserHelper
	{
		public static async Task<ApiResponse<BaseIdentification>> NewPublicUser(PartialRegister partial)
		{
			var hasher = new HashSalt(partial.Password);

			var parameters = JsonConvert.SerializeObject(new PublicUser
			{
				Email = null, // client-decision?
				PersonalNumber = partial.PersonalNumber,
				PasswordHash = Convert.ToBase64String(hasher.Hash),
				PasswordSalt = Convert.ToBase64String(hasher.Salt),
				PhoneNumber = partial.Phone,
				First = "Emri", // client-decision?
				Last = "Mbiemri" // client-decision?
			});

			var response = await ApiRequestHelper<BaseIdentification>.Post("api/user/create", parameters);

			return response;
		}

		internal async static Task<bool> UpdatePassword(ResetPassword reset)
		{
			var newHash = new HashSalt(reset.Password);

			var parameters = JsonConvert.SerializeObject(new PasswordResetApiModel
			{
				Password = reset.OldPassword,
				PasswordHash = Convert.ToBase64String(newHash.Hash),
				PasswordSalt = Convert.ToBase64String(newHash.Salt),
				Identification = reset.Identification
			});

			var response = await ApiRequestHelper<bool>.Post("api/user/updatePassword", parameters);

			return response.Status == 0 & response.Data.First();
		}

		public async static Task<bool> ClearTokens(string identification)
		{
			var parameters = JsonConvert.SerializeObject(new BaseIdentification { Identification = identification });

			var response = await ApiRequestHelper<bool>.Post("api/user/clearTokens", parameters);

			return response.Status == 0;
		}

		public static async Task<bool> SMSAsync(SMSTokenModel model)
		{
			var parameters = JsonConvert.SerializeObject(model);

			var response = await ApiRequestHelper<BaseIdentification>.Post("api/user/verifysms", parameters);

			return response.Status == 0;
		}

		internal static async Task<SMSTokenModel> SetToken(string identification)
		{
			var parameters = JsonConvert.SerializeObject(new BaseIdentification { Identification = identification });

			var response = await ApiRequestHelper<SMSTokenModel>.Post("api/user/smstoken", parameters);

			if (response.Status == 0)
				return response.Data.First();
			else
				return new SMSTokenModel { Identification = identification, Validity = 0 };
		}

		internal static async Task<bool> StoreImages(Images images, VerificationModel state)
		{
			var base64 = new Base64Images
			{
				Identification = images.Identification,
				BackID = !state.BackID ? Convert.ToBase64String(await images.BackID.GetBytes()) : null,
				FrontID = !state.FrontID ? Convert.ToBase64String(await images.FrontID.GetBytes()) : null,
				Selfie = !state.Selfie ? Convert.ToBase64String(await images.Selfie.GetBytes()) : null,
				BizDokument = (images.BizDokument != null && !state.BizDokument) ? Convert.ToBase64String(await images.BizDokument.GetBytes()) : null
			};

			var parameters = JsonConvert.SerializeObject(base64);
			var response = await ApiRequestHelper<bool>.Post("api/user/images", parameters);

			return response.Status == 0 && response.Data.First();
		}

		internal async static Task ConfirmationChecks(Images images)
		{
			var state = await AccountState(images.Identification);

			if ((!state.FrontID && !images.FrontID.IsImage()) || (!state.BackID && !images.BackID.IsImage()) ||
				(!state.Selfie && !images.Selfie.IsImage()) || (!state.FrontID && !images.FrontID.IsImage()))
				throw new Exception("ModelState");

			// check bizdoc only if uploaded
			if (images.BizDokument != null && !images.BizDokument.IsImage())
				throw new Exception("InvalidBizDoc");

			// storing might fail
			if (!await StoreImages(images, state))
				throw new Exception("StoringFailed");

			if (state.IsActive)
				throw new ObjectDisposedException("ALREADY_ACTIVE");

			if ((await SetToken(images.Identification)).Validity == 0)
				throw new MissingFieldException();
		}

		public static bool Modulo11(string PersonalNumber)
		{ // backend modulo11 check
			try
			{
				bool result = false;
				if (PersonalNumber.Length == 10 && PersonalNumber.All(char.IsNumber))
				{
					var newValue = PersonalNumber.Substring(0, 9);
					int checkDigit = Convert.ToInt32(PersonalNumber.Substring(PersonalNumber.Length - 1, 1)),
						sum = 0, j = 2;
					for (var i = newValue.Length - 1; i >= 0; i--)
					{
						var temp = Convert.ToInt32(newValue[i].ToString());
						sum += temp * j;
						j++;
						if (j > 7)
							j = 2;
					}
					var reminder = 11 - (sum % 11);
					result = Convert.ToInt32(reminder.ToString().Substring(reminder.ToString().Length - 1, 1)) == checkDigit;
				}
				return result;
			}
			catch
			{ return false; }
		}

		public static async Task<VerificationModel> AccountState(string identification)
		{
			var parameters = JsonConvert.SerializeObject(new BaseIdentification { Identification = identification });

			var response = await ApiRequestHelper<VerificationModel>.Post("api/user/accountState", parameters);

			if (response.Status == 0)
				return response.Data.First();
			else
				return null;
		}

		public static async Task<ApiResponse<LoginResponse>> Login(Credentials credentials)
			=> await ApiRequestHelper<LoginResponse>.Post("authenticate",
				JsonConvert.SerializeObject(credentials));
	}
}
