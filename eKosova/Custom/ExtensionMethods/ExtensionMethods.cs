﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection.Emit;
using System.Threading.Tasks;

namespace eKosova.Custom.ExtensionMethods
{
	public static class ExtensionMethods
	{
		public static string Capitalize(this string value) => string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value) ? "" : value.First().ToString().ToUpper() + String.Join("", value.Skip(1));

		public static IHtmlContent CustomDropdown(this IHtmlHelper helper, string id, IEnumerable<SelectListItem> options, string labelOption, object htmlAttributes, string iconClass = "")
		{
			TagBuilder customDropdown = new TagBuilder("div"), select = new TagBuilder("select"), button = new TagBuilder("button"), dropdown = new TagBuilder("div");
			var attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
			customDropdown.MergeAttributes(attrs);
			customDropdown.AddCssClass("custom-dropdown");
			customDropdown.Attributes.Add("data-id", Guid.NewGuid().ToString());
			select.Attributes.Add("id", id);
			select.Attributes.Add("name", id);
			dropdown.AddCssClass("options");
			button.AddCssClass($"btn-option {iconClass}");
			button.Attributes.Add("type", "button");
			button.InnerHtml.Append($"{(options.Any(_ => _.Selected) ? options.Where(_ => _.Selected).FirstOrDefault().Text : labelOption)}");

			if (!(string.IsNullOrEmpty(labelOption) || string.IsNullOrWhiteSpace(labelOption)))
			{
				select.InnerHtml.AppendHtml("<option value='0'></option>");
				dropdown.InnerHtml.AppendHtml($"<span class='option option-label' data-value='0'>{labelOption}</span>");
			}

			foreach (var item in options)
			{
				select.InnerHtml.AppendHtml($"<option value='{item.Value}'{(item.Disabled ? "disabled" : "")}{(item.Selected ? "selected" : "")}></option>");
				dropdown.InnerHtml.AppendHtml($"<span class='option{(item.Disabled ? " disabled" : "")}{(item.Selected ? " selected" : "")}' data-value='{item.Value}'>{item.Text}</span>");
			}

			customDropdown.InnerHtml.AppendHtml(select).AppendHtml(button).AppendHtml(dropdown);
			// customDropdown.MergeAttributes(attrs);
			return customDropdown;
		}

		public static IHtmlContent CustomDropdownFor<TResult>(this IHtmlHelper<TResult> helper, Expression<Func<dynamic, TResult>> expression, IEnumerable<SelectListItem> options, string labelOption, object htmlAttributes, string iconClass = "")
		{
			TagBuilder customDropdown = new TagBuilder("div"), select = new TagBuilder("select"), button = new TagBuilder("button"), dropdown = new TagBuilder("div");
			customDropdown.AddCssClass("custom-dropdown");
			customDropdown.Attributes.Add("data-id", Guid.NewGuid().ToString());
			select.Attributes.Add("id", expression.Name);
			dropdown.AddCssClass("options");
			button.AddCssClass($"btn-option {iconClass}");
			button.Attributes.Add("type", "button");
			button.InnerHtml.Append($"{(options.Any(_ => _.Selected) ? options.Where(_ => _.Selected).FirstOrDefault().Text : labelOption)}");

			if (!(string.IsNullOrEmpty(labelOption) || string.IsNullOrWhiteSpace(labelOption)))
			{
				select.InnerHtml.AppendHtml("<option value=0></option>");
				dropdown.InnerHtml.AppendHtml($"<span class='option option-label' data-value='0'>{labelOption}</span>");
			}

			foreach (var item in options)
			{
				select.InnerHtml.AppendHtml($"<option value='{item.Value}'{(item.Disabled ? "disabled" : "")}{(item.Selected ? "selected" : "")}></option>");
				dropdown.InnerHtml.AppendHtml($"<span class='option{(item.Disabled ? " disabled" : "")}{(item.Selected ? " selected" : "")}' data-value='{item.Value}'>{item.Text}</span>");
			}
			var attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

			customDropdown.InnerHtml.AppendHtml(select).AppendHtml(button).AppendHtml(dropdown);
			customDropdown.MergeAttributes(attrs);
			return customDropdown;
		}

		public static IHtmlContent CustomInputFile(this IHtmlHelper helper, string id, string defaultText = "Klikoni këtu për të ngarkuar një fajll", bool allowMultiple = false, object htmlAttributes = null)
		{
			TagBuilder inputContainer = new TagBuilder("label"), input = new TagBuilder("input");
			if (allowMultiple)
				input.Attributes.Add("multiple", "");
			input.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
			inputContainer.MergeAttributes(new Dictionary<string, string>(new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("class", "file"), new KeyValuePair<string, string>("for", id) }));
			input.MergeAttributes(new Dictionary<string, string>(new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("type", "file"), new KeyValuePair<string, string>("id", id), new KeyValuePair<string, string>("data-default-text", defaultText), new KeyValuePair<string, string>("name", id) }));
			inputContainer.InnerHtml.AppendHtml($"<span>{defaultText}</span>").AppendHtml(input);
			return inputContainer;
		}
	}
}