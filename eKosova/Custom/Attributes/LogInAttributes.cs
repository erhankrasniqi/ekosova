﻿using eKosova.Custom.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace eKosova.Custom.Attributes
{
	internal sealed class LoggedInAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			if (!LogedUser.LoggedIn())
				context.Result = new RedirectToActionResult("Index", "Security", null);
		}
	}
	internal sealed class AlreadyLoggedInAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			if (LogedUser.LoggedIn())
				context.Result = new RedirectToActionResult("Index", "Home", null);
		}
	}
}