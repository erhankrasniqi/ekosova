﻿using eKosova.Domain;
using FluentValidation;

namespace eKosova.Custom.Validators
{
	public class CredentialsValidator : AbstractValidator<Credentials>
	{
		public CredentialsValidator()
		{
			RuleFor(x => x.Account).NotNull().NotEmpty().Length(10);
			RuleFor(x => x.Password).NotNull().NotEmpty();
		}
	}
}
