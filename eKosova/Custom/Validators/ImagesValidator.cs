﻿using eKosova.Custom.Helpers;
using eKosova.Domain;
using FluentValidation;

namespace eKosova.Custom.Validators
{
	public class ImagesValidator : AbstractValidator<Images>
	{
		public ImagesValidator()
		{
			RuleFor(x => x.Identification).NotNull().NotEmpty().Length(36);
			RuleFor(x => x.FrontID).NotNull().NotEmpty().Must(uploaded => uploaded.IsImage());
			RuleFor(x => x.BackID).NotNull().NotEmpty().Must(uploaded => uploaded.IsImage());
			RuleFor(x => x.Selfie).NotNull().NotEmpty().Must(uploaded => uploaded.IsImage());
		}
	}
}
