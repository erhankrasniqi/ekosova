﻿using eKosova.Models;
using FluentValidation;

namespace eKosova.Custom.Validators
{
	public class PartialRegisterValidator : AbstractValidator<PartialRegister>
	{
		public PartialRegisterValidator()
		{
			RuleFor(x => x.Phone).Length(8);
			RuleFor(x => x.PersonalNumber).NotNull().NotEmpty().Length(10).Must(_ => UserHelper.Modulo11(_));
			RuleFor(x => x.Password).NotNull().NotEmpty();
		}
	}
	public class PasswordUpdateValidator : AbstractValidator<ResetPassword>
	{
		public PasswordUpdateValidator()
		{
			RuleFor(x => x.Identification).NotNull().NotEmpty();
			RuleFor(x => x.OldPassword).NotNull().NotEmpty();
			RuleFor(x => x.Password).NotNull().NotEmpty();
		}
	}
}
