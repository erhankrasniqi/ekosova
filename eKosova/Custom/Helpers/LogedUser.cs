﻿using eKosova.Domain;
using eKosova.Domain.Notifications;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eKosova.Custom.Helpers
{
	/*public static class SessionExtensions
	{
		public static T GetComplexData<T>(this ISession session, string key)
		{
			var data = session.GetString(key);
			if (data == null) return default;
			return JsonConvert.DeserializeObject<T>(data);
		}

		public static bool HasData(this ISession session, string key)
		{
			var data = session.GetString(key);
			return data != null;
		}

		public static void SetComplexData(this ISession session, string key, object value) => session.SetString(key, JsonConvert.SerializeObject(value));
	}*/

	public static class LogedUser
	{
		public static IHttpContextAccessor _contextAccessor;

		public static void SetHttpContextAccessor(IHttpContextAccessor accessor) => _contextAccessor = accessor;

		public static bool LoggedIn() => _contextAccessor.HttpContext.User.Identity.Name != null;

		public static string GetLoggedUserId() => _contextAccessor.HttpContext.User.Claims.FirstOrDefault(_ => _.Type == "Identification").Value;

		public static int GetNotificationCount()
		{
			if (LoggedIn())
				return int.Parse(_contextAccessor.HttpContext.User.Claims.FirstOrDefault(_ => _.Type == "NotificationsCount").Value);
			else return 0;
		}

		public static IEnumerable<Notification> GetNotifications() => LoggedIn() ? JsonConvert.DeserializeObject<IEnumerable<Notification>>(_contextAccessor.HttpContext.User.Claims.FirstOrDefault(_ => _.Type == "Notifications").Value) : null;

		public static string GetLoggedUserFull() => LoggedIn() ? (_contextAccessor.HttpContext.User.Claims.FirstOrDefault(_ => _.Type == "FullName").Value) : "Profili im";

		public static string GetLoggedUserToken() => _contextAccessor.HttpContext.User.Claims.FirstOrDefault(_ => _.Type == "AccessToken").Value;

		public static string GetLoggedUserEmail() => _contextAccessor.HttpContext.User.Claims.FirstOrDefault(_ => _.Type == "Email").Value;

		public static string GetLoggedUserRefreshToken() => _contextAccessor.HttpContext.User.Claims.FirstOrDefault(_ => _.Type == "RefreshToken").Value;

		public static System.DateTime GetLoggedUserTokenValidTime() => Convert.ToDateTime(_contextAccessor.HttpContext.User.Claims.FirstOrDefault(_ => _.Type == "TokenValidity").Value);

		public static string GetCurrentLocation()
		{
			var request = _contextAccessor.HttpContext.Request;
			var absoluteUri = string.Concat(
						request.Scheme,
						"://",
						request.Host.ToUriComponent(),
						request.PathBase.ToUriComponent(),
						request.Path.ToUriComponent(),
						request.QueryString.ToUriComponent());
			return absoluteUri;
		}
	}
}
