﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace eKosova.Custom.Helpers
{
	public static class FormFileExtensions
	{
		public const int ImageMinimumBytes = 512;

		public static bool IsImage(this IFormFile postedFile)
		{
			// check mime
			if (postedFile.ContentType.ToLower() != "image/jpg" &&
						postedFile.ContentType.ToLower() != "image/jpeg" &&
						postedFile.ContentType.ToLower() != "image/x-png" &&
						postedFile.ContentType.ToLower() != "image/png")
				return false;

			// backend extension check
			if (Path.GetExtension(postedFile.FileName).ToLower() != ".jpg"
				&& Path.GetExtension(postedFile.FileName).ToLower() != ".png"
				&& Path.GetExtension(postedFile.FileName).ToLower() != ".jpeg")
				return false;

			// try read
			try
			{
				// check if readable
				if (!postedFile.OpenReadStream().CanRead)
					return false;

				// image are > 512 bytes
				if (postedFile.Length < ImageMinimumBytes)
					return false;

				byte[] buffer = new byte[ImageMinimumBytes];
				postedFile.OpenReadStream().Read(buffer, 0, ImageMinimumBytes);
				string content = System.Text.Encoding.UTF8.GetString(buffer);

				// check suspicious stuff
				if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
					RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline))
					return false;
			}
			catch (Exception)
			{
				return false;
			}
			finally
			{
				postedFile.OpenReadStream().Position = 0;
			}
			return true;
		}
	}

}
