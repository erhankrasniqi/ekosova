﻿using Microsoft.Extensions.Caching.Memory;
using System;

namespace eKosova.Custom.Helpers
{
	public static class CacheKeys
	{
		public static MemoryCacheEntryOptions Options = new MemoryCacheEntryOptions()
		 .SetSlidingExpiration(TimeSpan.FromDays(1));

		public static string Services { get { return "_Entry"; } }
		public static string SubServices { get { return "_SubServices"; } }
		public static string Informations { get { return "_Informations"; } }
		public static string Faqs { get { return "_Faqs"; } }
	}
}
