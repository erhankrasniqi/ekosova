using AutoMapper;
using eKosova.Interfaces;
using eKosova.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace eKosova
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
			StaticConfig = configuration;
		}
		//readonly string MyAllowSpecificOrigin = "_myAllowSpecificOrigins";
		public IConfiguration Configuration { get; private set; }

		public static IConfiguration StaticConfig { get; private set; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddAuthentication("CookieAuthentication").AddCookie("CookieAuthentication", config =>
			{
				config.Cookie.Name = "UserLoginCookie";
				config.LoginPath = "/Security/Index";
			});

			//services.AddCors(Options =>
			//{
			//	Options.AddPolicy("_myAllowSpecificOrigins",
			//		builder => builder
			//		.AllowAnyOrigin()
			//		.AllowAnyMethod()
			//		.AllowAnyHeader()
			//		);
			//});

			services.AddMemoryCache();

			services.AddMvc(_ => _.EnableEndpointRouting = false);

			services.AddScoped<IInformationViewService, InformationViewService>();

			services.AddScoped<IServiceCategoryViewService, ServiceViewService>();

			services.AddScoped<IContactService, ContactService>();

			services.AddScoped<IHelpService, HelpService>();

			services.AddScoped<IFaqViewService, FaqViewService>();

			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

			services.AddSession(options =>
			{
				options.IdleTimeout = TimeSpan.FromMinutes(30);
				options.Cookie.HttpOnly = true;
				options.Cookie.IsEssential = true;
			});

			services.AddDetection();

			services.AddAutoMapper(typeof(Startup));

			services.AddControllersWithViews().AddNewtonsoftJson();
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}
			//app.UseCors(MyAllowSpecificOrigin);
			//app.UseHttpsRedirection();

			app.UseSession();

			app.UseStaticFiles();

			app.UseDetection();

			app.UseRouting();

			app.UseAuthentication();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
