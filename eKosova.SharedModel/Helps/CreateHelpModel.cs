﻿using FluentValidation;
using System;
using System.Linq;

namespace eKosova.SharedModel.Helps
{
	public class CreateHelpModel
	{
		public string FullName { get; set; }
		public string Email { get; set; }
		public string Description { get; set; }
		public bool Status { get; set; }
		public DateTime InsertionDate { get; set; }
	}

	public class CreateHelpValidator : AbstractValidator<CreateHelpModel>
	{
		public CreateHelpValidator()
		{
			//RuleFor(x => x.ID).Null();
			RuleFor(x => x.FullName).Length(3, 150).NotEmpty().Must(FullName => FullName.All(_ => char.IsLetter(_) || char.IsWhiteSpace(_)));
			RuleFor(x => x.Email).EmailAddress().Length(1, 150).NotEmpty();
			RuleFor(x => x.Description).NotEmpty();
		}
	}
}
