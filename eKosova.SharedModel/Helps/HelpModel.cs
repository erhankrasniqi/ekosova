﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.SharedModel.Helps
{
    public class HelpModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public DateTime InsertionDate { get; set; }
    }
}
