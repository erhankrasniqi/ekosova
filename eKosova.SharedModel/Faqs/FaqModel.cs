﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.SharedModel.Faqs
{
   public class FaqModel 
    {
        public int Id  { get; set; }
        public DateTime InsertionDate { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int IDInternalUser { get; set; }

        public int IDMedia { get; set; }
    }
}

