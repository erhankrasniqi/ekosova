﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.SharedModel.Medias
{
    public class CreateMediaModel
    { 
        public string VideoPath { get; set; }
        public string VoicePath { get; set; }
        public string Description { get; set; }
    }
}
