﻿using FluentValidation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace eKosova.SharedModel.Contacts
{
	public class CreateContactModel
	{
		public string FullName { get; set; }
		public string Email { get; set; }
		public string Description { get; set; }
		public bool Status { get; set; }
		public DateTime InsertionDate { get; set; }

	}

	public class CreateContactValidator : AbstractValidator<CreateContactModel>
	{
		public CreateContactValidator()
		{
			//RuleFor(x => x.ID).Null();
			RuleFor(x => x.FullName).Length(3, 150).NotEmpty().Must(FullName => FullName.All(_ => char.IsLetter(_) || char.IsWhiteSpace(_)));
			RuleFor(x => x.Email).EmailAddress().Length(1, 150).NotEmpty();
			RuleFor(x => x.Description).NotEmpty();
		}
	}
}
