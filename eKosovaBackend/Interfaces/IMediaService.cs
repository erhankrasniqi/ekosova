﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosova.SharedModel.Medias;

namespace eKosovaBackend.Interfaces
{
    public interface IMediaService
    {
        Response<MediaModel> Create(CreateMediaModel _);
    }
}
