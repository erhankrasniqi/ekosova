﻿using eKosova.SharedModel.Helps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Interfaces
{
    public interface IHelpService
    {
        Response<HelpModel> Create(CreateHelpModel _);

    }
}
