﻿using eKosova.Domain.Faqs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Interfaces
{
    public interface IFaqService
    {
        Response<Faq> Create(Faq _);
        IEnumerable<Faq> GetFaqLsit();
        IEnumerable<Faq> GetActiveFaqList();
        Response<Faq> GetResponseFaqList();
        Response<Faq> GetResponseActiveFaqList();
    }
}
