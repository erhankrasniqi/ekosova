﻿using eKosova.Domain.ServiceCategories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Interfaces
{
	public interface IServiceCategoryService
    {
        IEnumerable<ServiceCategory> GetList();
        Response<ServiceSubCategoryDynamicForm> GetResponseList();
        IEnumerable<ServiceSubCategory> GetSubCategoryList();
        Response<ServiceSubCategory> GetResponseSubCategoryList(int ID);
    }
}
