﻿using eKosova.Domain;
using eKosova.Domain.Informations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Interfaces
{
    public interface IInformationService
    {
        IEnumerable<Information> GetInformationList();
        Response<Information> GetResponseInformationList();
        IEnumerable<InformationCategory> GetInformationCategoryList();
        Response<InformationCategory> GetResponseInformationCategoryList();
    }
}
