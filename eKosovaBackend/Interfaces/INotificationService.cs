﻿using eKosova.Domain.Notifications;
using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Interfaces
{
	public interface INotificationService
	{
		Task<Response<BaseEntity>> Update(Notification _);
		Task<Response<Notification>> GetById(int ID);
		Task<Response<IEnumerable<Notification>>> GetByIdentification(string identification);
		Task<Response<IEnumerable<Notification>>> GetUnRead(string identification);
	}
}
