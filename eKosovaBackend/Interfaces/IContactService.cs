﻿using eKosova.SharedModel.Contacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Interfaces
{
    public interface IContactService
    {
        Response<ContactModel> Create(CreateContactModel _);

    }
}
