﻿using eKosova.Domain;
using eKosova.Domain.PublicUsers;
using eKosova.Domain.SeedWork;
using System.Threading.Tasks;

namespace eKosovaBackend.Interfaces
{
	public interface IPublicUserService
	{
		Task<Response<BaseIdentification>> Create(PublicUser _);
		Task<Response<PublicUser>> Select(int ID);
		Task<Response<PublicUser>> SelectPersonalNumber(string personalNumber);
		Task<Response<PublicUser>> SelectIdentification(string identification);
		Task Update(PublicUser _);
		Task<Response<SMSTokenModel>> SetSMS(string identitication);
		Task<Response<BaseIdentification>> VerifySMS(SMSTokenModel sms);
		Task<Response<bool>> StoreImages(Base64Images base64);
		Task<Response<bool>> UpdatePassword(PasswordResetApiModel reset);
		Task<Response<bool>> ClearTokens(string identification);
		Task<Response<VerificationModel>> AccountState(string identification);
	}
}
