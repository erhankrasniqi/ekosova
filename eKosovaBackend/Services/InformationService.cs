﻿using eKosova.Domain.Informations;
using eKosova.SharedModel;
using eKosova.Business;
using eKosovaBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosova.Domain;

namespace eKosovaBackend.Services
{
    public class InformationService : IInformationService
    {
        private readonly IInformationRepository _informationRepository;
        public InformationService(IInformationRepository informationRepository)
        {
            _informationRepository = informationRepository;
        }

        public IEnumerable<Information> GetInformationList() { return _informationRepository.GetInformations(); }
        public Response<Information> GetResponseInformationList() => new Response<Information>(PublicResultStatusCodes.Done, GetInformationList().ToList());
        public IEnumerable<InformationCategory> GetInformationCategoryList() { return _informationRepository.GetInformationCategories(); }
        public Response<InformationCategory> GetResponseInformationCategoryList() => new Response<InformationCategory>(PublicResultStatusCodes.Done, GetInformationCategoryList().ToList());
	}
}
