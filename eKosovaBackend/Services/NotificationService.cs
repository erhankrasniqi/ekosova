﻿using eKosova.Domain.Notifications;
using eKosova.Domain.SeedWork;
using eKosova.SharedModel;
using eKosovaBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Services
{
	public class NotificationService : INotificationService
	{
		private readonly INotificationRepoService _repository;

		public NotificationService(INotificationRepoService repository)
		{
			_repository = repository;
		}

		public async Task<IEnumerable<Notification>> GetUnRead(string identification)
		{
			return await _repository.GetUnRead(identification);
		}

		public async Task Update(Notification _)
		{
			await _repository.Update(_);
		}

		async Task<Response<Notification>> INotificationService.GetById(int ID)
		{
			return new Response<Notification>(PublicResultStatusCodes.Done, await _repository.GetById(ID));
		}

		async Task<Response<IEnumerable<Notification>>> INotificationService.GetByIdentification(string identification)
		{
			return new Response<IEnumerable<Notification>>(PublicResultStatusCodes.Done, await _repository.GetByIdentification(identification));
		}

		async Task<Response<IEnumerable<Notification>>> INotificationService.GetUnRead(string identification)
		{
			return new Response<IEnumerable<Notification>>(PublicResultStatusCodes.Done, await _repository.GetUnRead(identification));
		}

		async Task<Response<BaseEntity>> INotificationService.Update(Notification _)
		{
			await _repository.Update(_);
			return new Response<BaseEntity>(PublicResultStatusCodes.Done);
		}
	}
}
