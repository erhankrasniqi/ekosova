﻿using eKosova.Domain.ServiceCategories;
using eKosovaBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Services
{
	public class ServiceCategoryService : IServiceCategoryService
	{
		private readonly IServiceCategoryRepository _serviceRepository;
		public ServiceCategoryService(IServiceCategoryRepository serviceRepository)
		{ _serviceRepository = serviceRepository; }

		public IEnumerable<ServiceCategory> GetList() => _serviceRepository.GetAll();

		public Response<ServiceSubCategoryDynamicForm> GetResponseList() => new Response<ServiceSubCategoryDynamicForm>(eKosova.SharedModel.PublicResultStatusCodes.Done, _serviceRepository.GetAllWithDynamicForm(2).ToList());

		public Response<ServiceSubCategory> GetResponseSubCategoryList(int ID) => new Response<ServiceSubCategory>(eKosova.SharedModel.PublicResultStatusCodes.Done, _serviceRepository.GetAllSub().Where(_ => _.IDServiceCategory == ID).ToList());

		public IEnumerable<ServiceSubCategory> GetSubCategoryList() => _serviceRepository.GetAllSub();
	}
}
