﻿using AutoMapper;
using eKosova.Domain.Contacts;
using eKosova.SharedModel;
using eKosova.SharedModel.Contacts;
using eKosovaBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Services
{
    public class ContactService : IContactService
    {
        private readonly IContactRepoService _repository;

        private readonly IMapper _mapper;
        public ContactService(IContactRepoService repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public Response<ContactModel> Create(CreateContactModel _)
        {
            try
            {
                var contact = _mapper.Map<Contact>(_);
                _repository.AddContact(contact);
                return new Response<ContactModel>(PublicResultStatusCodes.Done, _mapper.Map<ContactModel>(contact));
            }
            catch (Exception e)
            {
                return new Response<ContactModel>(PublicResultStatusCodes.QueryHasError);
            }

        }
    }
}
