﻿using AutoMapper;
using eKosova.Domain.Faqs;
using eKosova.SharedModel;
using eKosovaBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Services
{
    public class FaqService : IFaqService
    {
        private readonly IFaqRepoService _repository;
        private readonly IMapper _mapper;
        public FaqService(IFaqRepoService repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public Response<Faq> Create(Faq _)
        {
            try
            {
                var faq = _mapper.Map<Faq>(_);
                _repository.AddFaq(faq);
                return new Response<Faq>(PublicResultStatusCodes.Done, _mapper.Map<Faq>(faq));
            }
            catch (Exception e)
            {
                return new Response<Faq>(PublicResultStatusCodes.QueryHasError);
            }
        }

        public IEnumerable<Faq> GetActiveFaqList() => _mapper.Map<IList<Faq>>(_repository.GetActiveFaqs().OrderByDescending(x => x.InsertionDate));


        public IEnumerable<Faq> GetFaqLsit() => _mapper.Map<IList<Faq>>(_repository.GetAllFaqs().OrderByDescending(x => x.InsertionDate));


        public Response<Faq> GetResponseActiveFaqList() => new Response<Faq>(PublicResultStatusCodes.Done, GetActiveFaqList().ToList());


        public Response<Faq> GetResponseFaqList() => new Response<Faq>(PublicResultStatusCodes.Done, GetActiveFaqList().ToList());

        //public IEnumerable<Faq> GetActiveFaqList() => _mapper.Map<IList<Faq>>(_repository.GetActiveFaqs());


        //public IEnumerable<Faq> GetFaqLsit() => _mapper.Map<IList<Faq>>(_repository.GetAllFaqs());

        //public Response<Faq> GetResponseActiveFaqList() => new Response<Faq>(PublicResultStatusCodes.Done, GetActiveFaqList().ToList());


        //public Response<Faq> GetResponseFaqList() => new Response<Faq>(PublicResultStatusCodes.Done, GetActiveFaqList().ToList());
        //public Response<Faq> GetResponseFaqList() => new Response<Faq>(PublicResultStatusCodes.Done, GetActiveFaqList().ToList());
    }
}
