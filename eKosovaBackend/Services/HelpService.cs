﻿using AutoMapper;
using eKosova.Domain.Helps;
using eKosova.SharedModel;
using eKosova.SharedModel.Helps;
using eKosovaBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Services
{
    public class HelpService : IHelpService
    {
        private readonly IHelpRepoService _repository;

        private readonly IMapper _mapper;
        public HelpService(IHelpRepoService repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public Response<HelpModel> Create(CreateHelpModel _)
        {
            try
            {
                var help = _mapper.Map<Help>(_);
                _repository.AddHelp(help);
                return new Response<HelpModel>(PublicResultStatusCodes.Done, _mapper.Map<HelpModel>(help));
            }
            catch (Exception)
            {
                return new Response<HelpModel>(PublicResultStatusCodes.QueryHasError);
            }

        }

        
    }
}
