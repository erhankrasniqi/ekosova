﻿using AutoMapper;
using eKosova.Domain.Contacts;
using eKosova.Domain.Media;
using eKosova.SharedModel;
using eKosova.SharedModel.Medias;
using eKosovaBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Services
{
    public class MediaService : IMediaService
    {
        //private readonly IMediaRepoService repository;
        private readonly IMapper _mapper;
        private readonly IMediaRepoService _repository;

        public MediaService(IMediaRepoService repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;

        }

        public Response<MediaModel> Create(CreateMediaModel _)
        {
            try
            {
                var media = _mapper.Map<Media>(_);
                _repository.AddMedia(media);
                return new Response<MediaModel>(PublicResultStatusCodes.Done, _mapper.Map<MediaModel>(media));
            }
            catch
            {
                return new Response<MediaModel>(PublicResultStatusCodes.QueryHasError);
            }
        }
    }
}
