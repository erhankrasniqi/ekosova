﻿using AutoMapper;
using eKosova.Domain.PublicUsers;
using eKosova.SharedModel;
using eKosovaBackend.Interfaces;
using System;
using System.Threading.Tasks;
using eKosova.Domain.SeedWork;
using System.Linq;
using eKosova.Domain;
using eKosova.Business;

namespace eKosovaBackend.Services
{
	public class PublicUserService : IPublicUserService
	{
		private readonly IPublicUserRepoService _repository;

		public PublicUserService(IPublicUserRepoService repository)
		{ _repository = repository; }

		public async Task<Response<BaseIdentification>> Create(PublicUser _)
		{
			try
			{
				var registered = await _repository.GetByPersonalNumber(_.PersonalNumber);
				if (registered != null)
					return new Response<BaseIdentification>(PublicResultStatusCodes.AccountAlreadyExists);
				var result = await _repository.Add(_);
				return new Response<BaseIdentification>(PublicResultStatusCodes.Done, result);
			}
			catch (Exception e)
			{
				return new Response<BaseIdentification>(PublicResultStatusCodes.QueryHasError);
			}
		}

		public async Task<Response<PublicUser>> Select(int ID)
		{
			try
			{
				var user = await _repository.GetById(ID);
				return new Response<PublicUser>(PublicResultStatusCodes.Done, user);
			}
			catch (Exception e)
			{
				return new Response<PublicUser>(PublicResultStatusCodes.QueryHasError);
			}
		}

		public async Task<Response<PublicUser>> SelectIdentification(string identification)
		{
			try
			{
				var user = await _repository.GetByIdentification(identification);
				return new Response<PublicUser>(PublicResultStatusCodes.Done, user);
			}
			catch (Exception e)
			{
				return new Response<PublicUser>(PublicResultStatusCodes.QueryHasError);
			}
		}

		public async Task<Response<PublicUser>> SelectPersonalNumber(string personalNumber)
		{
			try
			{
				var user = await _repository.GetByPersonalNumber(personalNumber);
				return new Response<PublicUser>(PublicResultStatusCodes.Done, user);
			}
			catch (Exception e)
			{
				return new Response<PublicUser>(PublicResultStatusCodes.QueryHasError);
			}
		}

		public async Task<Response<bool>> ClearTokens(string identification)
		{
			try
			{
				var userByIdRepod = await SelectIdentification(identification);
				var userById = userByIdRepod.Data.First();
				userById.AccessToken = null;
				userById.RefreshToken = null;
				await Update(userById);
				return new Response<bool>(PublicResultStatusCodes.Done, true);
			}
			catch
			{
				return new Response<bool>(PublicResultStatusCodes.SuspiciousAttempt, false);
			}
		}

		public async Task<Response<SMSTokenModel>> SetSMS(string identification)
		{
			try
			{
				var userByIdRepod = await SelectIdentification(identification);
				var userById = userByIdRepod.Data.FirstOrDefault();
				var now = DateTime.Now;

				if (userById == null)
					return new Response<SMSTokenModel>(PublicResultStatusCodes.SuspiciousAttempt, new SMSTokenModel { Identification = identification, Validity = 0 });

				if (userById.SMSTokenDT != null)
				{
					var difference = (TimeSpan)(now - userById.SMSTokenDT);
					if (difference.TotalMinutes < 2)
						return new Response<SMSTokenModel>(PublicResultStatusCodes.Done, new SMSTokenModel { Identification = identification, Validity = 120 - (int)difference.TotalSeconds });
				}
				var random = new Random().Next(1000, 9999);
				userById.SMSToken = random;
				userById.SMSTokenDT = now;
				await Update(userById);
				return new Response<SMSTokenModel>(PublicResultStatusCodes.Done, new SMSTokenModel { Identification = identification, Validity = 120 });
			}
			catch (Exception e)
			{
				return new Response<SMSTokenModel>(PublicResultStatusCodes.EditOperationWasNotPerformed, new SMSTokenModel { Identification = identification, Validity = 0 });
			}
		}

		public async Task<Response<bool>> StoreImages(Base64Images base64)
		{
			try
			{
				var userRepod = await SelectIdentification(base64.Identification);

				var user = userRepod.Data.FirstOrDefault();

				if (user == null)
					throw new Exception("Kick to exception");

				var state = (await AccountState(base64.Identification)).Data.First();
				if (!state.FrontID)
					user.PathIDFront = ImageEncryption.Save(base64.FrontID);
				if (!state.BackID)
					user.PathIDBack = ImageEncryption.Save(base64.BackID);
				if (!state.Selfie)
					user.PathSelfie = ImageEncryption.Save(base64.Selfie);
				var IsIndividual = string.IsNullOrEmpty(base64.BizDokument);
				if (!IsIndividual && !state.BizDokument)
					user.PathBusinessCertificate ??= ImageEncryption.Save(base64.BizDokument);

				user.IsIndividual = IsIndividual;
				await Update(user);

				return new Response<bool>(PublicResultStatusCodes.Done, true);
			}
			catch
			{
				return new Response<bool>(PublicResultStatusCodes.ModelIsNotValid);
			}
		}

		public async Task Update(PublicUser _)
		{
			try
			{
				await _repository.Update(_);
			}
			catch (Exception e) { }
		}

		public async Task<Response<bool>> UpdatePassword(PasswordResetApiModel reset)
		{
			try
			{
				var userByIdRepod = await SelectIdentification(reset.Identification);
				if (userByIdRepod.Status != 0)
					return new Response<bool>(PublicResultStatusCodes.ClientIdNotValid, false);
				var userById = userByIdRepod.Data.FirstOrDefault();
				if (userById == null)
					return new Response<bool>(PublicResultStatusCodes.ClientIdNotValid, false);

				if (!HashHelper.AreEqual(reset.Password, Convert.FromBase64String(userById.PasswordSalt), Convert.FromBase64String(userById.PasswordHash)))
					return new Response<bool>(PublicResultStatusCodes.WrongOldPassword, false);

				userById.PasswordHash = reset.PasswordHash;
				userById.PasswordSalt = reset.PasswordSalt;

				await Update(userById);

				return new Response<bool>(PublicResultStatusCodes.Done, true);
			}
			catch
			{
				return new Response<bool>(PublicResultStatusCodes.EditOperationWasNotPerformed, true);
			}
		}

		public async Task<Response<BaseIdentification>> VerifySMS(SMSTokenModel sms)
		{
			try
			{
				var userRepod = await SelectIdentification(sms.Identification);
				var user = userRepod.Data.FirstOrDefault();
				if (user == null)
					return new Response<BaseIdentification>(PublicResultStatusCodes.ClientIdNotValid);

				if (user.SMSToken == sms.SMSToken)
				{
					user.IsActive = true;
					user.SMSToken = null;
					user.SMSTokenDT = null;
					await Update(user);
					return new Response<BaseIdentification>(PublicResultStatusCodes.Done, new BaseIdentification { Identification = sms.Identification });
				}
				else
					return new Response<BaseIdentification>(PublicResultStatusCodes.ModelIsNotValid);
			}
			catch
			{
				return new Response<BaseIdentification>(PublicResultStatusCodes.SuspiciousAttempt);
			}
		}

		public async Task<Response<VerificationModel>> AccountState(string identification)
		{
			try
			{
				var userRepod = await SelectIdentification(identification);
				var user = userRepod.Data.FirstOrDefault();
				if (user == null)
					return new Response<VerificationModel>(PublicResultStatusCodes.ClientIdNotValid);

				return new Response<VerificationModel>(PublicResultStatusCodes.Done, new VerificationModel
				{
					Identification = user.Identification,
					IsVerified = user.IsVerified,
					FrontID = user.PathIDFront != null,
					BackID = user.PathIDBack != null,
					Selfie = user.PathSelfie != null,
					BizDokument = user.PathBusinessCertificate != null,
					IsIndividual = user.IsIndividual,
					IsActive = user.IsActive
				});
			}
			catch
			{
				return new Response<VerificationModel>(PublicResultStatusCodes.InternalServerError);
			}
		}
	}
}
