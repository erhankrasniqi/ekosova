﻿using eKosova.SharedModel;
using eKosovaBackend.Helpers;
using eKosovaBackend.JwtAuthentication.Abstractions;
using eKosovaBackend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Security.Claims;
using System.Linq;
using System;
using eKosovaBackend.JwtAuthentication.Types;
using eKosova.Business;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using eKosova.Domain.PublicUsers;
using eKosova.Domain;
using eKosovaBackend.Interfaces;

namespace eKosovaBackend.Services
{
	public interface IAuthService
	{
		Task<Response<LoginResponse>> VerifyLogInUser(Credentials credentials);
		Task<string> GetUserRefreshToken(string username);
		Task UpdateTokens(string username, string newRefreshToken, TokenWithClaimsPrincipal newJwtToken);
		Task<object> Refresh(RefreshTokenModel refreshToken);
	}

	public class AuthService : IAuthService
	{
		private readonly IHttpContextAccessor _contextAccessor;
		private readonly IJwtTokenGenerator _jwtTokenGenerator;
		private IPublicUserService _publicUserService;
		private INotificationService _notificationService;
		private readonly Token _token;
		public AuthService(
						   IHttpContextAccessor contextAccessor,
						   IJwtTokenGenerator jwtTokenGenerator,
						   IOptions<Token> token,
						   IPublicUserService publicUserService,
						   INotificationService notificationService)
		{
			_contextAccessor = contextAccessor;
			_jwtTokenGenerator = jwtTokenGenerator;
			_token = token.Value;
			_publicUserService = publicUserService;
			_notificationService = notificationService;
		}

		public async Task<Response<LoginResponse>> VerifyLogInUser(Credentials credentials)
		{
			var userRepo = await _publicUserService.SelectPersonalNumber(credentials.Account);

			var user = userRepo.Data.FirstOrDefault();

			bool valid = false;

			if (user != null)
				valid = true;

			if (valid)
			{
				if (HashHelper.AreEqual(credentials.Password, Convert.FromBase64String(user.PasswordSalt), Convert.FromBase64String(user.PasswordHash)))
				{
					if (user.PathIDBack == null || user.PathIDFront == null || user.PathSelfie == null || (!user.IsIndividual && user.PathBusinessCertificate == null))
						return new Response<LoginResponse>(PublicResultStatusCodes.NotAuthorized, new LoginResponse { Identification = user.Identification });

					if (!user.IsActive)
						return new Response<LoginResponse>(PublicResultStatusCodes.RecordExist, new LoginResponse { Identification = user.Identification });

					_contextAccessor.HttpContext = IdentityHelper.SetIdentity(_contextAccessor.HttpContext, user);

					var refreshToken = RefreshTokenHelper.GenerateRefreshToken();

					var accessTokenResult = _jwtTokenGenerator.GenerateAccessTokenWithClaimsPrincipal(user.PersonalNumber, AddMyClaims(user));

					await UpdateTokens(user.PersonalNumber, refreshToken, accessTokenResult);

					var response = new LoginResponse
					{
						First = user.First,
						Last = user.Last,
						Identification = user.Identification,
						Token = accessTokenResult.AccessToken,
						RefreshToken = refreshToken,
						ValidTokenTimeInMinutes = _token.ValidTimeInMinutes,
						ValidDateTimeToken = DateTime.Now.AddMinutes(_token.ValidTimeInMinutes),
						Username = user.PersonalNumber,
						Notifications = (await _notificationService.GetByIdentification(user.Identification)).Data.First().Reverse(),
						Email = user.Email
					};

					return new Response<LoginResponse>(PublicResultStatusCodes.Done, response);
				}
				return new Response<LoginResponse>(PublicResultStatusCodes.WrongOldPassword);
			}
			return new Response<LoginResponse>(PublicResultStatusCodes.ClientIdNotValid);
		}

		private static IEnumerable<Claim> AddMyClaims(PublicUser user)
		{
			var myClaims = new List<Claim>
			{
				new Claim(ClaimTypes.NameIdentifier, user.ID.ToString()),
				new Claim(ClaimTypes.Name, user.PersonalNumber),
			};

			return myClaims;
		}

		public async Task<string> GetUserRefreshToken(string username)
		{
			var userRepod = await _publicUserService.SelectPersonalNumber(username);
			var user = userRepod.Data.FirstOrDefault();
			return user.RefreshToken;
		}

		public async Task UpdateTokens(string username, string newRefreshToken, TokenWithClaimsPrincipal jwtToken)
		{
			var userRepod = await _publicUserService.SelectPersonalNumber(username);
			var user = userRepod.Data.FirstOrDefault();
			user.RefreshToken = newRefreshToken;
			user.AccessToken = jwtToken.AccessToken;
			await _publicUserService.Update(user);
		}

		public async Task<object> Refresh(RefreshTokenModel refreshToken)
		{
			try
			{
				var principal = GetPrincipalFromExpiredToken(refreshToken.Token);

				var username = principal.Identity.Name;

				var savedRefreshToken = GetUserRefreshToken(username).Result;

				if (savedRefreshToken != refreshToken.RefreshToken)
					return new { Status = PublicResultStatusCodes.NotAuthorized };

				var newJwtToken = _jwtTokenGenerator.GenerateAccessTokenWithClaimsPrincipal(username, principal.Claims);

				var newRefreshToken = RefreshTokenHelper.GenerateRefreshToken();

				await UpdateTokens(username, newRefreshToken, newJwtToken);

				return new
				{
					Status = PublicResultStatusCodes.Done,
					Data = new List<object> {
						new {
							RefreshToken = newRefreshToken,
							Token = newJwtToken.AccessToken,
							ValidTokenTimeInMinutes =_token.ValidTimeInMinutes,
							ValidDateTimeToken = DateTime.Now.AddMinutes(_token.ValidTimeInMinutes) }
					}
				};
			}
			catch
			{
				return new { Status = PublicResultStatusCodes.NotAuthorized };
			}
		}

		private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
		{
			var tokenValidationParameters = new TokenValidationParameters
			{
				ValidateAudience = false,
				ValidateIssuer = false,
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_token.SigningKey)),
				ValidateLifetime = false // expired token
			};

			var tokenHandler = new JwtSecurityTokenHandler();

			var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);

			var jwtSecurityToken = securityToken as JwtSecurityToken;

			if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
				throw new SecurityTokenException("Invalid token");

			return principal;
		}

	}
}
