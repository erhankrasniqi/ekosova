﻿using eKosova.Domain;
using eKosova.Domain.PublicUsers;
using eKosova.Domain.SeedWork;
using eKosovaBackend.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace eKosovaBackend.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private IPublicUserService _publicUserService;

		public UserController(IPublicUserService publicUserRepoService)
		{ _publicUserService = publicUserRepoService; }

		[HttpPost("create")]
		public async Task<IActionResult> CreatePublicUser([FromBody] PublicUser user)
			=> Ok(await _publicUserService.Create(user));

		[HttpPost("smstoken")]
		public async Task<IActionResult> SetSMSToken([FromBody] BaseIdentification model)
			=> Ok(await _publicUserService.SetSMS(model.Identification));

		[HttpPost("verifysms")]
		public async Task<IActionResult> VerifySMSToken([FromBody] SMSTokenModel sms)
			=> Ok(await _publicUserService.VerifySMS(sms));

		[HttpPost("images")]
		public async Task<IActionResult> StoreImages([FromBody] Base64Images base64)
			=> Ok(await _publicUserService.StoreImages(base64));

		[HttpPost("updatePassword")]
		public async Task<IActionResult> UpdatePassword([FromBody] PasswordResetApiModel reset)
			=> Ok(await _publicUserService.UpdatePassword(reset));

		[HttpPost("clearTokens")]
		public async Task<IActionResult> ClearTokens([FromBody] BaseIdentification model)
			=> Ok(await _publicUserService.ClearTokens(model.Identification));

		[HttpPost("accountState")]
		public async Task<IActionResult> AccountState([FromBody] BaseIdentification model)
			=> Ok(await _publicUserService.AccountState(model.Identification));
	}
}
