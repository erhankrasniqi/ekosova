﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosova.SharedModel.Contacts;
using eKosova.SharedModel.Medias;
using eKosovaBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MediaController : ControllerBase
    {
        private readonly IMediaService _mediaService;

        public MediaController(IMediaService mediaService)
        {
            _mediaService = mediaService;
        }

        [HttpPost("create")]
        [ProducesResponseType(typeof(Response<MediaModel>), 200)]
        public IActionResult Create([FromBody] CreateMediaModel _) => Ok(_mediaService.Create(_));
    }
}
