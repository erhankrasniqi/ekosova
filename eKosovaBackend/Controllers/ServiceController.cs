﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosova.Domain.ServiceCategories;
using eKosovaBackend.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaBackend.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ServiceController : ControllerBase
	{
		private readonly IServiceCategoryService _service;

		public ServiceController(IServiceCategoryService service) {
			_service = service;
		}

		[HttpGet("getServices")]
		[ProducesResponseType(typeof(Response<ServiceCategory>), 200)]
		public IActionResult GetServiceCategories() => Ok(_service.GetResponseList());

		[HttpGet("getSubServices")]
		[ProducesResponseType(typeof(Response<ServiceSubCategory>), 200)]
		public IActionResult GetServiceSubCategories(int ID) => Ok(_service.GetResponseSubCategoryList(ID));
	}
}
