﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosova.Domain.Faqs;
using eKosovaBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FaqController : ControllerBase
    {
        private readonly IFaqService _faqService;

        public FaqController(IFaqService faqService)
        {
            _faqService = faqService;
        }
        [HttpGet("getFaqs")]
        [ProducesResponseType(typeof(Response<List<Faq>>), 200)]
        public IActionResult GetFaqs() => Ok(_faqService.GetResponseFaqList());

        [HttpGet("getActiveFaqs")]
        [ProducesResponseType(typeof(Response<List<Faq>>), 200)]
        public IActionResult getActiveFaqs() => Ok(_faqService.GetResponseFaqList());
    }
}
