﻿using eKosova.Domain.Informations;
using eKosovaBackend.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace eKosovaBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InformationController : ControllerBase
    {
        private readonly IInformationService _informationService;
        public InformationController(IInformationService informationService)
        {
            _informationService = informationService;
        }

        [HttpGet("getInformations")]
        [SwaggerOperation(Summary = "Get Informations", Description = "This is endpoint where you get informations list.")]
        [ProducesResponseType(typeof(Response<Information>), 200)]
        public IActionResult GetInformations() => Ok(_informationService.GetResponseInformationList());

        [HttpGet("getInformationsCategory")]
        [SwaggerOperation(Summary = "Get Informations Category", Description = "This is endpoint where you get information categories list.")]
        [ProducesResponseType(typeof(Response<Information>), 200)]
        public IActionResult GetInformationsCategory() => Ok(_informationService.GetResponseInformationCategoryList());
    }
}
