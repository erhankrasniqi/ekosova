﻿using eKosovaBackend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eKosovaBackend.JwtAuthentication.Abstractions;
using eKosovaBackend.Models;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;

namespace eKosovaBackend.Controllers
{
	public class HomeController : Controller
	{
		private readonly IAuthService _authService;
		private readonly IHttpContextAccessor _contextAccessor;
		private readonly IJwtTokenGenerator _jwtTokenGenerator;
		private readonly IWebHostEnvironment _env;
		private readonly Token _token;

		public HomeController(IAuthService authService, IHttpContextAccessor contextAccessor, IJwtTokenGenerator jwtTokenGenerator, IOptions<Token> token,
			IWebHostEnvironment env)
		{
			_contextAccessor = contextAccessor;
			_authService = authService;
			_jwtTokenGenerator = jwtTokenGenerator;
			_token = token.Value;
			_env = env;
		}

		[HttpGet]
		public ActionResult Index() => Content("CanYouSeeMe?");
	}
}
