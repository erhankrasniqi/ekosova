﻿using System.Threading.Tasks;
using eKosova.Domain.Notifications;
using eKosovaBackend.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaBackend.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class NotificationController : ControllerBase
	{
		private readonly INotificationService _service;

		public NotificationController(INotificationService service) { _service = service; }

		[HttpGet("getId")]
		[ProducesResponseType(typeof(Response<Notification>), 200)]
		public async Task<IActionResult> GetById()
			=> Ok(await _service.GetById(7));

		[HttpGet("getIdentification")]
		[ProducesResponseType(typeof(Response<Notification>), 200)]
		public async Task<IActionResult> GetByIdentification()
			=> Ok(await _service.GetByIdentification("ignoreme_"));
	}
}
