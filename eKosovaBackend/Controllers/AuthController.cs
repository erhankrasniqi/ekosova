﻿using eKosova.Domain;
using eKosovaBackend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaBackend.Controllers
{
	public class AuthController : Controller
	{
		private readonly IAuthService _authService;
		public AuthController(IAuthService authService)
		{ _authService = authService; }

		[HttpGet]
		[Authorize]
		public ActionResult AccessLimited() => Content("Authorized API action");

		[HttpPost("authenticate")]
		public IActionResult Authenticate([FromBody] Credentials credentials)
			=> Ok(_authService.VerifyLogInUser(credentials).Result);

		[HttpPost("refresh")]
		public IActionResult Refresh([FromBody] RefreshTokenModel refreshToken)
			=> Ok(_authService.Refresh(refreshToken).Result);
	}
}
