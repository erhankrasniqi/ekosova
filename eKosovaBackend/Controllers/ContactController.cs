﻿using eKosova.SharedModel.Contacts;
using eKosovaBackend.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly IContactService _contactService;
        public ContactController(IContactService contactService)
        {
            _contactService = contactService;
        }

        [HttpPost("create")]
        [SwaggerOperation(Summary = "Create Contact", Description = "This is endpoint where a contact can be created by the user through filling the contact form.")]
        //[ProducesResponseType(typeof(Response<ContactModel>), 200)]
        public IActionResult Create([FromBody] CreateContactModel _) => Ok(_contactService.Create(_));
    }
}
