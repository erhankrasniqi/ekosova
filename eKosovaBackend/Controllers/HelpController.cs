﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosova.SharedModel.Helps;
using eKosovaBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace eKosovaBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelpController : ControllerBase
    {
        private readonly IHelpService _helpService;
        public HelpController(IHelpService helpService)
        {
            _helpService = helpService;
        }

        [HttpPost("create")]
        [SwaggerOperation(Summary = "Create Help", Description = "This is endpoint where a help can be created by the user through filling the help form.")]
        [ProducesResponseType(typeof(Response<HelpModel>), 200)]
        public IActionResult Create([FromBody] CreateHelpModel _) => Ok(_helpService.Create(_));
    }
}
