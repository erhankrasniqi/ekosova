﻿using Microsoft.IdentityModel.Tokens;
using eKosovaBackend.JwtAuthentication.Types;

namespace eKosovaBackend.JwtAuthentication.Extensions
{
	public static class TokenValidationParametersExtensions
	{
		public static TokenOptions ToTokenOptions(this TokenValidationParameters tokenValidationParameters,
			int tokenExpiryInMinutes = 5)
		{
			return new TokenOptions(tokenValidationParameters.ValidIssuer,
				tokenValidationParameters.ValidAudience,
				tokenValidationParameters.IssuerSigningKey,
				tokenExpiryInMinutes);
		}
	}
}
