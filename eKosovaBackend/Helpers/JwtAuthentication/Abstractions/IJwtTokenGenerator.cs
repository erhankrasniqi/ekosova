﻿using eKosovaBackend.JwtAuthentication.Types;
using System.Collections.Generic;
using System.Security.Claims;

namespace eKosovaBackend.JwtAuthentication.Abstractions
{
	public interface IJwtTokenGenerator
	{
		TokenWithClaimsPrincipal GenerateAccessTokenWithClaimsPrincipal(string userName,
			IEnumerable<Claim> userClaims);
		string GenerateAccessToken(string userName, IEnumerable<Claim> userClaims);
	}
}
