﻿using eKosova.Domain.PublicUsers;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;

namespace eKosovaBackend.Helpers
{
	public static class IdentityHelper
	{
		public static HttpContext SetIdentity(HttpContext context, PublicUser user)
		{
			var claims = new[] {
									new Claim(ClaimTypes.Name,user.PersonalNumber),
									new Claim(ClaimTypes.NameIdentifier, user.ID.ToString()),
								};
			var identity = new ClaimsIdentity(claims, AuthenticationSchemes.Basic.ToString());
			var userPrincipal = new GenericPrincipal(identity, new[] { "admin" });
			context.User = userPrincipal;

			return context;
		}
	}
}