using System;
using System.IO;
using System.Text;
using AutoMapper;
using eKosova.Domain;
using eKosova.Domain.Contacts;
using eKosova.Domain.Faqs;
using eKosova.Domain.Helps;
using eKosova.Domain.Informations;
using eKosova.Domain.Media;
using eKosova.Domain.Notifications;
using eKosova.Domain.PublicUsers;
using eKosova.Domain.SeedWork;
using eKosova.Domain.ServiceCategories;
using eKosova.Infrastructure.Contacts;
using eKosova.Infrastructure.Faqs;
using eKosova.Infrastructure.Helps;
using eKosova.Infrastructure.Informations;
using eKosova.Infrastructure.Medias;
using eKosova.Infrastructure.Notifications;
using eKosova.Infrastructure.PublicUsers;
using eKosova.Infrastructure.Repositories;
using eKosovaBackend.Helpers;
using eKosovaBackend.Interfaces;
using eKosovaBackend.JwtAuthentication.Extensions;
using eKosovaBackend.Models;
using eKosovaBackend.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using RepoDb;

namespace eKosovaBackend
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

			services.AddSession(options =>
			{
				options.IdleTimeout = TimeSpan.FromMinutes(30);
				options.Cookie.HttpOnly = true;
				options.Cookie.IsEssential = true;
			});

			services.AddSwaggerGen(c => {
				c.EnableAnnotations();
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "eKosova API", Version = "v1" });
			});

			var validationParams = new TokenValidationParameters
			{
				ClockSkew = System.TimeSpan.Zero,

				ValidateAudience = true,
				ValidAudience = Configuration["Token:Audience"],

				ValidateIssuer = true,
				ValidIssuer = Configuration["Token:Issuer"],

				IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["Token:SigningKey"])),
				ValidateIssuerSigningKey = true,

				RequireExpirationTime = true,
				ValidateLifetime = true,

			};

			var validTime = int.Parse(Configuration["Token:ValidTimeInMinutes"]);

			services.AddJwtAuthenticationForAPI(validationParams, validTime);

			SqlServerBootstrap.Initialize();

			services.AddScoped<IAuthService, AuthService>();
			services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));
			services.AddScoped(typeof(IAsyncRepository<>), typeof(GenericRepository<>));
			services.AddScoped<IContactService, ContactService>();
			services.AddScoped<IContactRepoService, ContactRepoService>();
			services.AddScoped<IHelpService, HelpService>();
			services.AddScoped<IHelpRepoService, HelpRepoService>();
			services.AddScoped<IMediaService, MediaService>();
			services.AddScoped<IMediaRepoService, MediaRepoService>();
			services.AddScoped<IInformationService, InformationService>();
			services.AddScoped<IInformationRepository, InformationRepository>();
			services.AddScoped<IPublicUserRepoService, PublicUserRepoService>();
			services.AddScoped<IPublicUserService, PublicUserService>();
			services.AddScoped<IServiceCategoryRepository, ServiceCategoryRepository>();
			services.AddScoped<IServiceCategoryService, ServiceCategoryService>();

			services.AddScoped<INotificationService, NotificationService>();
			services.AddScoped<INotificationRepoService, NotificationRepoService>();

            services.AddScoped<IFaqService, FaqService>();
            services.AddScoped<IFaqRepoService, FaqRepoService>();

            services.Configure<Token>(Configuration.GetSection("Token"));

			services.AddAutoMapper(typeof(Startup));

			var appSettingsSection = Configuration.GetSection("AppSettings");

			var appSettings = appSettingsSection.Get<AppSettings>();

			services.AddMvc(_ => _.EnableEndpointRouting = false);

			services.Configure<AppSettings>(appSettingsSection);

			services.AddControllers();

			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseSession();

			app.UseMvc();

			// app.UseMiddleware<CustomUnauthorizeResponseMiddleware>();

			app.UseSwagger();

			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "eKosova API Version 1");
			});

			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthentication();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
