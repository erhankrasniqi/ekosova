﻿using AutoMapper;
using eKosova.Domain.Contacts;
using eKosova.Domain.Faqs;
using eKosova.Domain.Helps;
using eKosova.Domain.Media;
using eKosova.SharedModel.Contacts;
using eKosova.SharedModel.Faqs;
using eKosova.SharedModel.Helps;
using eKosova.SharedModel.Medias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaBackend.Profiles
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<CreateContactModel, Contact>().ReverseMap();
            CreateMap<ContactModel, Contact>().ReverseMap();
            CreateMap<CreateHelpModel, Help>().ReverseMap();
            CreateMap<HelpModel, Help>().ReverseMap();
            CreateMap<MediaModel, Media>().ReverseMap();
            CreateMap<CreateMediaModel, Media>().ReverseMap();
            CreateMap<FaqModel, Faq>().ReverseMap();


        }
    }
}
