﻿using eKosova.SharedModel;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace eKosovaBackend
{
	public class Response<T> //where T : class
	{
		[JsonProperty("Status")]
		public int Status { get; set; }

		[JsonProperty("Data", NullValueHandling = NullValueHandling.Ignore)]
		public IEnumerable<T> Data { get; set; }

		public Response()
		{
		}

		public Response(PublicResultStatusCodes status)
		{
			Status = (int)status;
		}

		public Response(PublicResultStatusCodes status, IList<T> data)
		{
			Status = (int)status;
			Data = data;
		}

		public Response(PublicResultStatusCodes status, T data)
		{
			Status = (int)status;
			Data = new List<T>() { data };
		}
	}
}
