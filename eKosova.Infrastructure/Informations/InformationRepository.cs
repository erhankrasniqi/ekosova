﻿using eKosova.Domain;
using eKosova.Domain.Informations;
using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eKosova.Infrastructure.Informations
{
    public class InformationRepository : IInformationRepository
    {
        private readonly IRepository<Information> _informationRepository;
        private readonly IRepository<InformationCategory> _informationCategoryRepository;
        public InformationRepository(IRepository<Information> informationRepository,
                                     IRepository<InformationCategory> informationCategoryRepository)
        {
            _informationRepository = informationRepository;
            _informationCategoryRepository = informationCategoryRepository;
        }
        //Information
        public void AddInformation(Information _) {
            throw new NotImplementedException();
                }
        public Information GetInformationById(int id) { throw new NotImplementedException(); }
        public IEnumerable<Information> GetInformations() {
            var infos = _informationRepository.GetAll();
            foreach(var item in infos)
            {
                item.InformationCategories = GetInformationCategoryById(item.ID).ToList();
            }
            return infos;
        }

        //InformationCategory
        public void AddInformationCategory(InformationCategory _) { throw new NotImplementedException(); }
        public IEnumerable<InformationCategory> GetInformationCategories() {
            return _informationCategoryRepository.GetAll();
        }

        public IEnumerable<InformationCategory> GetInformationCategoryById(int id)
        {
            return _informationCategoryRepository.GetAll().Where(c => c.IDInformation == id);
        }
	}
}
