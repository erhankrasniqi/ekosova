﻿using eKosova.Domain.PublicUsers;
using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosova.Infrastructure.PublicUsers
{
	public class PublicUserRepoService : IPublicUserRepoService
	{
		private readonly IRepository<PublicUser> _repository;

		public PublicUserRepoService(IRepository<PublicUser> repository)
		{
			_repository = repository;
		}

		public async Task<BaseIdentification> Add(PublicUser _)
		{
			var query = "EXEC [dbo].[InsertPublicUser] @PasswordHash, @PasswordSalt, @PhoneNumber, @IsIndividual, @PersonalNumber, @Email, @Identification, @First, @Last";
			//var query = "EXEC [dbo].[InsertPublicUser] @PhoneNumber, @IsIndividual, @PersonalNumber, @Email";
			var Identification = Guid.NewGuid().ToString();
			object parameters = new
			{
				_.PasswordHash,
				_.PasswordSalt,
				_.PhoneNumber,
				_.IsIndividual,
				_.PersonalNumber,
				_.Email,
				Identification,
				_.First,
				_.Last
			};
			await _repository.Create(_, query, parameters);
			return new BaseIdentification { Identification = Identification };
		}

		public IEnumerable<PublicUser> GetActive()
		{
			throw new NotImplementedException();
		}

		public IEnumerable<PublicUser> GetAll()
		{
			throw new NotImplementedException();
		}

		public async Task<PublicUser> GetById(int id)
		{
			var query = "EXEC [dbo].[SelectPublicUser] @ID";
			object parameters = new
			{
				ID = id
			};
			return await _repository.Read(query, parameters);
		}

		public async Task<PublicUser> GetByIdentification(string identification)
		{
			var query = "EXEC [dbo].[SelectPublicUserIdentification] @Identification";
			object parameters = new
			{
				Identification = identification
			};
			return await _repository.Read(query, parameters);
		}

		public async Task<PublicUser> GetByPersonalNumber(string personalNumber)
		{
			var query = "EXEC [dbo].[SelectPublicUserPersonalNumber] @PersonalNumber";
			object parameters = new
			{
				PersonalNumber = personalNumber
			};
			return await _repository.Read(query, parameters);
		}

		public async Task Update(PublicUser _)
		{
			var query = "EXEC [dbo].[UpdatePublicUser] @ID, @SMSToken, @SMSTokenDT, @First, @Last, @PasswordHash, @PasswordSalt, @PhoneNumber, @Email, @PathIDFront, @PathIDBack, @PathSelfie, @PathBusinessCertificate, @PathBusinessAuthorizationDocument, @AccessToken, @RefreshToken, @IsVerified, @VerifyDate, @IDInternalUser, @IsActive, @IsIndividual";
			await _repository.Update(query, _);
		}
	}
}
