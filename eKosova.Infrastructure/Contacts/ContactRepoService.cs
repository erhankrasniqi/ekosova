﻿using eKosova.Domain.Contacts;
using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Infrastructure.Contacts
{
    public class ContactRepoService : IContactRepoService
    {
        private readonly IRepository<Contact> _contactRepository;
        public ContactRepoService(IRepository<Contact> contactRepository)
        {
            _contactRepository = contactRepository;
        }

        public void AddContact(Contact _)
        {
            string query = "EXEC [dbo].[InsertContact] @FullName, @Email, @Description";
            object parameters = new { _.FullName, _.Email, _.Description};
            _contactRepository.Create(_,query,parameters);
        }

        public IEnumerable<Contact> GetActiveContacts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Contact> GetAllContacts()
        {
            throw new NotImplementedException();
        }

        public Contact GetContactById(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateContact(Contact _)
        {
            throw new NotImplementedException();
        }
    }
}
