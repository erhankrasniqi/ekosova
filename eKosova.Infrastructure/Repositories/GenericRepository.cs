﻿using eKosova.Domain.SeedWork;
using RepoDb;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace eKosova.Infrastructure.Repositories
{
	public class GenericRepository<T> : BaseRepository<T, SqlConnection>, IRepository<T>, IAsyncRepository<T> where T : BaseEntity
	{
		private static readonly string connectionString = appConfig.Default.ConnectionString;

		public GenericRepository() : base(connectionString) { }

		public async Task<T> Create(T model, string query, object parameters)
		{
			try
			{
				using (var db = await new SqlConnection(connectionString).EnsureOpenAsync())
				{
					var response = (await db.ExecuteQueryAsync<T>(query, parameters)).FirstOrDefault();
					return response;
				}
			}
			catch (Exception error)
			{
				return null;
			}
		}

		public async Task<T> Read(string query, object parameters)
		{
			try
			{
				using (var db = await new SqlConnection(connectionString).EnsureOpenAsync())
				{
					var response = (await db.ExecuteQueryAsync<T>(query, parameters)).FirstOrDefault();
					return response;
				}
			}
			catch (Exception error)
			{
				return null;
			}
		}

		public async Task Update(string query, T model)
		{
			try
			{
				using (var db = await new SqlConnection(connectionString).EnsureOpenAsync())
				{
					var response = (await db.ExecuteQueryAsync<T>(query, model)).FirstOrDefault();
				}
			}
			catch (Exception error)
			{
				//
			}
		}

		public Task<T> CreateAsync(T model)
		{
			throw new NotImplementedException();
		}

		public void Delete(T model)
		{
			throw new NotImplementedException();
		}

		public Task DeleteAsync(T model)
		{
			throw new NotImplementedException();
		}

		public int DeleteId(int id)
		{
			throw new NotImplementedException();
		}

		public Task<int> DeleteIdAsync(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<T> GetAll()
		{
			try
			{
				using (var connection = new SqlConnection(connectionString).EnsureOpen())
				{
					var list = connection.QueryAll<T>();
					return list;
				}

			}
			catch (Exception e)

			{
				return null;
			}
		}

		public async Task<IEnumerable<T>> GetAllSpecific(string query, object parameters)
		{
			try
			{
				using (var db = await new SqlConnection(connectionString).EnsureOpenAsync())
				{
					var response = (await db.ExecuteQueryAsync<T>(query, parameters));
					return response;
				}
			}
			catch (Exception)

			{
				return null;
			}
		}

		public Task<IEnumerable<T>> GetAllAsync()
		{
			throw new NotImplementedException();
		}

		public Task<T> ReadAsync(int id)
		{
			throw new NotImplementedException();
		}

		public void Update(T model)
		{
			throw new NotImplementedException();
		}

		public Task UpdateAsync(T model)
		{
			throw new NotImplementedException();
		}

		public async Task<IEnumerable<T>> GetAllQuery(string query)
		{
			try
			{
				using (var connection = new SqlConnection(connectionString).EnsureOpen())
				{

					//var list = connection.QueryAll<T>();
					var response = (await connection.ExecuteQueryAsync<T>(query));
					return response;
				}

			}
			catch (Exception e)
			{
				return null;
			}
		}

	}
}
