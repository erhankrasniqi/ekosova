﻿using eKosova.Domain.Notifications;
using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eKosova.Infrastructure.Notifications
{
	public class NotificationRepoService : INotificationRepoService
	{
		private readonly IRepository<Notification> _repository;

		public NotificationRepoService(IRepository<Notification> repository)
		{
			_repository = repository;
		}

		public async Task<Notification> GetById(int ID)
		{
			var query = "EXEC [dbo].[SelectNotificationById] @ID";
			object parameters = new { ID };
			return await _repository.Read(query, parameters);
		}

		public async Task<IEnumerable<Notification>> GetByIdentification(string identification)
		{
			var query = "EXEC [dbo].[SelectNotificationByIdentification] @Identification";
			object parameters = new
			{
				Identification = identification
			};
			return await _repository.GetAllSpecific(query, parameters);
		}

		public async Task<IEnumerable<Notification>> GetUnRead(string identification)
		{
			var result = await GetByIdentification(identification);
			return result.Where(_ => _.IsRead == false);
		}

		public async Task Update(Notification _)
		{
			var query = "EXEC [dbo].[UpdateNotificationRead] @ID, @Identification";
			await _repository.Update(query, _);
		}
	}
}
