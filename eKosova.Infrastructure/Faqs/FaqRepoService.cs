﻿using eKosova.Domain.Faqs;
using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Infrastructure.Faqs
{
  public  class FaqRepoService : IFaqRepoService
    {
        private readonly IRepository<Faq> _faqRepository;
        public  FaqRepoService(IRepository<Faq> faqRepository)
        {
            _faqRepository = faqRepository;
        }

        public void AddFaq(Faq _)
        {
            throw new NotImplementedException();
            //string query = "EXEC [dbo].[InsertFaq] @Question, @Answer, @IDInternalUser, @IDMedia";
            //object parameters = new { _.Question, _.Answer, _.IDInternalUser, _.IDMedia };
            //_faqRepository.Create(_, query, parameters);
        }

        public IEnumerable<Faq> GetActiveFaqs()
        {
            var faqList = _faqRepository.GetAll();
            return faqList;
        }

        public IEnumerable<Faq> GetAllFaqs()
        {
            var faqList = _faqRepository.GetAll();
             return faqList;
        }

        public Faq GetFaqByID(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateFaq(Faq _)
        {
            throw new NotImplementedException();
        }




        //public readonly IRepository<Faq> _faqRepository;

        //public FaqRepoService(IRepository<Faq> faqRepository)
        //{
        //    _faqRepository = faqRepository;
        //}

        //public void AddFaq(Faq _)
        //{
        //    //throw new NotImplementedException();

        //    string query = "EXEC [dbo].[InsertFaq] @Question, @Answer, @IDInternalUser, @IDMedia";
        //    object parameters = new { _.Question, _.Answer, _.IDInternalUser, _.IDMedia };
        //    _faqRepository.Create(_, query, parameters);
        //}

        //public IEnumerable<Faq> GetActiveFaqs()
        //{
        //    var faqList = _faqRepository.GetAll();
        //    return faqList;
        //}

        //public IEnumerable<Faq> GetAllFaqs()
        //{
        //    var faqList = _faqRepository.GetAll();
        //    return faqList;
        //}

        //public Faq GetFaqByID(int id)
        //{
        //    throw new NotImplementedException();
        //}

        //public void UpdateFaq(Faq _)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
