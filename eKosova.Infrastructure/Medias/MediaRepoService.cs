﻿using eKosova.Domain.Media; 
using eKosova.Domain.SeedWork; 
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Infrastructure.Medias
{
   public class MediaRepoService : IMediaRepoService
    {
        private readonly IRepository<Media> _mediaRepository;

        public MediaRepoService(IRepository<Media> mediaRepository)
        {
            _mediaRepository = mediaRepository;
        }

        public void AddMedia(Media _)
        {
            string query = "EXEC [dbo].[InsertMedia] @VideoPath, @VoicePath, @Description";
            object parameters = new { _.VideoPath, _.VoicePath, _.Description };
            _mediaRepository.Create(_,query,parameters);
        }

        public IEnumerable<Media> GetActiveMedias()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Media> GetAllMedias()
        {
            throw new NotImplementedException();
        }

        public Media GetMediaById(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateMedia(Media _)
        {
            throw new NotImplementedException();
        }
    }
}
