﻿using eKosova.Domain.Helps;
using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Infrastructure.Helps
{
    public class HelpRepoService : IHelpRepoService
    {
        private readonly IRepository<Help> _helpRepository;
        public HelpRepoService(IRepository<Help> helpRepository)
        {
            _helpRepository = helpRepository;
        }
        public void AddHelp(Help _)
        {
            string query = "EXEC [dbo].[InsertHelp] @FullName, @Email, @Description";
            object parameters = new { _.FullName, _.Email, _.Description };
            _helpRepository.Create(_, query, parameters);
        }

        public IEnumerable<Help> GetActiveHelps()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Help> GetAllHelps()
        {
            throw new NotImplementedException();
        }
        

        public Help GetHelpById(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateHelp(Help _)
        {
            throw new NotImplementedException();
        }
    }
}
