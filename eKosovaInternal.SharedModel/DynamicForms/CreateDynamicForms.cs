﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.DynamicForms
{
    public class CreateDynamicForms
    {
        public DynamicFormModel DynamicForm { get; set; }
        public List<QuestionModel> Questions { get; set; }
    }
}
