﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.DynamicForms
{
    public class DynamicFormQuestionGridXY
    {
        public int IDGrid_X_Questions { get; set; }
        public int IDGrid_Y_Questions { get; set; }
        public int IDDynamicFormQuestions { get; set; }
    }
}
