﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.DynamicForms
{
    public class DynamicFormGridOption
    {
        public string Title { get; set; }
        public int IDDynamicFormQuestions { get; set; }
    }
}
