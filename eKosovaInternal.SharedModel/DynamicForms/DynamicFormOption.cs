﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.DynamicForms
{
    public class DynamicFormOption
    {
        public string Title { get; set; }
        public int IDDynamicFormQuestion { get; set; }
    }
}
