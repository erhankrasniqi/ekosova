﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.DynamicForms
{
    public class QuestionModel
    {
        public int ID { get; set; }
        public int IDDynamicForm { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? IsRequired { get; set; }
        public string Section { get; set; }
        public int IDDynamicFormQuestionType { get; set; }
        public bool? ShowQuestion { get; set; }
        public bool? HasAnswer { get; set; }
        public int IDDynamicFormQuestionParent { get; set; }
        public int? LevelOfQuestion { get; set; }
        public List<QuestionModel> SubQuestion { get; set; } = new List<QuestionModel>();
        public List<DynamicFormOption> QuestionOptions { get; set; } = new List<DynamicFormOption>();
        public List<DynamicFormGridOption> QuestionGridOptionsX { get; set; } = new List<DynamicFormGridOption>();
        public List<DynamicFormGridOption> QuestionGridOptionsY { get; set; } = new List<DynamicFormGridOption>();
    }
}
