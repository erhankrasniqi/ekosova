﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Medias
{
    public class MediaModel
    {
        public int ID { get; set; }
        public string VideoPath { get; set; }
        public string VoicePath { get; set; }
        public string Description { get; set; } 
    }
}
