﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Logs
{
    public class LogGeneralModel
    {
        public int ID { get; set; }
        public DateTime EntryDate { get; set; }
        public string ComputerName { get; set; }
        public string IPAddress { get; set; }
        public int IDLogBrowserType { get; set; }
        public int IDLogOperationSystemType { get; set; }
        public bool IsMobileDevice { get; set; }
        public int IDLogUserAuthorizationStatus { get; set; }
    }
}
