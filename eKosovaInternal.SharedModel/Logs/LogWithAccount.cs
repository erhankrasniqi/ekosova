﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Logs
{
    public class LogWithAccount : LogGeneralModel
    {
        public string Account { get; set; }
    }
}
