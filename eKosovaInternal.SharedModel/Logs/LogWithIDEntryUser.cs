﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Logs
{
    public class LogWithIdEntryUser : LogGeneralModel
    {
        public int IDEntryUser { get; set; }
        public string EntryUser { get; set; }
    }
}
