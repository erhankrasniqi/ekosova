﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Logs
{
    public class LogWithIdUser : LogGeneralModel
    {
        public int IDUser { get; set; }
        public string EntryUser { get; set; }

    }
}
