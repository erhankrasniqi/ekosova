﻿using eKosovaInternal.SharedModel.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Logs
{
    public class LogUserActivitySearchModel : PagingParameters
    {
        public int? IDUser { get; set; }
        public string ComputerName { get; set; }
        public string IPAddress { get; set; }
        public string LogBrowserType { get; set; }
        public string LogOperatingSystemType { get; set; }
        public int? IDModule { get; set; }
        public string Module { get; set; }
        public bool? IsMobileDevice { get; set; }
        public bool? IsPublic { get; set; }
        public string URL { get; set; }
        public int? IDLogUserActivityStatus { get; set; }
        public string LogUserActivityStatus { get; set; }
        public string EntryUser { get; set; }
        public DateTime? EntryDate { get; set; }
    }
}
