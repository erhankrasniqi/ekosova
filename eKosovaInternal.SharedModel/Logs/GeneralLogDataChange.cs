﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Logs
{
    public class GeneralLogDataChange
    {
        public int ID { get; set; }
        public DateTime EntryDate { get; set; }
        public int IDEntryUser { get; set; }
        public string EntryUser { get; set; }
        public string ComputerName { get; set; }
        public string IPAddress { get; set; }
        public string LogBrowserType { get; set; }
        public string LogOperatingSystemType { get; set; }
        public bool IsMobileDevice { get; set; }
        public string LogDataChangeStatus { get; set; }

    }
}
