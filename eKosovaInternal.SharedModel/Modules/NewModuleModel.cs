﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Modules
{
    public class NewModuleModel
    {
        public string Title { get; set; }
        public string Description { get; set; }

    }
    public class NewModuleValidator : AbstractValidator<NewModuleModel>
    {
        public NewModuleValidator()
        {
            RuleFor(x => x.Title).NotEmpty();
        }
    }
}
