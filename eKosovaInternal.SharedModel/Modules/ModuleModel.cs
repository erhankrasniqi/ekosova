﻿using eKosovaInternal.SharedModel.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Modules
{
    public class ModuleModel : LookupBaseModel
    {
        public int IDRole { get; set; }
        public int IDUser { get; set; }
        public int? IDParent { get; set; }
        public int IDRoleAuthorizationType { get; set; }
        public int IDRoleAuthorization { get; set; }
        public string FullName { get; set; }
        public int MarginDistance { get; set; }
        public List<int> IDs { get; set; }
    }
}
