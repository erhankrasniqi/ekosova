﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Faqs
{
  public  class EditFaqModel 
    {
        public int Id { get; set; }
        public DateTime InsertionDate { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int IDInternalUser { get; set; }

        public string MediaVideo { get; set; }
        public string MediaVoice { get; set; }
        public string MediaDescription { get; set; }
    }
}
