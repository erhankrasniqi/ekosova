﻿using System;
using System.Collections.Generic;
using System.Text; 
using eKosovaInternal.Domain.Medias;

namespace eKosovaInternal.SharedModel.Faqs
{
   public class FaqModel
    {
        public int Id { get; set; }
        public DateTime InsertionDate { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int IDInternalUser { get; set; }

        public string MediaVideo { get; set; }
        public string MediaVoice { get; set; }
        public string MediaDescription { get; set; }



        //public FaqModel()
        //{
        //    Media = new HashSet<Media>();
        //}
        //public int Id { get; set; }
        //public DateTime InsertationDate { get; set; }
        //public string Question { get; set; }
        //public string Answer { get; set; }
        //public int IDInternalUser { get; set; }

        //public virtual ICollection<Media> Media { get; set; }
    }
}
