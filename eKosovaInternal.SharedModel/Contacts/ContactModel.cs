﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Contacts
{
    public class ContactModel
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public DateTime InsertionDate { get; set; }
        public string Answer { get; set; }
    }
}
