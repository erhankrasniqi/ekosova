﻿using eKosovaInternal.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Edergesa
{
 public   class SubmissionPersonalUserModel
    {
		 
		public string First { get; set; }
		public string Last { get; set; } 
		public string PhoneNumber { get; set; }
		public string Email { get; set; } 
		public string PersonalNumber { get; set; }
		public string BusinessNumber { get; set; }
        public string BusinessName { get; set; }
        public string PathIDFront { get; set; }
		public string PathIDBack { get; set; }
		public string PathSelfie { get; set; }
		public string PathBusinessCertificate { get; set; }   
		public Base64Images Base64 { get; set; }
	}
}
