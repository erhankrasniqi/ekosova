﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Submission
{
    public class InsertSubmissionDocumentModel
    {
        public int IDSubmission { get; set; }
        public string Path { get; set; }
        public int Size { get; set; }
        public string Extensiontype { get; set; }
        public string Name { get; set; }
    }
}
