﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Submission
{
   public class HierarchysubmissionTypeModel
    {
        public int ID { get; set; }
        public int IDHierarchy { get; set; }
        public string Title { get; set; }
    }
}
