﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.General
{
    public class Pagination<T> where T : class
    {
        public int Count { get; set; } = 1;
        public int PageSize { get; set; }
        public int Page { get; set; }
        public IList<T> Result { get; set; }
    }
}
