﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.PublicUserHistory
{
   public class PublicUserHistoryModel
    {
        public int ID { get; set; }
        public DateTime InsertionDate { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
    }
}
