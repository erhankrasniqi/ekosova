﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Notifications
{
    public class CreateNotificationQueue
    {
        public string Message { get; set; }
        public string ToEmail { get; set; }
        public string ToPhone { get; set; }
        public bool IsSent { get; set; }
        public string Identification { get; set; }
        public int IDNotificationQueueType { get; set; }
    }
}
