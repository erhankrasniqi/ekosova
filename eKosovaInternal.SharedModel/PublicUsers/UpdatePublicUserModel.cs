﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.PublicUsers
{
    public class UpdatePublicUserModel
    {
        public int ID { get; set; }
        public bool IDFront { get; set; }
        public bool IDBack { get; set; }
        public bool IDSelfie { get; set; }
        public string Reason { get; set; }
    }
}
