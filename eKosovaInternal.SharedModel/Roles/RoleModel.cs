﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Roles
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool WithPasswordPolicy { get; set; }
        public bool CanBeDeleted { get; set; }
        public int? PasswordValidityDays { get; set; }
    }
}
