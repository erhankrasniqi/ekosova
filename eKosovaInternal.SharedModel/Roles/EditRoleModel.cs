﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Roles
{
    public class EditRoleModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool WithPasswordPolicy { get; set; }
        public int? PasswordValidityDays { get; set; }

    }

    public class EditRoleValidator : AbstractValidator<EditRoleModel>
    {
        public EditRoleValidator()
        {
            RuleFor(x => x.Title).NotEmpty();
        }
    }
}
