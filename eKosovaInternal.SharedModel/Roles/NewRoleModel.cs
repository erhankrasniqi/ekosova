﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Roles
{
    public class NewRoleModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool WithPasswordPolicy { get; set; }
        public int? PasswordValidityDays { get; set; }

    }

    public class NewRoleValidator : AbstractValidator<NewRoleModel>
    {
        public NewRoleValidator()
        {
            RuleFor(x => x.Title).NotEmpty();
            RuleFor(x => x.PasswordValidityDays).NotNull().When(t => t.WithPasswordPolicy);
        }
    }
}
