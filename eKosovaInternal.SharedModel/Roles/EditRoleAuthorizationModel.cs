﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Roles
{
    public class EditRoleAuthorizationModel
    {
        public int IDRoleAuthorizationType { get; set; }
    }

    public class UpdateRoleAuthorizationValidator : AbstractValidator<EditRoleAuthorizationModel>
    {
        public UpdateRoleAuthorizationValidator()
        {
            RuleFor(x => x.IDRoleAuthorizationType).GreaterThan(0);
        }
    }
}
