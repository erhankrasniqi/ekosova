﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.SharedModel.Roles
{
    public class RoleAuthorizationModel
    {
        public int ID { get; set; }
        public int IDRole { get; set; }
        public int IDRoleAuthorizationType { get; set; }
        public int IDModule { get; set; }
    }
}
