﻿using System.Collections.Generic;

namespace eKosova.Domain
{
	public class ApiResponse<T>
    {
        public int Status { get; set; }
        public IList<T> Data { get; set; }
        public T SingleData { get; set; }
    }
}
