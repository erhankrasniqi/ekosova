﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.Informations
{
    public interface IInformationRepository
    {
        //Information
        void AddInformation(Information _);
        Information GetInformationById(int id);
        IEnumerable<Information> GetInformations();

        //InformationCategory
        void AddInformationCategory(InformationCategory _);
        IEnumerable<InformationCategory> GetInformationCategoryById(int id);
        IEnumerable<InformationCategory> GetInformationCategories();
    }
}
