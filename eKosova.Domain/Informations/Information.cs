﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.Informations
{
    public class Information : BaseEntity
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string IconContent { get; set; }
        public DateTime InsertionDate { get; set; }
        public int IDInternalUser { get; set; }
        public List<InformationCategory> InformationCategories { get; set; } 
    }
}
