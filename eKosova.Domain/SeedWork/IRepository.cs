﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosova.Domain.SeedWork
{
	public interface IRepository<T> where T : BaseEntity
	{
		Task<T> Create(T model, string query, object param);
		void Delete(T model);
		int DeleteId(int id);
		IEnumerable<T> GetAll();
		Task<T> Read(string query, object param);
		Task Update(string query, T model);
		Task<IEnumerable<T>> GetAllSpecific(string query, object param);
		Task<IEnumerable<T>> GetAllQuery(string query);
	}
}
