﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosova.Domain.SeedWork
{
	public interface IAsyncRepository<T> where T : BaseEntity
	{
		Task<T> CreateAsync(T model);
		Task DeleteAsync(T model);
		Task<int> DeleteIdAsync(int id);
		Task<IEnumerable<T>> GetAllAsync();
		Task<T> ReadAsync(int id);
		Task UpdateAsync(T model);
	}
}
