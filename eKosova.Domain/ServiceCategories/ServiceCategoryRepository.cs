﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosova.Domain.ServiceCategories
{
	public class ServiceCategoryRepository : IServiceCategoryRepository
	{
		private readonly IRepository<ServiceCategory> _serviceRepository;
		private readonly IRepository<ServiceSubCategory> _serviceSubRepository;
		private readonly IRepository<ServiceSubCategoryDynamicForm> _serviceSubDynamicFormRepository;

		public ServiceCategoryRepository(IRepository<ServiceCategory> serviceRepository,
							 IRepository<ServiceSubCategory> serviceSubRepository,
							 IRepository<ServiceSubCategoryDynamicForm> serviceSubDynamicFormRepository)
		{
			_serviceRepository = serviceRepository;
			_serviceSubRepository = serviceSubRepository;
			_serviceSubDynamicFormRepository = serviceSubDynamicFormRepository;
		}


		public void Add(ServiceCategory _)
		{
			throw new NotImplementedException();
		}

		public void AddSubCategory(ServiceSubCategory _)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<ServiceCategory> GetAll()
		{
			return _serviceRepository.GetAll();
		}
		public IEnumerable<ServiceSubCategory> GetAllSub()
		{
			return _serviceSubRepository.GetAll();
		}

        public IEnumerable<ServiceSubCategoryDynamicForm> GetAllWithDynamicForm(int IDServiceCategory)
        {
			string query = "EXEC [dbo].[SelectSubServices] @IDServiceCategory";
			object parameters = new { IDServiceCategory };
			return _serviceSubDynamicFormRepository.GetAllSpecific(query, parameters).Result;
		}

        public ServiceCategory GetById(int id)
		{
			throw new NotImplementedException();
		}

		public ServiceSubCategory GetSubById(int id)
		{
			throw new NotImplementedException();
		}
	}
}
