﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.ServiceCategories
{
	public class ServiceSubCategoryDynamicForm : BaseEntity
	{
		public string Name { get; set; }
		public DateTime InsertionDate { get; set; }
		public string IconPath { get; set; }
		public int IDInternalUser { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int IDServiceCategory { get; set; }
    }
}
