﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosova.Domain.ServiceCategories
{
	public interface IServiceCategoryRepository
	{
        void Add(ServiceCategory _);
        ServiceCategory GetById(int id);
        IEnumerable<ServiceCategory> GetAll();
        IEnumerable<ServiceSubCategoryDynamicForm> GetAllWithDynamicForm(int IDServiceCategoryId);

        void AddSubCategory(ServiceSubCategory _);
        ServiceSubCategory GetSubById(int id);
        IEnumerable<ServiceSubCategory> GetAllSub();
    }
}
