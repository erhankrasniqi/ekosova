﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.ServiceCategories
{
	public class ServiceSubCategory : BaseEntity
	{
		public string Name { get; set; }
		public int IDServiceCategory { get; set; }
		public string IconPath { get; set; }
	}
}
