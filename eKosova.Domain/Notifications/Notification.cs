﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.Notifications
{
	public class Notification : BaseEntity
	{
		public string Message { get; set; }
		public int Type { get; set; }
		public bool IsRead { get; set; }
		public DateTime SentDate { get; set; }
	}
}
