﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosova.Domain.Notifications
{
	public interface INotificationRepoService
	{
		Task Update(Notification _);
		Task<Notification> GetById(int ID);
		Task<IEnumerable<Notification>> GetByIdentification(string identification);
		Task<IEnumerable<Notification>> GetUnRead(string identification);
	}
}
