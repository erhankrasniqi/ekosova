﻿using System.Collections.Generic;
using eKosova.Domain.SeedWork;
using System.Threading.Tasks;

namespace eKosova.Domain.PublicUsers
{
	public interface IPublicUserRepoService
	{
		Task<BaseIdentification> Add(PublicUser _);
		Task Update(PublicUser _);
		Task<PublicUser> GetById(int id);
		Task<PublicUser> GetByPersonalNumber(string personalNumber);
		Task<PublicUser> GetByIdentification(string identification);
		IEnumerable<PublicUser> GetAll();
		IEnumerable<PublicUser> GetActive();
	}
}
