﻿using eKosova.Domain.SeedWork;

namespace eKosova.Domain.PublicUsers
{
	public class VerificationModel : BaseIdentification
	{
		public bool FrontID { get; set; }
		public bool BackID { get; set; }
		public bool Selfie { get; set; }
		public bool BizDokument { get; set; }
		public bool? IsVerified { get; set; }
		public bool IsIndividual { get; set; }
		public bool IsActive { get; set; }
	}
}
