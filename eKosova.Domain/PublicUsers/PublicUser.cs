﻿using eKosova.Domain.SeedWork;
using System;

namespace eKosova.Domain.PublicUsers
{
	public class PublicUser : BaseEntity
	{
		public int? SMSToken { get; set; }
		public DateTime? SMSTokenDT { get; set; }
		public string First { get; set; }
		public string Last { get; set; }
		public string PasswordHash { get; set; }
		public string PasswordSalt { get; set; }
		public DateTime InsertionDate { get; set; }
		public string PhoneNumber { get; set; }
		public string Email { get; set; }
		public bool IsIndividual { get; set; }
		public string PersonalNumber { get; set; }
		public string BusinessNumber { get; set; }
		public string PathIDFront { get; set; }
		public string PathIDBack { get; set; }
		public string PathSelfie { get; set; }
		public string PathBusinessCertificate { get; set; }
		public string PathBusinessAuthorizationDocument { get; set; }
		public string AccessToken { get; set; }
		public string RefreshToken { get; set; }
		public bool? IsVerified { get; set; }
		public DateTime? VerifyDate { get; set; }
		public int? IDInternalUser { get; set; }
		public bool IsActive { get; set; }
		public string Identification { get; set; }
		public string BusinessName { get; set; }
	}
}
