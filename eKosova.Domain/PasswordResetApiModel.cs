﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain
{
	public class PasswordResetApiModel : BaseIdentification
	{
		public string PasswordHash { get; set; }
		public string PasswordSalt { get; set; }
		public string Password { get; set; }
	}
}
