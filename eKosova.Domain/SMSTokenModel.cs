﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eKosova.Domain
{
	public class SMSTokenModel : BaseIdentification
	{
		[Required]
		public int SMSToken { get; set; }
		public int Validity { get; set; }
	}
}
