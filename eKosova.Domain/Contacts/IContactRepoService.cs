﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.Contacts
{
    public interface IContactRepoService
    {
        void AddContact(Contact _);
        void UpdateContact(Contact _);
        Contact GetContactById(int id);
        IEnumerable<Contact> GetAllContacts();
        IEnumerable<Contact> GetActiveContacts();
        
    }
}
