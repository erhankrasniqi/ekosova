﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.DynamicForms
{
    public class DynamicFormQuestions : BaseEntity
    {
        public int IDDynamicForm { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? IsRequired { get; set; }
        public string Section { get; set; }
        public int IDDynamicFormQuestionType { get; set; }
        public bool? ShowQuestion { get; set; }
        public bool? HasAnswer { get; set; }
        public int IDDynamicFormQuestionParent { get; set; }
        public int? LevelOfQuestion { get; set; }
    }
}
