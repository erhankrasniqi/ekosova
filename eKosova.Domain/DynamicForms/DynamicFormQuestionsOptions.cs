﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.DynamicForms
{
    public class DynamicFormQuestionsOptions : BaseEntity
    {
        public string Title { get; set; }
        public int? IDDynamicFormQuestion { get; set; }
    }
}
