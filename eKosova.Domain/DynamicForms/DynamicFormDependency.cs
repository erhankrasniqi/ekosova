﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.DynamicForms
{
    public class DynamicFormDependency : BaseEntity
    {
        public int IDDynamicFormQuestion { get; set; }
        public int? X_ID { get; set; }
        public int? Y_ID { get; set; }
        public int? Xi_ID { get; set; }
        public int? Yi_ID { get; set; }
        public int? MathematicalExpression { get; set; }
    }
}
