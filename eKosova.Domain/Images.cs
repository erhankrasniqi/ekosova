﻿using eKosova.Domain.SeedWork;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace eKosova.Domain
{
	public class Images : BaseIdentification
	{
		public IFormFile FrontID { get; set; }
		public IFormFile BackID { get; set; }
		public IFormFile Selfie { get; set; }
		public IFormFile BizDokument { get; set; }
	}

	public class Base64Images : BaseIdentification
	{
		public string FrontID { get; set; }
		public string BackID { get; set; }
		public string Selfie { get; set; }
		public string BizDokument { get; set; }
	}

	public static class FileExtension
	{
		public static async Task<byte[]> GetBytes(this IFormFile formFile)
		{
			using (var memoryStream = new MemoryStream())
			{
				await formFile.CopyToAsync(memoryStream);
				return memoryStream.ToArray();
			}
		}
	}
}
