﻿using System.ComponentModel.DataAnnotations;

namespace eKosova.Domain
{
	public class Credentials
	{
		public string Account { get; set; }
		public string Password { get; set; }
	}
}
