﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.Faqs
{
   public interface IFaqRepoService
    {
        void AddFaq(Faq _);
        void UpdateFaq(Faq _);
        Faq GetFaqByID(int id);
        IEnumerable<Faq> GetAllFaqs();
        IEnumerable<Faq> GetActiveFaqs();
    }
}
