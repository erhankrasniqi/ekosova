﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.Faqs
{
    public class Faq : BaseEntity
    {
        public DateTime InsertionDate { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int IDInternalUser { get; set; }

        public int IDMedia { get; set; }
    }
}
