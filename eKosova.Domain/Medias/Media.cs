﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace eKosova.Domain.Media
{
    public partial class Media : BaseEntity
    {
        public string VideoPath { get; set; }
        public string VoicePath { get; set; }
        public string Description { get; set; }
    }
}
