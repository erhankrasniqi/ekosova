﻿using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.Helps
{
    public partial class Help : BaseEntity
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public DateTime InsertionDate { get; set; }
    }
}
