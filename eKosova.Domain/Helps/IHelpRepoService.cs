﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosova.Domain.Helps
{
    public interface IHelpRepoService
    {
        void AddHelp(Help _);
        void UpdateHelp(Help _);
        Help GetHelpById(int id);
        IEnumerable<Help> GetAllHelps();
        IEnumerable<Help> GetActiveHelps();
    }
}
