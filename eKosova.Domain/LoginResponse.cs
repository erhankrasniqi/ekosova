﻿using eKosova.Domain.Notifications;
using eKosova.Domain.SeedWork;
using System;
using System.Collections.Generic;

namespace eKosova.Domain
{
	public class LoginResponse : BaseIdentification
	{
		public string Token { get; set; }
		public string RefreshToken { get; set; }
		public string First { get; set; }
		public string Last { get; set; }
		public string Email { get; set; }
		public string Username { get; set; }
		public int ValidTokenTimeInMinutes { get; set; }
		public DateTime ValidDateTimeToken { get; set; }
		public IEnumerable<Notification> Notifications { get; set; }
	}
}
