﻿using eKosovaInternal.Domain.InternalUsers;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;

namespace eKosovaInternal.Business.Helpers
{
	public static class IdentityHelper
	{
		public static HttpContext SetIdentity(HttpContext context, InternalUser user)
		{
			var claims = new[] {
									new Claim(ClaimTypes.Name, user.Account),
									new Claim(ClaimTypes.NameIdentifier, user.Identification),
								};
			var identity = new ClaimsIdentity(claims, AuthenticationSchemes.Basic.ToString());
			var userPrincipal = new GenericPrincipal(identity, new[] { "admin" });
			context.User = userPrincipal;
			return context;
		}
	}
}