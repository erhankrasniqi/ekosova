﻿using eKosovaInternal.Business.JwtAuthentication.Types;
using System.Collections.Generic;
using System.Security.Claims;

namespace eKosovaInternal.Business.JwtAuthentication.Abstractions
{
	public interface IJwtTokenGenerator
	{
		TokenWithClaimsPrincipal GenerateAccessTokenWithClaimsPrincipal(string userName,
			IEnumerable<Claim> userClaims);
		string GenerateAccessToken(string userName, IEnumerable<Claim> userClaims);
	}
}
