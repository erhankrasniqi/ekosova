﻿using Microsoft.IdentityModel.Tokens;
using eKosovaInternal.Business.JwtAuthentication.Types;

namespace eKosovaInternal.Business.JwtAuthentication.Extensions
{
	public static class TokenValidationParametersExtensions
	{
		public static TokenOptions ToTokenOptions(this TokenValidationParameters tokenValidationParameters,
			int tokenExpiryInMinutes = 5)
		{
			return new TokenOptions(tokenValidationParameters.ValidIssuer,
				tokenValidationParameters.ValidAudience,
				tokenValidationParameters.IssuerSigningKey,
				tokenExpiryInMinutes);
		}
	}
}
