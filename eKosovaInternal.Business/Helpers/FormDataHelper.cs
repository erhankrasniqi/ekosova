﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace eKosovaInternal.Helpers
{
    public static class FormDataHelper
    {
        public static MultipartFormDataContent GetContent<T>(T model)
        {
            var formDataContent = new MultipartFormDataContent();
            var type = model.GetType();

            foreach (var item in type.GetProperties())
            {
                if(typeof(IFormFile).IsAssignableFrom(item.PropertyType))
                {
                    if (item.GetValue(model, null) != null)
                        formDataContent.Add(GetFileContent((IFormFile)item.GetValue(model, null)), item.Name, ((IFormFile)item.GetValue(model, null)).FileName);
                }
                else if (item.GetValue(model, null) != null)
                    formDataContent.Add(new StringContent(item.GetValue(model, null)?.ToString() ?? null), item.Name);
            }
            return formDataContent;
        }

        public static StreamContent GetFileContent(IFormFile file)
        {
            var fileContent = new StreamContent(file.OpenReadStream())
            {
                Headers =
                {
                    ContentLength = file.Length,
                    ContentType = new MediaTypeHeaderValue(file.ContentType)
                }
            };
            return fileContent;
        }

        public static Dictionary<string, string> GetContentForWordDocument<T>(T model)
        {
            var type = model.GetType();
            var dictionary = new Dictionary<string, string>();
            foreach (var item in type.GetProperties())
            {
                dictionary.Add(item.Name, item.GetValue(model, null)?.ToString());
            }
            return dictionary;
        }
    }
}
