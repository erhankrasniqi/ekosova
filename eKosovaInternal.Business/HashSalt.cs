﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace eKosovaBackend.Helpers
{
	public class HashSalt
	{
		public byte[] Salt { get; set; }
		public byte[] Hash { get; set; }

		public HashSalt(string plainPass)
		{
			Salt = HashHelper.GenerateSalt();
			Hash = HashHelper.GenerateHash(plainPass, Salt);
		}
	}

	public static class HashHelper
	{
		public static byte[] GenerateSalt()
		{
			var buffer = new byte[64];
			new RNGCryptoServiceProvider().GetBytes(buffer);
			return buffer;
		}

		public static byte[] GenerateHash(string plainPass, byte[] salt)
		=> new SHA256Managed().ComputeHash(
				Encoding.UTF8.GetBytes(plainPass + Convert.ToBase64String(salt)));

		public static bool AreEqual(string plainPass, byte[] salt, byte[] hash)
			=> GenerateHash(plainPass, salt).SequenceEqual(hash);
	}
}
