﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace eKosovaInternal.Domain
{
	public static class ImageEncryption
	{
		private const string PassPhrase = "3K050V4 / PBC!2O2O";

		private const string Salt = "20200915";

		public static byte[] EncryptBytes(byte[] inputBytes)
		{
			RijndaelManaged RijndaelCipher = new RijndaelManaged
			{
				Mode = CipherMode.CBC
			};
			byte[] salt = Encoding.ASCII.GetBytes(Salt);
			PasswordDeriveBytes password = new PasswordDeriveBytes(PassPhrase, salt, "SHA1", 2);

			ICryptoTransform encrypter = RijndaelCipher.CreateEncryptor(password.GetBytes(32), password.GetBytes(16));

			MemoryStream memoryStream = new MemoryStream();
			CryptoStream cryptoStream = new CryptoStream(memoryStream, encrypter, CryptoStreamMode.Write);
			cryptoStream.Write(inputBytes, 0, inputBytes.Length);
			cryptoStream.FlushFinalBlock();
			byte[] CipherBytes = memoryStream.ToArray();

			memoryStream.Close();
			cryptoStream.Close();

			return CipherBytes;
		}

		public static byte[] DecryptBytes(byte[] encryptedBytes)
		{
			RijndaelManaged RijndaelCipher = new RijndaelManaged
			{
				Mode = CipherMode.CBC
			};
			byte[] salt = Encoding.ASCII.GetBytes(Salt);
			PasswordDeriveBytes password = new PasswordDeriveBytes(PassPhrase, salt, "SHA1", 2);

			ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(password.GetBytes(32), password.GetBytes(16));

			MemoryStream memoryStream = new MemoryStream(encryptedBytes);
			CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);
			byte[] plainBytes = new byte[encryptedBytes.Length];

			cryptoStream.Read(plainBytes, 0, plainBytes.Length);

			memoryStream.Close();
			cryptoStream.Close();

			return plainBytes;
		}

		public static void ByteArrayToFile(string fileName, byte[] byteArray)
		{
			try
			{
				using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
				{
					fs.Write(byteArray, 0, byteArray.Length);
				}
			}
			catch
			{
				// ma von
			}
		}
	}
}
