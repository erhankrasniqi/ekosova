﻿using eKosovaInternal.Business;
using eKosovaInternal.SharedModel.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
	public interface IRoleService
	{
		IEnumerable<RoleModel> GetRoles();
		Response<RoleModel> GetResponseRoles();
		IEnumerable<RoleModel> GetRolesWithInactive();
		Response<RoleModel> GetResponseRolesWithInactive();

		RoleModel GetRole(int id);
		IEnumerable<RoleModel> GetPasiveRoles();
		IEnumerable<RoleModel> GetAllRoles();
		Response<RoleModel> GetResponseRole(int id);

		Response<RoleModel> CreateRole(NewRoleModel _);
		Response<RoleModel> UpdateRole(int id, EditRoleModel _);
		Response<RoleAuthorizationModel> UpdateRoleAuthorization(int id, EditRoleAuthorizationModel _);
	}
}
