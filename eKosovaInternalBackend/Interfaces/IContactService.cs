﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.Contacts;
using eKosovaInternal.Domain.Emails;
using eKosovaInternal.SharedModel.Contacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.Domain.Notifications;

namespace eKosovaInternalBackend.Interfaces
{
    public interface IContactService
    {
        IEnumerable<ContactModel> GetContactList();
        IEnumerable<ContactModel> GetActiveContactList();
        Response<ContactModel> GetResponseContactList();
        Response<ContactModel> GetResponseActiveContactList();
        Response<Contact> UpdateContact(Contact _);
        Response<Contact> PushContactNotification(Contact _); 



         
    }
}
