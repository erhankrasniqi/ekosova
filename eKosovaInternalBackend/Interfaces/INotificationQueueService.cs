﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.Notifications;
using eKosovaInternal.SharedModel.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
    public interface INotificationQueueService
    {
        Response<NotificationQueue> Create(CreateNotificationQueue _);

    }
}
