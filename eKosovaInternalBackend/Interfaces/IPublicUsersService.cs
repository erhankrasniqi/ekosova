﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.SharedModel.PublicUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
   public interface IPublicUserService
    {
        IEnumerable<PublicUser> GetPublicUserList();
        IEnumerable<PublicUser> GetActivePublicUserList();
        Response<PublicUser> GetResponsePublicUserList();
        Response<PublicUser> GetResponseActivePublicUserList();
        Response<UpdatePublicUserModel> UpdatePublicUser(UpdatePublicUserModel _);
        Task<Response<PublicUser>> GetResponsePublicUserPersonalNumber(string identification);
    }
}
