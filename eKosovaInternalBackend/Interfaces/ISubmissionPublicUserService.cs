﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.Submission;
using eKosovaInternal.SharedModel.Edergesa;
using eKosovaInternal.SharedModel.Submission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
   public interface ISubmissionPublicUserService
    {
        Response<SubmissionPersonalUserModel> GetSubmissionPublicUserList(int personalNumber);
        IEnumerable<HierarchysubmissionTypeModel> GetHieararchySubmission();
        Response<InsertSubmission> Create(InsertSubmissionModel _);
        Response<InsertSubmissionDocument> Create(InsertSubmissionDocumentModel _);

    }
}
