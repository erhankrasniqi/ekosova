﻿using eKosovaInternal.Business;
using eKosovaInternal.SharedModel.General;
using eKosovaInternal.SharedModel.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
    public interface IModuleService
    {
        IEnumerable<DefinationBaseModel> GetModules();

        Response<DefinationBaseModel> GetResponseModule();
        Response<ModuleModel> GetResponseModule(int IDRole);
        Response<ModuleModel> GetResponseModuleForUser(int IDUser);
        Response<ModuleModel> GetResponseModuleTree();
        Response<DefinationBaseModel> AddModule(NewModuleModel _);

        Response<LookupBaseModel> GetReportModules();
    }
}
