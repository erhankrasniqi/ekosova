﻿using eKosovaInternal.Business;
using eKosovaInternal.SharedModel.PublicUserHistory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
   public interface IPublicUserHistoryService
    {
       Response<PublicUserHistoryModel> GetPublicUserHistory(int publicUserId);
    }
}
