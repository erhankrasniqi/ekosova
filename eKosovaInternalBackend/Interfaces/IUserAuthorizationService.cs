﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.InternalUsers;

namespace eKosovaInternalBackend.Interfaces
{
	public interface IUserAuthorizationService
	{
		Response<UserAuthorizationType> GetAll();
	}
}
