﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain;
using eKosovaInternal.Domain.ADDomains;
using eKosovaInternal.Domain.InternalUsers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
	public interface IADDomainService
	{
		Task<bool> CheckForAdUser(InternalUser user, Credentials credentials);
		Task<Response<ADDomainViewModel>> GetAll();
	}
}
