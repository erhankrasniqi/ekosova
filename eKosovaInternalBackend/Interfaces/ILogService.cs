﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.Logs;
using eKosovaInternal.SharedModel.Logs;
//using eKosovaInternal.SharedModel.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
    public interface ILogService
    {
        void AddLogFailedAuthentication(string username);
        Task AddLogUserAuthorization(int authorizationStatus);
        Task<Response<LogUserAuthorization>> SetLogUserAuthorization(int authorizationStatus);
        IEnumerable<LogWithIdUser> GetLogUserAuthorization(int idUser);
        IEnumerable<LogWithIdUser> GetLogUserAuthorizations();
        Response<LogWithIdUser> GetResponseLogUserAuthorization(int idUser);
        IEnumerable<LogWithAccount> GetLogFailedAuthentications(DateTime? entryDate);
        IEnumerable<LogWithAccount> GetLogFailedAuthenticationsAll();
        Response<LogWithAccount> GetResponseLogFailedAuthentications(DateTime? entryDate);
        Response<LogWithAccount> GetResponseLogFailedAuthenticationsAll();

        void AddLogUserActivity(int idModule, int logUserActivityStatus, string Host, bool isPublic);
        Response<LogUserActivity> SetLogUserActivity(LogActivityModel _);
        public IEnumerable<LogWithIdEntryUser> GetLogActivities(int idUser);
        IEnumerable<LogWithIdEntryUser> GetLogActivities();
        public Response<LogWithIdEntryUser> GetResponseLogActivities(int idUser);

        void AddLogDataChange(byte[] before, byte[] after, DataChangeStatus action, string TableName);
        Response<LogWithIdUser> GetResponseLogUserAuthorizations();
    }
}
