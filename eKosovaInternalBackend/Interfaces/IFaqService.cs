﻿using eKosovaInternal.SharedModel.Faqs;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Faqs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
   public interface IFaqService
    {
        IEnumerable<Faq> GetFaqLsit();
        IEnumerable<Faq> GetActiveFaqList();
        Response<Faq> GetResponseFaqList();
        Response<Faq> GetResponseActiveFaqList();
        Response<UpdateFaqModel> UpdateFaq(UpdateFaqModel _);
        //Task<Response<UpdateFaqModel>> UpdateFaq(UpdateFaqModel _);
        Response<FaqModel> Create(CreateFaqModel _);

        Response<DeleteFaq> DeleteFaq(int id);
      
    }
}
