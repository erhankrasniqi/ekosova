﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.InternalUsers;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
	public interface IInternalUserService
	{
		Task<Response<dynamic>> Create(InternalUser _);
		Task<Response<dynamic>> Select(int ID);
		Task<Response<dynamic>> SelectPersonalNumber(string personalNumber);
		Task<Response<dynamic>> SelectIdentification(string identification);
		Task<Response<InternalUser>> SelectAccount(string account);
		Task<Response<InternalUserViewModel>> SelectAccountView(string account);
		Task<Response<bool>> ClearTokens(string identification);
		Task<Response<bool>> UpdateByAdmin(InternalUserViewModel _);
		Response<dynamic> Update(InternalUser _);
		Response<InternalUserViewModel> GetAll();
	}
}