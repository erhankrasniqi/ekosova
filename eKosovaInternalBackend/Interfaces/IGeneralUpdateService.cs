﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
    public interface IGeneralUpdateService<T>
    {
        void UpdateAddLogDataChange(string query, object param);
        void DeleteAddLogDataChange(T entity);
        T InsertAddLogDataChange(T entity,string query, object param, byte[] before, byte[] after = null);
    }
}
