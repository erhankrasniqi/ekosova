﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.Services;
using eKosovaInternal.SharedModel.DynamicForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
    public interface IServiceCategoryService
    {
        Response<ServiceCategory> GetServiceCategories();
    }
}
