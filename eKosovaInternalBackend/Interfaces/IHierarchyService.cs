﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.Hierarchies;

namespace eKosovaInternalBackend.Interfaces
{
	public interface IHierarchyService
	{
		Response<HierarchyViewModel> GetAll();
	}
}
