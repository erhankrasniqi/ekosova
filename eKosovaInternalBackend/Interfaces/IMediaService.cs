﻿using eKosovaInternal.SharedModel.Medias;
using eKosovaInternal.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
   public interface IMediaService
    {
        IEnumerable<MediaModel> GetMediaLsit();
        IEnumerable<MediaModel> GetActiveMediaList();
        Response<MediaModel> GetResponseMediaList();
        Response<MediaModel> GetResponseActiveMediaList();

    }
}
