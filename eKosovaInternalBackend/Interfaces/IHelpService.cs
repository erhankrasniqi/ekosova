﻿using eKosovaInternal.SharedModel.Helps;
using eKosovaInternal.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Domain.Helps;

namespace eKosovaInternalBackend.Interfaces
{
    public interface IHelpService
    {
        IEnumerable<HelpModel> GetHelpList();
        IEnumerable<HelpModel> GetActiveHelpList();

        Response<HelpModel> GetResponseHelpList();
        Response<HelpModel> GetResponseActiveHelpList();
        Response<Help> PushHelpNotification(Help _);


    }
}
