﻿using eKosova.Domain.DynamicForms;
using eKosovaInternal.Business;
using eKosovaInternal.SharedModel.DynamicForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Interfaces
{
    public interface IDynamicFormsService
    {
        Task<Response<bool>> CreateDynamicForm(CreateDynamicForms model);
        Task<Response<DynamicForm>> GetDynamicForms();
    }
}
