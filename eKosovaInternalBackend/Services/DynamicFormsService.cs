﻿using AutoMapper;
using eKosova.Domain.DynamicForms;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.DynamicForms;
using eKosovaInternal.Domain.Services;
using eKosovaInternal.SharedModel.DynamicForms;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
    public class DynamicFormsService : IDynamicFormsService
    {
        private readonly IDynamicFormsRepository _dynamicFormsRepository;
        private readonly IMapper _mapper;

        public DynamicFormsService(IDynamicFormsRepository dynamicFormsRepository, IMapper mapper)
        {
            _dynamicFormsRepository = dynamicFormsRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> CreateDynamicForm(CreateDynamicForms model)
        {
            try
            {
                var baseModel = _mapper.Map<DynamicForm>(model.DynamicForm);
                var dynamicForm = await _dynamicFormsRepository.AddDynamicForm(baseModel);

                foreach (var item in model.Questions)
                {
                    item.IDDynamicForm = dynamicForm;

                    var question = _mapper.Map<DynamicFormQuestions>(item);
                    var questionResponse = await _dynamicFormsRepository.AddQuestion(question);

                    foreach (var itemOptions in item.QuestionOptions)
                    {
                        itemOptions.IDDynamicFormQuestion = questionResponse;
                        var questionOptions = _mapper.Map<DynamicFormQuestionsOptions>(itemOptions);
                        _dynamicFormsRepository.AddQuestionOptions(questionOptions);
                    }

                    foreach (var itemGridX in item.QuestionGridOptionsX)
                    {
                        var questionGridX = _mapper.Map<DynamicFormGridXQuestions>(itemGridX);
                        var responseGridX = await _dynamicFormsRepository.AddDynamicFormQuestionGridX(questionGridX);

                        foreach (var itemGridY in item.QuestionGridOptionsY)
                        {
                            var questionGridY = _mapper.Map<DynamicFormGridYQuestions>(itemGridY);
                            var responseGridY = await _dynamicFormsRepository.AddDynamicFormQuestionGridY(questionGridY);

                            DynamicFormQuestionGridXY gridXY = new DynamicFormQuestionGridXY();
                            gridXY.IDDynamicFormQuestions = questionResponse;
                            gridXY.IDGrid_X_Questions = responseGridX;
                            gridXY.IDGrid_Y_Questions = responseGridY;
                            var gridModelXY = _mapper.Map<DynamicFormGridXY>(gridXY);
                            _dynamicFormsRepository.AddDynamicFormGridXY(gridModelXY);

                        }

                    }

                }

                return new Response<bool>(PublicResultStatusCodes.Done);
            }
            catch (Exception e)
            {
                return new Response<bool>(PublicResultStatusCodes.QueryHasError);
            }
        }

        public async Task<Response<DynamicForm>> GetDynamicForms()
        {
            try
            {
                var list = _dynamicFormsRepository.GetDynamicForms().Result;
                return new Response<DynamicForm>(PublicResultStatusCodes.Done, (IList<DynamicForm>)list);
            }
            catch(Exception e)
            {
                return new Response<DynamicForm>(PublicResultStatusCodes.QueryHasError);
            }
        }
    }
}
