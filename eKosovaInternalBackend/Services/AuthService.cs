﻿using eKosovaInternal.Business.JwtAuthentication.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Security.Claims;
using System;
using eKosovaInternal.Business.JwtAuthentication.Types;
using eKosovaInternal.Business;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using eKosovaInternal.Business.Helpers;
using eKosovaInternal.Domain;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternalBackend.Interfaces;
using eKosovaInternal.Domain.Logs;

namespace eKosovaInternalBackend.Services
{
	public interface IAuthService
	{
		Task<Response<LoginResponse>> VerifyLogInUser(Credentials credentials);
		Task<string> GetUserRefreshToken(string username);
		Task UpdateTokens(string username, string newRefreshToken, TokenWithClaimsPrincipal newJwtToken);
		Task<object> Refresh(RefreshTokenModel refreshToken);
	}

	public class AuthService : IAuthService
	{
		private readonly IHttpContextAccessor _contextAccessor;
		private readonly IJwtTokenGenerator _jwtTokenGenerator;
		private readonly IInternalUserRepoService _internalUserRepoService;
		private readonly IADDomainService _iaddService;
		private readonly IRoleService _roleService;
		private readonly ILogService _logService;
		private readonly Token _token;
		public AuthService(
						   IHttpContextAccessor contextAccessor,
						   IInternalUserRepoService internalUserRepoService,
						   IJwtTokenGenerator jwtTokenGenerator,
						   IADDomainService iaddService,
						   IRoleService roleService,
						   ILogService logService,
						   IOptions<Token> token)
		{
			_iaddService = iaddService;
			_internalUserRepoService = internalUserRepoService;
			_contextAccessor = contextAccessor;
			_jwtTokenGenerator = jwtTokenGenerator;
			_roleService = roleService;
			_logService = logService;
			_token = token.Value;
		}

		public async Task<Response<LoginResponse>> VerifyLogInUser(Credentials credentials)
		{
			var user = await _internalUserRepoService.GetByAccount(credentials.Account);

			bool valid = false;

			if (user != null && user.IsActive)
				valid = true;

			if (valid)
			{
				bool success;

				if (user.IDUserAuthorizationType == 2) success = await _iaddService.CheckForAdUser(user, credentials);

				else success = HashHelper.AreEqual(credentials.Password, Convert.FromBase64String(user.PasswordSalt), Convert.FromBase64String(user.PasswordHash));

				if (success)
				{
					_contextAccessor.HttpContext = IdentityHelper.SetIdentity(_contextAccessor.HttpContext, user);

					var accessTokenResult = _jwtTokenGenerator.GenerateAccessTokenWithClaimsPrincipal(user.Account, AddMyClaims(user));

					var refreshTokenResult = RefreshTokenHelper.GenerateRefreshToken();

					user.AccessToken = accessTokenResult.AccessToken;

					user.RefreshToken = refreshTokenResult;

					_internalUserRepoService.Update(user);

					var response = new LoginResponse
					{
						Identification = user.Identification,
						Token = accessTokenResult.AccessToken,
						RefreshToken = refreshTokenResult,
						ValidTokenTimeInMinutes = _token.ValidTimeInMinutes,
						ValidDateTimeToken = DateTime.Now.AddMinutes(_token.ValidTimeInMinutes),
						Username = user.PersonalNumber,
						Role = user.IDRole
					};
					await _logService.AddLogUserAuthorization((int)UserAuthorizationStatus.SignIn);
					return new Response<LoginResponse>(PublicResultStatusCodes.Done, response);
				}
				else
				{
					_logService.AddLogFailedAuthentication(credentials.Account);
					return new Response<LoginResponse>(PublicResultStatusCodes.WrongOldPassword);
				}
			}
			else
				return new Response<LoginResponse>(PublicResultStatusCodes.ClientIdNotValid);
		}

		private static IEnumerable<Claim> AddMyClaims(InternalUser user)
		{
			var myClaims = new List<Claim>
			{
				new Claim(ClaimTypes.NameIdentifier, user.ID.ToString()),
				new Claim(ClaimTypes.Name, user.Account),
				new Claim(ClaimTypes.Role, user.IDRole.ToString()),
				new Claim("FullName",user.First + " " + user.Last),
				new Claim("Identification", user.Identification)
			};

			return myClaims;
		}

		public async Task<string> GetUserRefreshToken(string username)
		{
			var user = await _internalUserRepoService.GetByPersonalNumber(username);
			return user.RefreshToken;
		}

		public async Task UpdateTokens(string username, string newRefreshToken, TokenWithClaimsPrincipal jwtToken)
		{
			var user = await _internalUserRepoService.GetByPersonalNumber(username);
			user.RefreshToken = newRefreshToken;
			user.AccessToken = jwtToken.AccessToken;
			_internalUserRepoService.Update(user);
		}

		public async Task<object> Refresh(RefreshTokenModel refreshToken)
		{
			try
			{
				var principal = GetPrincipalFromExpiredToken(refreshToken.Token);

				var username = principal.Identity.Name;

				var savedRefreshToken = await GetUserRefreshToken(username);

				if (savedRefreshToken != refreshToken.RefreshToken)
					throw new Exception("Kick to exception");

				var newJwtToken = _jwtTokenGenerator.GenerateAccessTokenWithClaimsPrincipal(username, principal.Claims);

				var newRefreshToken = RefreshTokenHelper.GenerateRefreshToken();

				await UpdateTokens(username, newRefreshToken, newJwtToken);

				return new
				{
					Status = PublicResultStatusCodes.Done,
					Data = new List<object> {
						new {
							RefreshToken = newRefreshToken,
							Token = newJwtToken.AccessToken,
							ValidTokenTimeInMinutes =_token.ValidTimeInMinutes,
							ValidDateTimeToken = DateTime.Now.AddMinutes(_token.ValidTimeInMinutes) }
					}
				};
			}
			catch (Exception e)
			{
				return new { Status = PublicResultStatusCodes.NotAuthorized };
			}
		}

		private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
		{
			var tokenValidationParameters = new TokenValidationParameters
			{
				ValidateAudience = false,
				ValidateIssuer = false,
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_token.SigningKey)),
				ValidateLifetime = false // we know it's expired
			};

			var tokenHandler = new JwtSecurityTokenHandler();

			var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);

			var jwtSecurityToken = securityToken as JwtSecurityToken;

			if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
				throw new SecurityTokenException("Invalid token");

			return principal;
		}
	}
}
