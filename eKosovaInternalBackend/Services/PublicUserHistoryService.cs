﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.PublicUserHistory;
using eKosovaInternal.SharedModel.PublicUserHistory;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
    public class PublicUserHistoryService : IPublicUserHistoryService
    {
        private readonly IPublicUserHistoryRepoService _repository;
        private readonly IMapper _mapper;

        public PublicUserHistoryService(IPublicUserHistoryRepoService repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public Response<PublicUserHistoryModel> GetPublicUserHistory(int publicUserId) => new Response<PublicUserHistoryModel>(PublicResultStatusCodes.Done, _mapper.Map<IList<PublicUserHistoryModel>>(_repository.GetAllPublicUserHistory(publicUserId)));


    }
}
