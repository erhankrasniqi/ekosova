﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Contacts;
using eKosovaInternal.Domain.Emails;
using eKosovaInternal.Domain.Notifications;
using eKosovaInternal.SharedModel.Contacts;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
    public class ContactService : IContactService
    {
        private readonly IContactRepoService _repository;
        private readonly IMapper _mapper;
        private readonly INotificationRepoService _notificationRepository;

        public ContactService(IContactRepoService repository, IMapper mapper, INotificationRepoService notificationRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _notificationRepository = notificationRepository;
        }

        public Response<Contact> UpdateContact(Contact _)
        {
            throw new NotImplementedException();
        }

        

        public IEnumerable<ContactModel> GetActiveContactList() => _mapper.Map<IList<ContactModel>>(_repository.GetActiveContacts());

        public IEnumerable<ContactModel> GetContactList() => _mapper.Map<IList<ContactModel>>(_repository.GetAllContacts());

        public Response<ContactModel> GetResponseActiveContactList() => new Response<ContactModel>(PublicResultStatusCodes.Done, GetActiveContactList().ToList());
        
        public Response<ContactModel> GetResponseContactList() => new Response<ContactModel>(PublicResultStatusCodes.Done, GetContactList().ToList());

        public Response<Contact> PushContactNotification(Contact _)
        {
            NotificationQueue notificationQueue = new NotificationQueue();
            notificationQueue.Message = _.Answer;
            notificationQueue.ToEmail = _.Email;
            notificationQueue.IDNotificationQueueType = 3;
            _notificationRepository.AddNotificationQueue(notificationQueue);
            _repository.UpdateContact(_);
            return new Response<Contact>(PublicResultStatusCodes.Done);
        }


    }
}
