﻿using AutoMapper;
using eKosovaInternal.SharedModel.Medias;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Medias;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
    public class MediaService : IMediaService
    {
        private readonly IMediaRepoService _repository;
        private readonly IMapper _mapper;
        public MediaService(IMediaRepoService repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<MediaModel> GetActiveMediaList() => _mapper.Map<IList<MediaModel>>(_repository.GetActiveMedias());
        public IEnumerable<MediaModel> GetMediaLsit() => _mapper.Map<IList<MediaModel>>(_repository.GetAllMedias());
        public Response<MediaModel> GetResponseActiveMediaList() => new Response<MediaModel>(PublicResultStatusCodes.Done, GetActiveMediaList().ToList());
        public Response<MediaModel> GetResponseMediaList() => new Response<MediaModel>(PublicResultStatusCodes.Done, GetMediaLsit().ToList());

    }
}
