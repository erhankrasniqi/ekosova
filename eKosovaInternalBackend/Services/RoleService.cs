﻿using AutoMapper;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Modules;
using eKosovaInternal.Domain.Roles;
using eKosovaInternal.SharedModel.Roles;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using eKosovaInternal.SharedModel.General;
using eKosovaInternal.Business.Helpers;

namespace eKosovaInternalBackend.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _repository;
        private readonly IInternalUserRepoService _userRepoService;// me bo userreposervice
        private readonly IModuleRepository _repositoryModule;
        private readonly IMapper _mapper;
        private readonly IGeneralUpdateService<Role> _roleAddUpdateService;
        private readonly IGeneralUpdateService<RoleAuthorization> _roleAuthAddUpdateService;
        private readonly IHttpContextAccessor _contextAccessor;
        public RoleService(IRoleRepository repository,
                           IMapper mapper,
                           IGeneralUpdateService<RoleAuthorization> roleAuthAddUpdateService,
                           IGeneralUpdateService<Role> roleAddUpdateService,
                           IModuleRepository repositoryModule,
                           IInternalUserRepoService userRepoService,
                           IHttpContextAccessor contextAccessor)
        {
            _repository = repository;
            _mapper = mapper;
            _roleAuthAddUpdateService = roleAuthAddUpdateService;
            _roleAddUpdateService = roleAddUpdateService;
            _repositoryModule = repositoryModule;
            _userRepoService = userRepoService;
            _contextAccessor = contextAccessor;
        }

        public IEnumerable<RoleModel> GetRoles()
        {
            var roles = _repository.GetRolesWithCriteria(_ => _.IsActive && !_.IsDeleted);
            return _mapper.Map<IList<RoleModel>>(roles);
        }
        public IEnumerable<RoleModel> GetRolesWithInactive()
        {
            var roles = _repository.GetRolesWithCriteria(_ => !_.IsDeleted);
            return _mapper.Map<IList<RoleModel>>(roles);
        }

        public Response<RoleModel> GetResponseRoles()
        {
            var roles = GetRoles().ToList();
            var users = _userRepoService.GetAll();
            var list = new List<RoleModel>();
            foreach (var item in roles)
            {
                list.Add(new RoleModel
                {
                    Id = item.Id,
                    Title = item.Title,
                    Description = item.Description,
                    IsActive = item.IsActive,
                    WithPasswordPolicy = item.WithPasswordPolicy,
                    PasswordValidityDays = item.PasswordValidityDays,
                    CanBeDeleted = !users.Any(_ => _.IDRole == item.Id)
                });
            }
            return new Response<RoleModel>(PublicResultStatusCodes.Done, list);
        }

        public Response<RoleModel> GetResponseRolesWithInactive()
        {
            var roles = GetRolesWithInactive().ToList();
            return new Response<RoleModel>(PublicResultStatusCodes.Done, roles);
        }
        public Response<RoleViewAccess> GetRoleAccessesByIdUser(int IdUser)
        {
            //var user = _userRepoService.GetById(IdUser);
            //List<RoleViewAccess> list = new List<RoleViewAccess>();
            //if (user.WithUserAuthorization == true)
            //{
            //    var result1 = _userRepoService.GetUserAuthorizations(IdUser);
            //    list = _mapper.Map<IList<RoleViewAccess>>(result1).ToList();
            //}
            //else
            //{
            //    var result2 = _repository.GetRoleAuthorizationByRole(user.IdRole);
            //    list = _mapper.Map<IList<RoleViewAccess>>(result2).ToList();
            //}
            //return new Response<RoleViewAccess>(PublicResultStatusCodes.Done, list);
            return null;
        }

        public RoleModel GetRole(int id)
        {
            var roles = _repository.GetRole(id);
            return _mapper.Map<RoleModel>(roles);
        }

        public Response<RoleModel> GetResponseRole(int id)
        {
            var role = GetRole(id);
            return new Response<RoleModel>(PublicResultStatusCodes.Done, role);
        }

        public IEnumerable<RoleModel> GetPasiveRoles()
        {
            var roles = _repository.GetRolesWithCriteria(_ => !_.IsActive && !_.IsDeleted);
            return _mapper.Map<IList<RoleModel>>(roles);
        }

        public IEnumerable<RoleModel> GetAllRoles()
        {
            var roles = _repository.GetRoles();
            return _mapper.Map<IList<RoleModel>>(roles);
        }

        public IEnumerable<DefinationBaseModel> GetRoleAuthorizationTypes() => _mapper.Map<IList<DefinationBaseModel>>(_repository.GetRoleAuthTypes());

        public Response<DefinationBaseModel> GetResponseRoleAuthTypes() => new Response<DefinationBaseModel>(PublicResultStatusCodes.Done, GetRoleAuthorizationTypes().ToList());

        public DefinationBaseModel GetRoleAuthTypeByRoleAndModule(int idRole, int idModule) => _mapper.Map<DefinationBaseModel>(_repository.GetRoleAuthTypeByRoleAndModule(idRole, idModule));
        public Response<DefinationBaseModel> GetResponseRoleAuthTypeByRoleAndModule(int idRole, int idModule) => new Response<DefinationBaseModel>(PublicResultStatusCodes.Done, GetRoleAuthTypeByRoleAndModule(idRole, idModule));

        //public Response<RoleAuthorizationModel> CreateRoleAuthorization(NewRoleAuthorizationModel _)
        //{
        //    var rAuth = _mapper.Map<RoleAuthorization>(_);
        //    EntryUpdateUserHelper.FillEntryData(ref rAuth);


        //    var serials = ConvertToBinaryHelper<RoleAuthorization>.SerializeAndConvert(null, rAuth);

        //    _roleAuthAddUpdateService.InsertAddLogDataChange(rAuth, serials.Item1, serials.Item2);

        //    return new Response<RoleAuthorizationModel>(PublicResultStatusCodes.Done, _mapper.Map<RoleAuthorizationModel>(rAuth));
        //}

        public Response<RoleAuthorizationModel> UpdateRoleAuthorization(int ID, EditRoleAuthorizationModel _)
        {
            var rAuth = _repository.GetRoleAuthorization(ID);
            rAuth.IDRoleAuthorizationType = _.IDRoleAuthorizationType;
            string query = "EXEC [dbo].[UpdateRoleAuthorization] @ID,@IDRoleAuthorizationType";
            object parameters = new
            {
                ID,
                rAuth.IDRoleAuthorizationType
            };
            _roleAuthAddUpdateService.UpdateAddLogDataChange(query,parameters);
            return new Response<RoleAuthorizationModel>(PublicResultStatusCodes.Done);
        }

        public Response<RoleModel> CreateRole(NewRoleModel _)
        {
            var role = _mapper.Map<Role>(_);
            string query = "EXEC [dbo].[InsertRole] @Title, @Description, @WithPasswordPolicy, @EntryUser, @IDEntryUser";
            role.IdEntryUser = /*int.Parse(_contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);*/5;
            role.EntryUser = /*_contextAccessor.HttpContext.User?.Identity?.Name;*/"Admin";
            object parameters = new { role.Title, role.Description, role.WithPasswordPolicy, role.EntryUser, role.IdEntryUser };
            var serials = ConvertToBinaryHelper<Role>.SerializeAndConvert(null, role);
            role.ID = _roleAddUpdateService.InsertAddLogDataChange(role, query, parameters, serials.Item1, serials.Item2).ID;

            var modules = _repositoryModule.GetModules();

            var list = new List<RoleAuthorization>();
            foreach (var item in modules)
            {
                list.Add(new RoleAuthorization()
                {
                    IDRole = role.ID,
                    IDRoleAuthorizationType = 3,
                    IDModule = item.ID,
                    EntryDate = DateTime.Now,
                    IDEntryUser = role.IdEntryUser,
                    EntryUser = role.EntryUser
                });
            }
            _repository.AddRangeRoleAuthorization(list);
            return new Response<RoleModel>(PublicResultStatusCodes.Done, _mapper.Map<RoleModel>(role));
        }

        public Response<RoleModel> UpdateRole(int id, EditRoleModel _)
        {
            var role = _repository.GetRole(id);
            _mapper.Map(_, role);
            role.IdUpdateUser = /*int.Parse(_contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);*/5;
            role.UpdateUser = /*_contextAccessor.HttpContext.User?.Identity?.Name;*/"Admin";
            string query = "EXEC [dbo].[UpdateRole] @ID, @Title, @Description, @UpdateUser, @IdUpdateUser";
            object parameters = new { role.ID, role.Title, role.Description, role.UpdateUser, role.IdUpdateUser};
            _roleAddUpdateService.UpdateAddLogDataChange(query, parameters);

            return new Response<RoleModel>(PublicResultStatusCodes.Done);
        }
    }
}

