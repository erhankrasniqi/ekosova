﻿using System.Threading.Tasks;
using eKosovaInternal.Domain;
using eKosovaInternal.Domain.ADDomains;
using eKosovaInternalBackend.Interfaces;
using System.DirectoryServices.AccountManagement;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.InternalUsers;
using System.Collections.Generic;
using AutoMapper;

namespace eKosovaInternalBackend.Services
{
	public class ADDomainService : IADDomainService
	{
		private readonly IADDomainRepoService _repoService;
		private readonly IMapper _mapper;

		public ADDomainService(IADDomainRepoService repoService, IMapper mapper)
		{ _repoService = repoService; _mapper = mapper; }

		public async Task<bool> CheckForAdUser(InternalUser user, Credentials credentials)
		{
			try
			{
				var domain = (await _repoService.GetADDomain(user.IDActiveDirectoryDomain)).Title;
				var pc = new PrincipalContext(ContextType.Domain, domain);
				return pc.ValidateCredentials(credentials.Account, credentials.Password);
			}
			catch
			{
				return false;
			}
		}

		public async Task<Response<ADDomainViewModel>> GetAll()
		{
			var result = (await _repoService.GetAllDomains());
			var mapped = _mapper.Map<IList<ADDomain>, IList<ADDomainViewModel>>(result);
			return new Response<ADDomainViewModel>(PublicResultStatusCodes.Done, mapped);
		}
	}
}
