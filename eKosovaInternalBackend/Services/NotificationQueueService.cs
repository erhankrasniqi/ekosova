﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Notifications;
using eKosovaInternal.SharedModel.Notifications;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
    public class NotificationQueueService : INotificationQueueService
    {
        private readonly INotificationRepoService _repository;

        private readonly IMapper _mapper;
        public NotificationQueueService(INotificationRepoService repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public Response<NotificationQueue> Create(CreateNotificationQueue _)
        {
            try
            {
                var notificationQueue = _mapper.Map<NotificationQueue>(_);
                _repository.AddNotificationQueue(notificationQueue);
                return new Response<NotificationQueue>(PublicResultStatusCodes.Done, _mapper.Map<NotificationQueue>(notificationQueue));
            }
            catch (Exception e)
            {
                return new Response<NotificationQueue>(PublicResultStatusCodes.QueryHasError);
            }

        }
    }
}
