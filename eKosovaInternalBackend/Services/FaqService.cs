﻿using AutoMapper;
using eKosovaInternal.SharedModel.Faqs;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Faqs;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eKosovaInternalBackend.Services
{
	public class FaqService : IFaqService
	{
		private readonly IFaqRepoService _repository;
		private readonly IMapper _mapper;

		public FaqService(IFaqRepoService repository, IMapper mapper)
		{
			_repository = repository;
			_mapper = mapper;
		}

		public Response<FaqModel> Create(CreateFaqModel _)
		{
			try
			{
				var faq1 = _mapper.Map<Faq>(_);
				faq1.Answer = faq1.Answer.Replace("&gt;", ">").Replace("&lt;", "<");
				_repository.AddFaq(faq1);
				return new Response<FaqModel>(PublicResultStatusCodes.Done, _mapper.Map<FaqModel>(faq1));
			}
			catch (Exception e)
			{
				return new Response<FaqModel>(PublicResultStatusCodes.QueryHasError);
			}
		}

		public Response<DeleteFaq> DeleteFaq(int id)
		{
			_repository.DeleteFaq(id);
			return new Response<DeleteFaq>(PublicResultStatusCodes.Done);
		}

		public IEnumerable<Faq> GetActiveFaqList() => _mapper.Map<IList<Faq>>(_repository.GetActiveFaqs().OrderByDescending(x => x.InsertionDate));


		public IEnumerable<Faq> GetFaqLsit() => _mapper.Map<IList<Faq>>(_repository.GetAllFaqs().OrderByDescending(x => x.InsertionDate));

		public Response<Faq> GetResponseActiveFaqList() => new Response<Faq>(PublicResultStatusCodes.Done, GetActiveFaqList().ToList());


		public Response<Faq> GetResponseFaqList() => new Response<Faq>(PublicResultStatusCodes.Done, GetActiveFaqList().ToList());

		public Response<UpdateFaqModel> UpdateFaq(UpdateFaqModel _)
		{
			_repository.UpdateFaq(_);
			return new Response<UpdateFaqModel>(PublicResultStatusCodes.Done);
		}


		//public Response<UpdatefaqModel> UpdateFaq(UpdatefaqModel _)
		//{

		//    Faq item = new Faq();
		//    item.Question = _.Question;
		//    item.Answer = _.Answer;
		//    item.IDInternalUser = 3;
		//    _repository.UpdateFaq(item);
		//    return new Response<UpdatefaqModel>(PublicResultStatusCodes.Done);
		//}
	}

}
