﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Edergesa;
using eKosovaInternal.Domain.Submission;
using eKosovaInternal.SharedModel.Edergesa;
using eKosovaInternal.SharedModel.Submission;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
    public class SubmissionPublicUserService : ISubmissionPublicUserService
    {
        private readonly ISubmissionPublicUserRepoService _repository;
        private readonly IMapper _mapper;

        public SubmissionPublicUserService(ISubmissionPublicUserRepoService repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public Response<InsertSubmission> Create(InsertSubmissionModel _)
        {
            try
            {
                var inserSubmmission = _mapper.Map<InsertSubmission>(_); 
                _repository.InserSubmission(inserSubmmission);
                return new Response<InsertSubmission>(PublicResultStatusCodes.Done, _mapper.Map<InsertSubmission>(inserSubmmission));
            }
            catch (Exception e)
            {
                return new Response<InsertSubmission>(PublicResultStatusCodes.QueryHasError);
            }
        }

        public Response<InsertSubmissionDocument> Create(InsertSubmissionDocumentModel _)
        {
            try
            {
                var inserSubmmissionDocument = _mapper.Map<InsertSubmissionDocument>(_);
                _repository.InserSubmissionDocument(inserSubmmissionDocument);
                return new Response<InsertSubmissionDocument>(PublicResultStatusCodes.Done, _mapper.Map<InsertSubmissionDocument>(inserSubmmissionDocument));
            }
            catch (Exception e)
            {
                return new Response<InsertSubmissionDocument>(PublicResultStatusCodes.QueryHasError);
            }
        }

        public IEnumerable<HierarchysubmissionTypeModel> GetHieararchySubmission() => _mapper.Map<IEnumerable<HierarchysubmissionTypeModel>>(_repository.GetHierarchyList());
        public Response<SubmissionPersonalUserModel> GetSubmissionPublicUserList(int personalNumber)
        {
            return new Response<SubmissionPersonalUserModel>(PublicResultStatusCodes.Done, _mapper.Map<IList<SubmissionPersonalUserModel>>(_repository.GetPublicUserByPersonalNumber(personalNumber)));
        }
    }
}
