﻿using eKosovaInternal.Domain.Logs;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
    public class GeneralUpdateService<T> : IGeneralUpdateService<T> where T : BaseEntity
    {
        private readonly ILogService _logService;
        private readonly IRepository<T> _generalRepository;
        public GeneralUpdateService(ILogService logService, IRepository<T> generalRepository)
        {
            _logService = logService;
            _generalRepository = generalRepository;
        }
        public void UpdateAddLogDataChange(string query, object param)
        {
            try
            {
                _generalRepository.Update(query, param);
            }
            catch (Exception ex)
            {
                throw;
            }
            
        }

        public void DeleteAddLogDataChange(T entity)
        {
            //_generalRepository.Delete(entity);
        }

        public T InsertAddLogDataChange(T entity,string query,object param, byte[] before, byte[] after = null)
        {
            try
            {
                var model = _generalRepository.Add(query, param);
                _logService.AddLogDataChange(before, after, DataChangeStatus.Insert, model.GetType().Name);
                return model;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
