﻿using AutoMapper;
using eKosovaInternal.SharedModel.Helps;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Helps;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Domain.Notifications;

namespace eKosovaInternalBackend.Services
{
    public class HelpService : IHelpService
    {
        private readonly IHelpRepoService _repository;
        private readonly IMapper _mapper;
        private readonly INotificationRepoService _notificationRepository;

        public HelpService(IHelpRepoService repository, IMapper mapper, INotificationRepoService notificationRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _notificationRepository = notificationRepository;
        }

        public IEnumerable<HelpModel> GetActiveHelpList() => _mapper.Map<IList<HelpModel>>(_repository.GetActiveHelps());

        public IEnumerable<HelpModel> GetHelpList() => _mapper.Map<IList<HelpModel>>(_repository.GetAllHelps());

        public Response<HelpModel> GetResponseActiveHelpList() => new Response<HelpModel>(PublicResultStatusCodes.Done, GetActiveHelpList().ToList());

        public Response<HelpModel> GetResponseHelpList() => new Response<HelpModel>(PublicResultStatusCodes.Done, GetHelpList().ToList());

        public Response<Help> PushHelpNotification(Help _)
        {
            NotificationQueue notificationQueue = new NotificationQueue();
            notificationQueue.Message = _.Answer;
            notificationQueue.ToEmail = _.Email;
            notificationQueue.IDNotificationQueueType = 3;
            _notificationRepository.AddNotificationQueue(notificationQueue);
            _repository.UpdateHelp(_);
            return new Response<Help>(PublicResultStatusCodes.Done);
        }
    }
}
