﻿using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Business;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using System.Collections.Generic;
using eKosovaInternal.Domain.ADDomains;

namespace eKosovaInternalBackend.Services
{
	public class InternalUserService : IInternalUserService
	{
		private readonly IInternalUserRepoService _repository;

		private readonly IMapper _mapper;

		public InternalUserService(IInternalUserRepoService repository, IMapper imapper)
		{
			_mapper = imapper;
			_repository = repository;
		}

		public Response<InternalUserViewModel> GetAll()
			=> new Response<InternalUserViewModel>(PublicResultStatusCodes.Done, _mapper.Map<IList<InternalUserViewModel>>(_repository.GetAll()));

		public async Task<Response<bool>> ClearTokens(string identification)
		{
			try
			{
				var user = await _repository.GetByIdentification(identification);
				user.AccessToken = null;
				user.RefreshToken = null;
				var result = Update(user);
				if (result.Status == 0)
					return new Response<bool>(PublicResultStatusCodes.Done, true);
				else
					throw new Exception(result.Data.First());
			}
			catch (Exception e)
			{
				return new Response<bool>(PublicResultStatusCodes.EditOperationWasNotPerformed);
			}
		}

		public async Task<Response<dynamic>> Create(InternalUser _)
		{
			try
			{
				var check = await SelectAccount(_.Account);
				if (check.Data.First() != null) { throw new Exception("EXISTS"); }
				var result = await _repository.Add(_);
				return new Response<dynamic>(PublicResultStatusCodes.Done, result);
			}
			catch (Exception e)
			{
				return new Response<dynamic>(PublicResultStatusCodes.QueryHasError);
			}
		}

		public async Task<Response<dynamic>> Select(int ID)
		{
			try
			{
				var user = await _repository.GetById(ID);
				return new Response<dynamic>(PublicResultStatusCodes.Done, user);
			}
			catch (Exception e)
			{
				return new Response<dynamic>(PublicResultStatusCodes.QueryHasError, e.Message);
			}
		}

		public async Task<Response<InternalUser>> SelectAccount(string account)
		{
			try
			{
				var user = await _repository.GetByAccount(account);
				return new Response<InternalUser>(PublicResultStatusCodes.Done, user);
			}
			catch
			{
				return new Response<InternalUser>(PublicResultStatusCodes.QueryHasError);
			}
		}

		public async Task<Response<InternalUserViewModel>> SelectAccountView(string account)
		{
			try
			{
				var user = await _repository.GetByAccount(account);
				var mapped = _mapper.Map<InternalUser, InternalUserViewModel>(user);
				return new Response<InternalUserViewModel>(PublicResultStatusCodes.Done, mapped);
			}
			catch
			{
				return new Response<InternalUserViewModel>(PublicResultStatusCodes.QueryHasError);
			}
		}

		public async Task<Response<dynamic>> SelectIdentification(string identification)
		{
			try
			{
				var user = await _repository.GetByIdentification(identification);
				return new Response<dynamic>(PublicResultStatusCodes.Done, user);
			}
			catch (Exception e)
			{
				return new Response<dynamic>(PublicResultStatusCodes.QueryHasError, e.Message);
			}
		}

		public async Task<Response<dynamic>> SelectPersonalNumber(string personalNumber)
		{
			try
			{
				var user = await _repository.GetByPersonalNumber(personalNumber);
				return new Response<dynamic>(PublicResultStatusCodes.Done, user);
			}
			catch (Exception e)
			{
				return new Response<dynamic>(PublicResultStatusCodes.QueryHasError, e.Message);
			}
		}

		public Response<dynamic> Update(InternalUser _)
		{
			try
			{
				_repository.Update(_);
				return new Response<dynamic>(PublicResultStatusCodes.Done);
			}

			catch (Exception e)
			{
				return new Response<dynamic>(PublicResultStatusCodes.EditOperationWasNotPerformed, e.Message);
			}
		}

		public async Task<Response<bool>> UpdateByAdmin(InternalUserViewModel _)
		{
			try
			{
				var internalUser = await _repository.GetById(_.ID);
				internalUser.First = _.First;
				internalUser.Last = _.Last;
				internalUser.PhoneNumber = _.PhoneNumber;
				internalUser.Account = _.Account;
				internalUser.IsActive = _.IsActive;
				internalUser.IDActiveDirectoryDomain = _.IDActiveDirectoryDomain;
				internalUser.IDHierarchy = _.IDHierarchy;
				internalUser.IDRole = _.IDRole;
				internalUser.IDUserAuthorizationType = _.IDUserAuthorizationType;
				_repository.Update(internalUser);
				return new Response<bool>(PublicResultStatusCodes.Done, true);
			}
			catch
			{
				return new Response<bool>(PublicResultStatusCodes.InternalServerError, false);
			}
		}
	}
}
