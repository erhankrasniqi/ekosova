﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.SharedModel.PublicUsers;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
    public class PublicUsersService : IPublicUserService
    {
        private readonly IPublicUsersRepoService _repository;
        private readonly IMapper _mapper;

        public PublicUsersService(IPublicUsersRepoService repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<PublicUser> GetActivePublicUserList() => _mapper.Map<IList<PublicUser>>(_repository.GetActivePublicUser());


        public IEnumerable<PublicUser> GetPublicUserList() => _mapper.Map<IList<PublicUser>>(_repository.GetAllPublicUser());


        public Response<PublicUser> GetResponseActivePublicUserList() => new Response<PublicUser>(PublicResultStatusCodes.Done, GetActivePublicUserList().ToList());

        public Response<PublicUser> GetResponsePublicUserList() => new Response<PublicUser>(PublicResultStatusCodes.Done, GetPublicUserList().ToList());

        public async Task<Response<PublicUser>> GetResponsePublicUserPersonalNumber(string identification)
        {
            try
            {
                var result =  await _repository.GetPublicUserByPersonalNumber(identification);
                return new Response<PublicUser>(PublicResultStatusCodes.Done, result);
            }
            catch (Exception e) {
                return new Response<PublicUser>(PublicResultStatusCodes.QueryHasError);
            }
        }

        public Response<UpdatePublicUserModel> UpdatePublicUser(UpdatePublicUserModel _) {
            var mappedData = _mapper.Map<UpdatePublicUser>(_);
            _repository.UpdatePublicUser(mappedData);
            return new Response<UpdatePublicUserModel>(PublicResultStatusCodes.Done); }

    }
}
