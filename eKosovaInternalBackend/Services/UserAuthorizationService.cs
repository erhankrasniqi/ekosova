﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Domain.UserAuthorizations;
using eKosovaInternalBackend.Interfaces;

namespace eKosovaInternalBackend.Services
{
	public class UserAuthorizationService : IUserAuthorizationService
	{
		private readonly IUserAuthorizationRepoService _repoService;

		private readonly IMapper _mapper;

		public UserAuthorizationService(IUserAuthorizationRepoService repoService, IMapper mapper)
		{
			_repoService = repoService;
			_mapper = mapper;
		}

		public Response<UserAuthorizationType> GetAll()
		{
			try
			{
				var hierarchy = _repoService.GetAll();
				return new Response<UserAuthorizationType>(PublicResultStatusCodes.Done, hierarchy);
			}
			catch
			{
				return null;
			}
		}
	}
}
