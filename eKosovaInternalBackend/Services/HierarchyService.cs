﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Hierarchies;
using eKosovaInternalBackend.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
	public class HierarchyService : IHierarchyService
	{
		private readonly IHierarchyRepoService _repoService;

		private readonly IMapper _mapper;

		public HierarchyService(IHierarchyRepoService repoService, IMapper mapper)
		{
			_repoService = repoService;
			_mapper = mapper;
		}

		public Response<HierarchyViewModel> GetAll()
		{
			try
			{
				var hierarchy = _repoService.GetAll();
				var mapped = _mapper.Map<IList<Hierarchy>, IList<HierarchyViewModel>>(hierarchy);
				return new Response<HierarchyViewModel>(PublicResultStatusCodes.Done, mapped);
			}
			catch
			{
				return null;
			}
		}
	}
}
