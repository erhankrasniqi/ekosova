﻿using AutoMapper;
using eKosova.Domain.DynamicForms;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.DynamicForms;
using eKosovaInternal.Domain.Services;
using eKosovaInternal.SharedModel.DynamicForms;
using eKosovaInternalBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
    public class ServiceCategoryService : IServiceCategoryService
    {

        private readonly IServicesRepository _serviceRepository;
        private readonly IMapper _mapper;

        public ServiceCategoryService(IServicesRepository serviceRepository, IMapper mapper)
        {
            _serviceRepository = serviceRepository;
            _mapper = mapper;
        }

        public Response<ServiceCategory> GetServiceCategories()
        {
            try
            {
                var list = _serviceRepository.GetServiceCategories().ToList();
                return new Response<ServiceCategory>(PublicResultStatusCodes.Done, list);
            }
            catch (Exception e)
            {
                return new Response<ServiceCategory> (PublicResultStatusCodes.QueryHasError);
            }
        }
    }
}
