﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Modules;
using eKosovaInternal.Domain.Roles;
using eKosovaInternal.SharedModel.General;
using eKosovaInternal.SharedModel.Modules;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
    public class ModuleService : IModuleService
    {
        private readonly IModuleRepository _repository;
        private readonly IRoleRepository _repositoryRole;
        private readonly IGeneralUpdateService<Module> _addUpdateService;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IMapper _mapper;

        public ModuleService(IModuleRepository repository,
                             IGeneralUpdateService<Module> addUpdateService,
                             IHttpContextAccessor contextAccessor,
                             IRoleRepository repositoryRole,
                             IMapper mapper)
        {
            _repository = repository;
            _addUpdateService = addUpdateService;
            _contextAccessor = contextAccessor;
            _repositoryRole = repositoryRole;
            _mapper = mapper;
        }

        public IEnumerable<DefinationBaseModel> GetModules() => _mapper.Map<IList<DefinationBaseModel>>(_repository.GetModules());

        public Response<DefinationBaseModel> GetResponseModule() => new Response<DefinationBaseModel>(PublicResultStatusCodes.Done, GetModules().ToList());
        public Response<ModuleModel> GetResponseModuleTree() => new Response<ModuleModel>(PublicResultStatusCodes.Done, _mapper.Map<IList<ModuleModel>>(_repository.GetModules()).ToList());
        public Response<ModuleModel> GetResponseModule(int IDRole)
        {
            var modules = _repository.GetModules();
            var authorization = _repositoryRole.GetRoleAuthorizationByRole(IDRole);
            //var data = _mapper.Map<IList<ModuleModel>>(modules).ToList();
            var list = new List<ModuleModel>();
            foreach (var item in modules)
            {
                list.Add(new ModuleModel()
                {
                    ID = item.ID,
                    Title = item.Title,
                    IDRoleAuthorizationType = authorization.Where(_ => _.IDModule == item.ID).FirstOrDefault().IDRoleAuthorizationType,
                    IDRoleAuthorization = authorization.Where(_ => _.IDModule == item.ID).FirstOrDefault().ID,
                    IDRole = IDRole,
                    IDParent = item.IDParent
                });
            }
            return new Response<ModuleModel>(PublicResultStatusCodes.Done, list);
        }
        public Response<ModuleModel> GetResponseModuleForUser(int IDUser)
        {
            var modules = _repository.GetModules();
            var authorization = _repositoryRole.GetRoleAuthorizationByUser(IDUser);
            //var data = _mapper.Map<IList<ModuleModel>>(modules).ToList();
            var list = new List<ModuleModel>();
            foreach (var item in modules)
            {
                list.Add(new ModuleModel()
                {
                    ID = item.ID,
                    Title = item.Title,
                    IDRoleAuthorizationType = authorization.Where(_ => _.IDModule == item.ID).FirstOrDefault().IdRoleAuthorizationType,
                    IDRoleAuthorization = authorization.Where(_ => _.IDModule == item.ID).FirstOrDefault().ID,
                    IDUser = IDUser,
                    IDParent = item.IDParent
                });
            }
            return new Response<ModuleModel>(PublicResultStatusCodes.Done, list);
        }


        public Response<DefinationBaseModel> AddModule(NewModuleModel _)
        {
            var module = _mapper.Map<Module>(_);

            return new Response<DefinationBaseModel>(PublicResultStatusCodes.Done, _mapper.Map<DefinationBaseModel>(module));
        }

        public Response<LookupBaseModel> GetReportModules()
        {
            var modules = _repository.GetModules(c => c.IDParent == (int)ModuleView.FinancialReports);
            var result = new List<LookupBaseModel>();

            foreach (var item in modules)
            {
                result.Add(new LookupBaseModel { ID = item.ID, Title = item.Title });
            }
            return new Response<LookupBaseModel>(PublicResultStatusCodes.Done, result);
        }

    }
}
