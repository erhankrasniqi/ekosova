﻿using AutoMapper;
using eKosovaInternal.Business;
using eKosovaInternal.Business.Helpers;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Domain.Logs;
using eKosovaInternal.SharedModel.General;
using eKosovaInternal.SharedModel.Logs;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Services
{
	public class LogService : ILogService
	{

		private readonly IInternalUserRepoService _userRepoService;
		private readonly ILogRepository _logRepository;
		private readonly IHttpContextAccessor _contextAccessor;
		private readonly IMapper _mapper;
		//private readonly IGeneralService _generalService;

		public LogService(IInternalUserRepoService userRepoService,
						  ILogRepository logRepository,
						  IHttpContextAccessor contextAccessor,
						  IMapper mapper
						  //IGeneralService generalService
						  )
		{
			_userRepoService = userRepoService;
			_logRepository = logRepository;
			_contextAccessor = contextAccessor;
			_mapper = mapper;
			NetworkHelper.SetHttpContextAccessor(_contextAccessor);
			//_generalService = generalService;
		}

		public async Task AddLogUserAuthorization(int authorizationStatus)
		{
			var entryUser = await _userRepoService.GetByAccount(_contextAccessor.HttpContext.User.Identity.Name);

			var logUserAuth = new LogUserAuthorization
			{
				IDUser = entryUser.ID,
				ComputerName = NetworkHelper.GetComputerName(),
				IPAddress = NetworkHelper.GetIPAddress(),
				IDLogBrowserType = NetworkHelper.GetBrowserTypeId(),
				IDLogOperatingSystemType = NetworkHelper.GetOperatingSystemTypeId(),
				IsMobileDevice = NetworkHelper.IsMobileDevice(),
				IDLogUserAuthorizationStatus = authorizationStatus,
				EntryUser = entryUser.Account
			};

			_logRepository.AddLogUserAuthorization(logUserAuth);
		}


		public async Task<Response<LogUserAuthorization>> SetLogUserAuthorization(int authorizationStatus)
		{
			await AddLogUserAuthorization(authorizationStatus);
			return new Response<LogUserAuthorization>(PublicResultStatusCodes.Done);
		}

		public IEnumerable<LogWithIdUser> GetLogUserAuthorization(int idUser) => _mapper.Map<IList<LogWithIdUser>>(_logRepository.GetLogUserAuthorizationsByIdUser(idUser));
		public IEnumerable<LogWithIdUser> GetLogUserAuthorizations() => _mapper.Map<IList<LogWithIdUser>>(_logRepository.GetLogUserAuthorizations());

		public Response<LogWithIdUser> GetResponseLogUserAuthorization(int idUser) => new Response<LogWithIdUser>(PublicResultStatusCodes.Done, GetLogUserAuthorization(idUser).ToList());
		public Response<LogWithIdUser> GetResponseLogUserAuthorizations() => new Response<LogWithIdUser>(PublicResultStatusCodes.Done, GetLogUserAuthorizations().ToList());

		public Response<LogUserAuthorizationStatusModel> GetLogUserAuthorizationStatus()
		{
			var data = _logRepository.GetLogUserAuthorizationStatus();
			return new Response<LogUserAuthorizationStatusModel>(PublicResultStatusCodes.Done, _mapper.Map<IList<LogUserAuthorizationStatusModel>>(data).ToList());
		}

		//LogUserActivity
		public void AddLogUserActivity(int idModule, int logUserActivityStatus, string Host, bool isPublic)
		{
			var entryUser = _userRepoService.GetByAccount(_contextAccessor.HttpContext.User.Identity.Name).Result;
			var logDataChange = new LogUserActivity
			{
				EntryDate = DateTime.Now,
				IDUser = entryUser.ID,
				EntryUser = entryUser.Account,
				ComputerName = NetworkHelper.GetComputerName(),
				IPAddress = NetworkHelper.GetIPAddress(),
				IDLogBrowserType = NetworkHelper.GetBrowserTypeId(),
				IDLogOperatingSystemType = NetworkHelper.GetOperatingSystemTypeId(),
				IDModule = idModule,
				IsPublic = isPublic,
				URL = Host,
				IsMobileDevice = NetworkHelper.IsMobileDevice(),
				IDLogUserActivityStatus = logUserActivityStatus
			};
			_logRepository.AddLogUserActivity(logDataChange);
		}

		public Response<LogUserActivity> SetLogUserActivity(LogActivityModel _)
		{
			AddLogUserActivity(_.IDModule, _.ActivityStatus, _.Host, _.IsPublic);
			return new Response<LogUserActivity>(PublicResultStatusCodes.Done);
		}

		public IEnumerable<LogWithIdEntryUser> GetLogActivities(int idUser) => _mapper.Map<IList<LogWithIdEntryUser>>(_logRepository.GetLogUserActivitiesByIdUser(idUser));
		public IEnumerable<LogWithIdEntryUser> GetLogActivities() => _mapper.Map<IList<LogWithIdEntryUser>>(_logRepository.GetLogUserActivities());

		public Response<LogWithIdEntryUser> GetResponseLogActivities(int idUser) => new Response<LogWithIdEntryUser>(PublicResultStatusCodes.Done, GetLogActivities(idUser).ToList());
		public Response<LogUserActivityStatusModel> GetLogUserActivityStatus()
		{
			var data = _logRepository.GetLogUserActivityStatus();
			return new Response<LogUserActivityStatusModel>(PublicResultStatusCodes.Done, _mapper.Map<IList<LogUserActivityStatusModel>>(data).ToList());
		}

		//public Response<Pagination<LogUserActivityModel>> GetLogUserActivityWithPaging(LogUserActivitySearchModel parameters)
		//{
		//	var result = new Pagination<LogUserActivityModel>
		//	{
		//		PageSize = parameters.PageSize,
		//		Page = parameters.Page,
		//		Result = new List<LogUserActivityModel>()
		//	};

		//	var source = _generalService.GetLogUserActivitySource(parameters);

		//	var pagedList = PagedList<LogUserActivity>
		//		.ToPagedList(source, parameters.Page, parameters.PageSize, "ID", "DESC");

		//	result.Count = pagedList.TotalCount;

		//	result.Result = _mapper.Map<IList<LogUserActivityModel>>(pagedList);
		//	return new Response<Pagination<LogUserActivityModel>>(PublicResultStatusCodes.Done, result);
		//}


		public void AddLogFailedAuthentication(string username)
		{
			var logFailedAuth = new LogFailedAuthentication
			{
				Account = username,
				ComputerName = NetworkHelper.GetComputerName(),
				IPAddress = NetworkHelper.GetIPAddress(),
				IDLogBrowserType = NetworkHelper.GetBrowserTypeId(),
				IDLogOperatingSystemType = NetworkHelper.GetOperatingSystemTypeId(),
				IsMobileDevice = NetworkHelper.IsMobileDevice(),
				EntryDate = DateTime.Now
			};

			_logRepository.AddLogFailedAuthentication(logFailedAuth);
		}
		public IEnumerable<LogWithAccount> GetLogFailedAuthentications(DateTime? entryDate) => _mapper.Map<IList<LogWithAccount>>(_logRepository.GetLogFailedAuthentications(entryDate));

		public Response<LogWithAccount> GetResponseLogFailedAuthentications(DateTime? entryDate) => new Response<LogWithAccount>(PublicResultStatusCodes.Done, GetLogFailedAuthentications(entryDate).ToList());

		public IEnumerable<LogWithAccount> GetLogFailedAuthenticationsAll() => _mapper.Map<IList<LogWithAccount>>(_logRepository.GetLogFailedAuthenticationsAll());


		public Response<LogWithAccount> GetResponseLogFailedAuthenticationsAll() => new Response<LogWithAccount>(PublicResultStatusCodes.Done, GetLogFailedAuthenticationsAll().ToList());

		//LogDataChange
		public void AddLogDataChange(byte[] before, byte[] after, DataChangeStatus action, string TableName)
		{
			var IdTable = _logRepository.GetTableByName(TableName);
			var entryUser = _userRepoService.GetByAccount(_contextAccessor.HttpContext.User?.Identity?.Name).Result;
			var logDataChange = new LogDataChange
			{
				EntryDate = DateTime.Now,
				IDEntryUser = entryUser != null ? entryUser.ID : 999999,
				EntryUser = entryUser != null ? entryUser.Account : "public",
				Before = before,
				After = after,
				IDTable = IdTable,
				ComputerName = NetworkHelper.GetComputerName(),
				IPAddress = NetworkHelper.GetIPAddress(),
				IDLogBrowserType = NetworkHelper.GetBrowserTypeId(),
				IDLogOperatingSystemType = NetworkHelper.GetOperatingSystemTypeId(),
				IsMobileDevice = NetworkHelper.IsMobileDevice(),
				IDLogDataChangeStatus = (int)action
			};
			_logRepository.AddLogDataChange(logDataChange);
		}

		//public Response<TableModel> GetTables()
		//{
		//	return new Response<TableModel>(PublicResultStatusCodes.Done, _mapper.Map<IList<TableModel>>(_logRepository.GetTables()).ToList());
		//}

	}
}
