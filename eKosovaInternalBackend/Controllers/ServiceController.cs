﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternalBackend.Interfaces;
using eKosovaInternal.Business;
using eKosovaInternal.SharedModel.DynamicForms;
using eKosovaInternal.Domain.Services;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly IServiceCategoryService _serviceCategoryService;

        public ServiceController(IServiceCategoryService serviceCategoryService)
        {
            _serviceCategoryService = serviceCategoryService;
        }

        [HttpGet("getServiceCategories")]
        [ProducesResponseType(typeof(Response<ServiceCategory>), 200)]
        public IActionResult GetServiceCategories() => Ok(_serviceCategoryService.GetServiceCategories());

    }
}
