﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosova.Domain.DynamicForms;
using eKosovaInternal.Business;
using eKosovaInternal.SharedModel.DynamicForms;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DynamicFormController : ControllerBase
    {

        private readonly IDynamicFormsService _dynamicFormsService;

        public DynamicFormController(IDynamicFormsService dynamicFormsService)
        {
            _dynamicFormsService = dynamicFormsService;
        }

        [HttpPost("createDynamicForm")]
        [ProducesResponseType(typeof(Response<bool>), 200)]
        public IActionResult Create([FromBody] CreateDynamicForms model) => Ok(_dynamicFormsService.CreateDynamicForm(model));

        [HttpGet("getDynamicForms")]
        [ProducesResponseType(typeof(Response<DynamicForm>), 200)]
        public IActionResult GetDynamicForms() => Ok(_dynamicFormsService.GetDynamicForms());
    }
}
