﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Contacts;
using eKosovaInternal.Domain.Emails;
using eKosovaInternal.SharedModel.Contacts;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly IContactService _contactService;
        public ContactController(IContactService contactService) {
            _contactService = contactService;
        }

        [HttpGet("getContacts")]
        [SwaggerOperation(Summary = "Get Contacts", Description = "This is endpoint where you get the contacts list.")]
        [ProducesResponseType(typeof(Response<List<ContactModel>>), 200)]
        public IActionResult GetContacts() => Ok(_contactService.GetResponseContactList());

        [HttpGet("getActiveContacts")]
        [SwaggerOperation(Summary = "Get Active Contacts", Description = "This is endpoint where you get the active contacts list.")]
        [ProducesResponseType(typeof(Response<List<ContactModel>>), 200)]
        public IActionResult GetActiveContacts() => Ok(_contactService.GetResponseActiveContactList());

        [HttpPost("edit")]
        [ProducesResponseType(typeof(Response<object>), 200)]
        public IActionResult Update([FromBody] Contact _) => Ok(_contactService.PushContactNotification(_));



    }
}
