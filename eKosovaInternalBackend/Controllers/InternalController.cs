﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using eKosovaInternal.Business.Helpers;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternalBackend.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class InternalController : ControllerBase
	{
		private readonly IInternalUserService _intService;

		private readonly IADDomainService _domainService;

		private readonly IHierarchyService _hierarchyService;

		private readonly IUserAuthorizationService _authorizationTypesService;

		private readonly IMapper _mapper;

		private readonly ILogService _logService;

		private readonly IHttpContextAccessor _contextAccessor;

		public InternalController(IInternalUserService internalUserService, IADDomainService domainService, IHierarchyService hierarchyService,
			IUserAuthorizationService authorizationTypesService, IMapper mapper, ILogService logService, IHttpContextAccessor contextAccessor)
		{
			_intService = internalUserService;
			_domainService = domainService;
			_hierarchyService = hierarchyService;
			_authorizationTypesService = authorizationTypesService;
			_mapper = mapper;
			_logService = logService;
			_contextAccessor = contextAccessor;
			NetworkHelper.SetHttpContextAccessor(contextAccessor);
		}

		[HttpPost("create")]
		public async Task<IActionResult> CreateInternalUser([FromBody] InternalUserCreate user)
		{
			var mapped = _mapper.Map<InternalUserCreate, InternalUser>(user);
			if (!string.IsNullOrEmpty(user.Password))
			{
				var hashsalt = new HashSalt(user.Password);
				mapped.PasswordHash = Convert.ToBase64String(hashsalt.Hash);
				mapped.PasswordSalt = Convert.ToBase64String(hashsalt.Salt);
			}
			var result = await _intService.Create(mapped);
			return Ok(result);
		}

		[HttpPost("check")]
		public async Task<IActionResult> CheckUser([FromBody] BaseString baseString) => Ok(await _intService.SelectAccountView(baseString.Value));

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
		[HttpPost("clearTokens")]
		public async Task<IActionResult> ClearTokens([FromBody] BaseIdentification identification)
		{
			await _logService.AddLogUserAuthorization((int)UserAuthorizationStatus.SignOut);
			return Ok(await _intService.ClearTokens(identification.Identification));
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
		[HttpGet("testAuthJWT")]
		public ActionResult Hajt()
		=> Ok(_contextAccessor.HttpContext.User.Claims.FirstOrDefault(_ => _.Type == "FullName"));

		[HttpGet("getAll")]
		public IActionResult GetAll() => Ok(_intService.GetAll());

		[HttpGet("getDomains")]
		public async Task<IActionResult> GetDomains() => Ok(await _domainService.GetAll());

		[HttpGet("getHierarchy")]
		public IActionResult GetHierarchy() => Ok(_hierarchyService.GetAll());

		[HttpGet("getAuthorizationTypes")]
		public IActionResult GetAuthorizationTypes() => Ok(_authorizationTypesService.GetAll());

		[HttpPost("updateAdmin")]
		public async Task<IActionResult> UpdateByAdmin([FromBody] InternalUserViewModel _) => Ok(await _intService.UpdateByAdmin(_));
	}
}
