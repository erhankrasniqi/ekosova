﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Edergesa;
using eKosovaInternal.Domain.Submission;
using eKosovaInternal.SharedModel.Submission;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubmissionController : Controller
    {
        private readonly ISubmissionPublicUserService _submissionService;

        public SubmissionController(ISubmissionPublicUserService submissionService)
        {
            _submissionService = submissionService;
        }

        [HttpGet("getSubmissionUser/{personalNumber}")]
        [ProducesResponseType(typeof(Response<List<SubmissionPublicUser>>), 200)]
        public IActionResult GetSubmissionUser(int personalNumber) => Ok(_submissionService.GetSubmissionPublicUserList(personalNumber));

        [HttpGet("getHieararchy")]
        [ProducesResponseType(typeof(Response<List<SubmissionPublicUser>>), 200)]
        public IActionResult GetHieararchy() => Ok(_submissionService.GetHieararchySubmission());

        [HttpPost("createSubmission")]
        [ProducesResponseType(typeof(Response<InsertSubmission>), 200)]
        public IActionResult CreateSubmission([FromBody] InsertSubmissionModel _) => Ok(_submissionService.Create(_));

        [HttpPost("createSubmissionDocument")]
        [ProducesResponseType(typeof(Response<InsertSubmissionDocument>), 200)]
        public IActionResult CreateSubmissionDocument([FromBody] InsertSubmissionDocumentModel _) => Ok(_submissionService.Create(_));
    }
}
