﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Medias;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MediaController : ControllerBase
    {
        private readonly IMediaService _mediaService;

        public MediaController(IMediaService mediaService)
        {
            _mediaService = mediaService;
        }

        [HttpGet("getMedias")]
        [ProducesResponseType(typeof(Response<List<Media>>), 200)]
        public IActionResult GetMedias() => Ok(_mediaService.GetResponseMediaList());

        [HttpGet("getActiveMedias")]
        [ProducesResponseType(typeof(Response<List<Media>>), 200)]
        public IActionResult getActiveMedias() => Ok(_mediaService.GetResponseMediaList());
    }
}
