﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.PublicUserHistory;
using eKosovaInternal.SharedModel.PublicUserHistory;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PublicUserHistoryController : ControllerBase
    {
        private readonly IPublicUserHistoryService _userHistoryService;

        public PublicUserHistoryController(IPublicUserHistoryService userHistoryService)
        {
            _userHistoryService = userHistoryService;
        }

        [HttpGet("getPublicUsersHistory/{publicUserId}")]
        [ProducesResponseType(typeof(Response<IList<PublicUserHistoryModel>>), 200)]
        public IActionResult GetPublicUsers(int publicUserId) => Ok(_userHistoryService.GetPublicUserHistory(publicUserId));
         

     
    }
}
