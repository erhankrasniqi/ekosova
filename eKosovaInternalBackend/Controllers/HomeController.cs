﻿using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternalBackend.Controllers
{
	public class HomeController : Controller
	{
		[HttpGet]
		public ActionResult Index() => Content("CanYouSeeMe?");
	}
}
