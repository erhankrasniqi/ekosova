﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Helps;
using eKosovaInternal.SharedModel.Helps;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelpController : ControllerBase
    {
        private readonly IHelpService _helpService;
        public HelpController(IHelpService helpService)
        {
            _helpService = helpService;
        }

        [HttpGet("getHelps")]
        [SwaggerOperation(Summary = "Get Helps", Description = "This is endpoint where you get helps list.")]
        [ProducesResponseType(typeof(Response<List<HelpModel>>), 200)]
        public IActionResult GetContacts() => Ok(_helpService.GetResponseHelpList());

        [HttpGet("getActiveHelps")]
        [SwaggerOperation(Summary = "Get Active Helps", Description = "This is endpoint where you get active helps list.")]
        [ProducesResponseType(typeof(Response<List<HelpModel>>), 200)]
        public IActionResult GetActiveContacts() => Ok(_helpService.GetResponseActiveHelpList());

        [HttpPost("edit")]
        [ProducesResponseType(typeof(Response<object>), 200)]
        public IActionResult Update([FromBody] Help _) => Ok(_helpService.PushHelpNotification(_));
    }
}
