﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Business;
using eKosovaInternal.SharedModel.Roles;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;
        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [HttpGet("getRoles")]
        [SwaggerOperation(Summary = "Roles", Description = "This endpoint is used to get Roles " +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<RoleModel>), 200)]
        public IActionResult GetRoles() => Ok(_roleService.GetResponseRoles());

        [HttpGet("getRolesWithInactive")]
        [SwaggerOperation(Summary = "All Roles", Description = "This endpoint is used to get all Roles " +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<RoleModel>), 200)]
        public IActionResult GetRolesWithInactive() => Ok(_roleService.GetResponseRolesWithInactive());

        [HttpGet("getRole/{id}")]
        [SwaggerOperation(Summary = "Role", Description = "This endpoint is used to get Role with Id " +
                    "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<RoleModel>), 200)]
        public IActionResult GetRole(int id) => Ok(_roleService.GetResponseRole(id));

        [HttpPost("createRole")]
        [SwaggerOperation(Summary = "Role", Description = "This endpoint is used to create Role" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<RoleModel>), 200)]
        public IActionResult CreateRole([FromBody] NewRoleModel _) => Ok(_roleService.CreateRole(_));

        [HttpPut("editRole/{id}")]
        [SwaggerOperation(Summary = "Role", Description = "This endpoint is used to edit Role" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<object>), 200)]
        public IActionResult UpdateRole(int id, [FromBody] EditRoleModel _) => Ok(_roleService.UpdateRole(id, _));

        [HttpPut("editRoleAuthorization/{id}")]
        [SwaggerOperation(Summary = "RoleAuthorization", Description = "This endpoint is used to edit RoleAuthorization" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<object>), 200)]
        public IActionResult UpdateRoleAuth(int id, [FromBody] EditRoleAuthorizationModel _) => Ok(_roleService.UpdateRoleAuthorization(id, _));
    }
}
