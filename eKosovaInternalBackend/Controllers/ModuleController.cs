﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Business;
using eKosovaInternal.SharedModel.General;
using eKosovaInternal.SharedModel.Modules;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModuleController : ControllerBase
    {
        private readonly IModuleService _service;

        public ModuleController(IModuleService service)
        {
            _service = service;
        }

        [HttpGet("getModules")]
        [SwaggerOperation(Summary = "Modules", Description = "This endpoint is used to get Modules " +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<DefinationBaseModel>), 200)]
        public IActionResult GetModules() => Ok(_service.GetResponseModule());

        [HttpGet("getModulesByRole/{ID}")]
        [SwaggerOperation(Summary = "Modules", Description = "This endpoint is used to get Modules by Role" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<ModuleModel>), 200)]
        public IActionResult GetModules(int ID) => Ok(_service.GetResponseModule(ID));

        [HttpGet("getResponseModuleTree")]
        [SwaggerOperation(Summary = "Modules", Description = "This endpoint is used to get Modules with Parent ID" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<ModuleModel>), 200)]
        public IActionResult GetResponseModuleTree(int ID) => Ok(_service.GetResponseModuleTree());

        [HttpGet("getModulesByUser/{ID}")]
        [SwaggerOperation(Summary = "Modules", Description = "This endpoint is used to get Modules by Role" +
           "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<ModuleModel>), 200)]
        public IActionResult GetModulesForUser(int ID) => Ok(_service.GetResponseModuleForUser(ID));

        [HttpPost("create")]
        [SwaggerOperation(Summary = "Module", Description = "This endpoint is used to create a module" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<DefinationBaseModel>), 200)]
        public IActionResult Create([FromBody] NewModuleModel _) => Ok(_service.AddModule(_));

        [HttpGet("reportTypes")]
        [SwaggerOperation(Summary = "Module", Description = "This endpoint is used to create a module of reports" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<LookupBaseModel>), 200)]
        public IActionResult ReportTypes() => Ok(_service.GetReportModules());

    }
}
