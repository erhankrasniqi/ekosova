﻿using eKosovaInternal.Business;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.SharedModel.Logs;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly ILogService _logService;
        public LogController(ILogService logService)
        {
            _logService = logService;
        }

        [HttpGet("userAuthorization")]
        [SwaggerOperation(Summary = "Log User Authorization", Description = "This endpoint is used to Log User Authorization when the user signout" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<object>), 200)]
        public IActionResult LogAuth() => Ok(_logService.SetLogUserAuthorization((int)UserAuthorizationStatus.SignOut));

        [HttpGet("getUserAuthorizations")]
        [SwaggerOperation(Summary = "Log User Authorization", Description = "This endpoint is used to Log User Authorization when the user signout" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<object>), 200)]
        public IActionResult LogUserAuthorizations() => Ok(_logService.GetResponseLogUserAuthorizations());

        [HttpGet("userAuthorizations/{idUser}")]
        [SwaggerOperation(Summary = "Log User Authorization", Description = "This endpoint is used to get Log User Authorization of a specific user using idUser" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<LogWithIdUser>), 200)]
        public IActionResult LogUserAuthorizations(int idUser) => Ok(_logService.GetResponseLogUserAuthorization(idUser));

        [HttpGet("getAllFailedAuthorizations")]
        [SwaggerOperation(Summary = "Log User Falied Authorization", Description = "This endpoint is used to get Log User Falied Auhtorization for a specific date using entryDate" +
            "</br> ResponseBody \"status\" can be Done=0")]
        [ProducesResponseType(typeof(Response<LogWithAccount>), 200)]
        public IActionResult LogFailedAuthorizations() => Ok(_logService.GetResponseLogFailedAuthenticationsAll());
    }
}
