﻿using eKosovaInternal.Domain;
using eKosovaInternalBackend.Interfaces;
using eKosovaInternalBackend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace eKosovaInternalBackend.Controllers
{
	public class AuthController : Controller
	{
		private readonly IAuthService _authService;
		private readonly IADDomainService _addomainService;

		public AuthController(IAuthService authService, IADDomainService domainService)
		{
			_authService = authService;
			_addomainService = domainService;
		}

		[HttpGet]
		[Authorize]
		public ActionResult AccessLimited() => Content("Authorized API action");

		[HttpPost("authenticate")]
		public async Task<IActionResult> Authenticate([FromBody] Credentials credentials) => Ok(await _authService.VerifyLogInUser(credentials));

		[HttpPost("refresh")]
		public async Task<IActionResult> Refresh([FromBody] RefreshTokenModel refreshToken) => Ok(await _authService.Refresh(refreshToken));
	}
}
