﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.SharedModel.PublicUsers;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PublicUsersController : ControllerBase
    {
        private readonly IPublicUserService _publicusersService;

        public PublicUsersController(IPublicUserService publicusersService)
        {
            _publicusersService = publicusersService;
        }

        [HttpGet("getPublicUsers")]
        [ProducesResponseType(typeof(Response<IList<PublicUser>>), 200)]
        public IActionResult GetPublicUsers() => Ok(_publicusersService.GetResponsePublicUserList());


        [HttpGet("getPublicUsersById")]
        [ProducesResponseType(typeof(Response<IList<PublicUser>>), 200)]
        public IActionResult GetById() => Ok(_publicusersService.GetResponsePublicUserList());

        [HttpPost("getPublicUser")]
        //[ProducesResponseType(typeof(Response<IList<PublicUser>>), 200)]
        public async Task<IActionResult> GetByPersonalNumber([FromBody] BaseIdentification baseIdentification) => Ok(await _publicusersService.GetResponsePublicUserPersonalNumber(baseIdentification.Identification));


        [HttpGet("getActivePublicUsers")]
        [ProducesResponseType(typeof(Response<IList<PublicUser>>), 200)]
        public IActionResult GetActivePublicUsers() => Ok(_publicusersService.GetResponsePublicUserList());

        [HttpPost("edit")]
        [ProducesResponseType(typeof(Response<object>), 200)]
        public IActionResult Update([FromBody] UpdatePublicUserModel _) => Ok(_publicusersService.UpdatePublicUser(_));
    }
}
