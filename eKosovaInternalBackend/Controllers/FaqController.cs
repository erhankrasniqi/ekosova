﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eKosovaInternal.Business;
using eKosovaInternal.Domain.Faqs;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.SharedModel.Faqs;
using eKosovaInternalBackend.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eKosovaInternalBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FaqController : Controller
    {
        // private readonly IFaqService faqService;
        private readonly IFaqService _faqService;

        public FaqController(IFaqService faqService)
        {
            _faqService = faqService;
        }

        [HttpGet("getFaqs")]
        [ProducesResponseType(typeof(Response<List<Faq>>), 200)]  
        public IActionResult GetFaqs() => Ok(_faqService.GetResponseFaqList());

        [HttpGet("getActiveFaqs")]
        [ProducesResponseType(typeof(Response<List<Faq>>), 200)]
        public IActionResult getActiveFaqs() => Ok(_faqService.GetResponseFaqList());

        [HttpPost("create")]
        [ProducesResponseType(typeof(Response<FaqModel>), 200)]
        public IActionResult Create([FromBody] CreateFaqModel _) => Ok(_faqService.Create(_));

        [HttpPost("edit")]
        [ProducesResponseType(typeof(Response<object>), 200)]
        public IActionResult Update([FromBody] UpdateFaqModel _) => Ok(_faqService.UpdateFaq(_));

        [HttpPost("delete")]
        [ProducesResponseType(typeof(Response<object>), 200)]
        public IActionResult Delete([FromBody] BaseEntity baseEntity) => Ok(_faqService.DeleteFaq(baseEntity.ID));

    }
}
