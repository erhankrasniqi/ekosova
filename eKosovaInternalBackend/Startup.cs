using System;
using System.Text;
using AutoMapper;
using eKosovaInternal.Business.Helpers;
using eKosovaInternal.Domain;
using eKosovaInternal.Domain.Contacts;
using eKosovaInternal.Domain.DynamicForms;
using eKosovaInternal.Domain.Emails;
using eKosovaInternal.Domain.Faqs;
using eKosovaInternal.Domain.Helps;
using eKosovaInternal.Domain.Medias;
using eKosovaInternal.Domain.Notifications;
using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Infrastructure.Contacts;
using eKosovaInternal.Infrastructure.DynamicForms;
using eKosovaInternal.Infrastructure.Emails;
using eKosovaInternal.Infrastructure.Faqs;
using eKosovaInternal.Infrastructure.Helps;
using eKosovaInternal.Infrastructure.InternalUsers;
using eKosovaInternal.Infrastructure.Medias;
using eKosovaInternal.Infrastructure.Notifications;
using eKosovaInternal.Infrastructure.PublicUsers;
using eKosovaInternal.Infrastructure.Repositories;
using eKosovaInternal.Infrastructure.Services;
using eKosovaInternal.Infrastructure.Administrations;
using eKosovaInternal.Domain.Modules;
using eKosovaInternal.Domain.Roles;
using eKosovaInternalBackend.Interfaces;
using eKosovaInternalBackend.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using RepoDb;
using eKosovaInternal.Infrastructure.Roles;
using eKosovaInternal.Infrastructure.ADDomains;
using eKosovaInternal.Domain.ADDomains;
using eKosovaInternal.Infrastructure.Hierarchies;
using eKosovaInternal.Domain.Hierarchies;
using eKosovaInternal.Domain.UserAuthorizations;
using eKosovaInternal.Infrastructure.UserAuthorizations;
using eKosovaInternal.Domain.Edergesa;
using eKosovaInternal.Infrastructure.Edergesa;
using eKosovaInternal.Domain.Logs;
using eKosovaInternal.Infrastructure.Logs;
using System.Collections.Generic;
using eKosovaInternal.Domain.PublicUserHistory;
using eKosovaInternal.Infrastructure.PublicUserHistory;

namespace eKosovaInternalBackend
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}
		// readonly string MyAllowSpecificOrigin = "_myAllowSpecificOrigins";
		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{

			//services.AddCors(Options =>
			//{
			//    Options.AddPolicy("_myAllowSpecificOrigins",
			//        builder => builder
			//        .AllowAnyOrigin()
			//        .AllowAnyMethod()
			//        .AllowAnyHeader()
			//        );

			//});

			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

			services.AddSession(options =>
			{
				options.IdleTimeout = TimeSpan.FromMinutes(30);
				options.Cookie.HttpOnly = true;
				options.Cookie.IsEssential = true;
			});

			services.AddSwaggerGen(c =>
			{
				c.EnableAnnotations();
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "eKosova Internal API", Version = "v1" });
				c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
				{
					Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
					Name = "Authorization",
					In = ParameterLocation.Header,
					Type = SecuritySchemeType.ApiKey,
					Scheme = "Bearer"
				});

				c.AddSecurityRequirement(new OpenApiSecurityRequirement()
				  {
					{
					  new OpenApiSecurityScheme
					  {
						Reference = new OpenApiReference
						  {
							Type = ReferenceType.SecurityScheme,
							Id = "Bearer"
						  },
						  Scheme = "oauth2",
						  Name = "Bearer",
						  In = ParameterLocation.Header,

						},
						new List<string>()
					  }
					});
			});

			var validationParams = new TokenValidationParameters
			{
				ClockSkew = TimeSpan.Zero,

				ValidateAudience = true,
				ValidAudience = Configuration["Token:Audience"],

				ValidateIssuer = true,
				ValidIssuer = Configuration["Token:Issuer"],

				IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["Token:SigningKey"])),
				ValidateIssuerSigningKey = true,

				RequireExpirationTime = true,
				ValidateLifetime = true,

			};

			var validTime = int.Parse(Configuration["Token:ValidTimeInMinutes"]);

			services.AddJwtAuthenticationForAPI(validationParams, validTime);
			SqlServerBootstrap.Initialize();

			services.AddScoped<IAuthService, AuthService>();
			services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));
			services.AddScoped(typeof(IAsyncRepository<>), typeof(GenericRepository<>));
			services.AddScoped(typeof(IGeneralUpdateService<>), typeof(GeneralUpdateService<>));
			services.AddScoped<IContactService, ContactService>();
			services.AddScoped<IContactRepoService, ContactRepoService>();
			services.AddScoped<IHelpService, HelpService>();
			services.AddScoped<IHelpRepoService, HelpRepoService>();
			services.AddScoped<eKosovaInternal.Domain.InternalUsers.IInternalUserRepoService, InternalUserRepoService>();
			services.AddScoped<IInternalUserService, InternalUserService>();
			services.AddScoped<IEmailSender, EmailSender>();
			services.AddScoped<IRoleRepository, RoleRepository>();
			services.AddScoped<IRoleService, RoleService>();
			services.AddScoped<IModuleRepository, ModuleRepository>();
			services.AddScoped<IModuleService, ModuleService>();



			services.AddScoped<IMediaService, MediaService>();
			services.AddScoped<IMediaRepoService, MediaRepoService>();

			services.AddScoped<IFaqService, FaqService>();
			services.AddScoped<IFaqRepoService, FaqRepoService>();

			services.AddScoped<IPublicUserService, PublicUsersService>();
			services.AddScoped<IPublicUsersRepoService, PublicUsersRepoService>();

			services.AddScoped<ISubmissionPublicUserService, SubmissionPublicUserService>();
			services.AddScoped<ISubmissionPublicUserRepoService, SubmissionPublicUserRepoService>();

			services.AddScoped<ILogRepository, LogRepository>(); //ssssssssssssssssspodi
			services.AddScoped<ILogService, LogService>();

			services.AddScoped<IDynamicFormsService, DynamicFormsService>();
			services.AddScoped<IDynamicFormsRepository, DynamicFormsRepository>();

			services.AddScoped<IServiceCategoryService, ServiceCategoryService>();
			services.AddScoped<IServicesRepository, ServicesRepository>();

			services.AddScoped<INotificationRepoService, NotificationRepoService>();
			services.AddScoped<INotificationQueueService, NotificationQueueService>();

			services.AddScoped<IADDomainRepoService, ADDomainRepoService>();
			services.AddScoped<IADDomainService, ADDomainService>();

			services.AddScoped<IHierarchyService, HierarchyService>();
			services.AddScoped<IHierarchyRepoService, HierarchyRepoService>();

			services.AddScoped<IUserAuthorizationService, UserAuthorizationService>();
			services.AddScoped<IUserAuthorizationRepoService, UserAuthorizationRepoService>();

			services.AddScoped<IPublicUserHistoryService, PublicUserHistoryService>();
			services.AddScoped<IPublicUserHistoryRepoService, PublicUserHistoryRepoService>();

			services.Configure<Token>(Configuration.GetSection("Token"));

			services.AddAutoMapper(typeof(Startup));

			var appSettingsSection = Configuration.GetSection("AppSettings");

			var appSettings = appSettingsSection.Get<AppSettings>();

			services.AddMvc(_ => _.EnableEndpointRouting = false);

			services.Configure<AppSettings>(appSettingsSection);

			services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			//  app.UseCors(MyAllowSpecificOrigin);
			app.UseSession();

			app.UseMvc();

			app.UseSwagger();

			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "eKosova Internal API Version 1");
			});

			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthentication();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
