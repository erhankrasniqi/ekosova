﻿using AutoMapper;
using eKosova.Domain.DynamicForms;
using eKosovaInternal.Domain.ADDomains;
using eKosovaInternal.Domain.Contacts;
using eKosovaInternal.Domain.Edergesa;
using eKosovaInternal.Domain.Faqs;
using eKosovaInternal.Domain.Helps;
using eKosovaInternal.Domain.Hierarchies;
using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Domain.Logs;
using eKosovaInternal.Domain.Medias;
using eKosovaInternal.Domain.Modules;
using eKosovaInternal.Domain.Notifications;
using eKosovaInternal.Domain.PublicUserHistory;
using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.Domain.Roles;
using eKosovaInternal.Domain.Services;
using eKosovaInternal.Domain.Submission;
using eKosovaInternal.SharedModel.Contacts;
using eKosovaInternal.SharedModel.DynamicForms;
using eKosovaInternal.SharedModel.Edergesa;
using eKosovaInternal.SharedModel.Faqs;
using eKosovaInternal.SharedModel.General;
using eKosovaInternal.SharedModel.Helps;
using eKosovaInternal.SharedModel.Logs;
using eKosovaInternal.SharedModel.Medias;
using eKosovaInternal.SharedModel.Modules;
using eKosovaInternal.SharedModel.Notifications;
using eKosovaInternal.SharedModel.PublicUserHistory;
using eKosovaInternal.SharedModel.PublicUsers;
using eKosovaInternal.SharedModel.Roles;
using eKosovaInternal.SharedModel.Submission;

namespace eKosovaInternalBackend.Profiles
{
	public class AutoMapping : Profile
	{
		public AutoMapping()
		{
			CreateMap<ContactModel, Contact>().ReverseMap();
			CreateMap<HelpModel, Help>().ReverseMap();
			CreateMap<MediaModel, Media>().ReverseMap();
			CreateMap<CreateFaqModel, Faq>().ReverseMap();
			CreateMap<FaqModel, Faq>().ReverseMap();
			CreateMap<UpdatePublicUserModel, UpdatePublicUser>().ReverseMap();
			CreateMap<CreateNotificationQueue, NotificationQueue>().ReverseMap();
			CreateMap<DynamicFormModel, DynamicForm>().ReverseMap();
			CreateMap<QuestionModel, DynamicFormQuestions>().ReverseMap();
			CreateMap<DynamicFormOption, DynamicFormQuestionsOptions>().ReverseMap();
			CreateMap<DynamicFormGridOption, DynamicFormGridXQuestions>().ReverseMap();
			CreateMap<DynamicFormGridOption, DynamicFormGridYQuestions>().ReverseMap();
			CreateMap<DynamicFormQuestionGridXY, DynamicFormGridXY>().ReverseMap();
			CreateMap<MainServices, ServiceCategory>().ReverseMap();
			CreateMap<RoleModel, Role>().ReverseMap();
			CreateMap<NewRoleModel, Role>().ReverseMap();
			CreateMap<EditRoleModel, Role>().ReverseMap();
			CreateMap<NewModuleModel, Module>().ReverseMap();
			CreateMap<ModuleModel, Module>().ReverseMap();
			CreateMap<InternalUserViewModel, InternalUser>().ReverseMap();
			CreateMap<ADDomain, ADDomainViewModel>().ReverseMap();
			CreateMap<DefinationBaseModel, RoleAuthorizationType>().ReverseMap();
			CreateMap<RoleAuthorizationModel, RoleAuthorization>().ReverseMap();
			CreateMap<HierarchyViewModel, Hierarchy>().ReverseMap();
			CreateMap<InternalUserCreate, InternalUser>().ReverseMap();
			CreateMap<SubmissionPersonalUserModel, SubmissionPublicUser>().ReverseMap();
			CreateMap<HierarchysubmissionTypeModel, HierarchySubmissionType>().ReverseMap();
			CreateMap<InsertSubmissionModel, InsertSubmission>().ReverseMap();
			CreateMap<InsertSubmissionDocumentModel, InsertSubmissionDocument>().ReverseMap();
			CreateMap<LogWithIdUser, LogUserAuthorization>().ReverseMap();
			CreateMap<LogWithIdEntryUser, LogUserActivity>().ReverseMap();
			CreateMap<LogWithAccount, LogFailedAuthentication>().ReverseMap();
			CreateMap<PublicUserHistoryModel, PublicUserHistory>().ReverseMap();
		}
	}
}
