﻿using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Domain.UserAuthorizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eKosovaInternal.Infrastructure.UserAuthorizations
{
	public class UserAuthorizationRepoService : IUserAuthorizationRepoService
	{
		private readonly IRepository<UserAuthorizationType> _repository;

		public UserAuthorizationRepoService(IRepository<UserAuthorizationType> repository)
		{
			_repository = repository;
		}

		public IList<UserAuthorizationType> GetAll()
		{
			return _repository.GetAll().ToList();
		}
	}
}
