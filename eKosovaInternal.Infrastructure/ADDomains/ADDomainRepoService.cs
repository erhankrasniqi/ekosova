﻿using eKosovaInternal.Domain.ADDomains;
using eKosovaInternal.Domain.SeedWork;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.ADDomains
{
	public class ADDomainRepoService : IADDomainRepoService
	{
		private readonly IRepository<ADDomain> _addRepository;

		public ADDomainRepoService(IRepository<ADDomain> addRepository)
		{
			_addRepository = addRepository;
		}

		public Task<ADDomain> GetADDomain(int ID)
		{
			string query = "EXEC [dbo].[SelectADDomainById] @ID";
			object param = new { ID };
			return _addRepository.Read(query, param);
		}

		public async Task<IList<ADDomain>> GetAllDomains()
		{
			string query = "EXEC [dbo].[SelectADDomainAll]";
			return (await _addRepository.GetAllQuery(query)).ToList();
		}
	}
}
