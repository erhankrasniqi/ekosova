﻿using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.Domain.SeedWork;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RepoDb;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.Repositories
{
	public class GenericRepository<T> : BaseRepository<T, SqlConnection>, IRepository<T>, IAsyncRepository<T> where T : BaseEntity
	{
		private static readonly string connectionString = appConfig.Default.ConnectionString;

		public GenericRepository() : base(connectionString) { }

		public T Create(T model)
		{
			throw new NotImplementedException();
		}

		public async Task<T> Create(T model, string query, object param)
		{
			try
			{
				using (var db = await new SqlConnection(connectionString).EnsureOpenAsync())
				{
					var response = (await db.ExecuteQueryAsync<T>(query, param)).FirstOrDefault();
					return response;
				}
			}
			catch (Exception error)
			{
				return null;
			}
		}
		public T Add(string query, object param)
		{
			try
			{
				using (var db = new SqlConnection(connectionString).EnsureOpen())
				{
					var response = (db.ExecuteQuery<T>(query, param)).FirstOrDefault();
					return response;
				}
			}
			catch (Exception error)
			{
				return null;
			}
		}

		public Task<T> CreateAsync(T model)
		{
			throw new NotImplementedException();
		}

		public void Delete(T model)
		{
			throw new NotImplementedException();
		}

		public Task DeleteAsync(T model)
		{
			throw new NotImplementedException();
		}

		public int DeleteId(int id)
		{
			throw new NotImplementedException();
		}

		public Task<int> DeleteIdAsync(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<T> GetAll()
		{
			try
			{
				using (var connection = new SqlConnection(connectionString).EnsureOpen())
				{

					var list = connection.QueryAll<T>();
					return list;
				}

			}
			catch (Exception e)
			{
				return null;
			}
		}
		public async Task<IEnumerable<T>> GetAllQuery(string query)
		{
			try
			{
				using (var connection = new SqlConnection(connectionString).EnsureOpen())
				{

					//var list = connection.QueryAll<T>();
					var response = (await connection.ExecuteQueryAsync<T>(query));
					return response;
				}

			}
			catch (Exception e)
			{
				return null;
			}
		}

		public IEnumerable<T> GetAll(string query)
		{
			try
			{
				using (var connection = new SqlConnection(connectionString).EnsureOpen())
				{

					//var list = connection.QueryAll<T>();
					var response = connection.ExecuteQuery<T>(query);
					return response;
				}

			}
			catch (Exception e)
			{
				return null;
			}
		}

		public IEnumerable<T> GetAll(string query, object param)
		{
			try
			{
				using (var connection = new SqlConnection(connectionString).EnsureOpen())
				{

					//var list = connection.QueryAll<T>();
					var response = connection.ExecuteQuery<T>(query, param);
					return response;
				}

			}
			catch (Exception e)
			{
				return null;
			}
		}

		public Task<IEnumerable<T>> GetAllAsync()
		{
			throw new NotImplementedException();
		}

		public async Task<T> Read(string query, object parameters)
		{
			try
			{
				using (var db = await new SqlConnection(connectionString).EnsureOpenAsync())
				{
					var response = (await db.ExecuteQueryAsync<T>(query, parameters)).FirstOrDefault();
					return response;
				}
			}
			catch (Exception error)
			{
				return null;
			}
		}
		public T ReadById(string query, object parameters)
		{
			try
			{
				using (var db = new SqlConnection(connectionString).EnsureOpen())
				{
					var response = db.ExecuteQuery<T>(query, parameters).FirstOrDefault();
					return response;
				}
			}
			catch (Exception error)
			{
				return null;
			}
		}

		public Task<T> ReadAsync(int id)
		{
			throw new NotImplementedException();
		}

		public T Update(string query, object param)
		{
			try
			{
				using (var db = new SqlConnection(connectionString).EnsureOpen())
				{
					var response = db.ExecuteQuery<T>(query, param).FirstOrDefault(); // jo
					return response;
				}
			}
			catch (Exception error)
			{
				return null;
			}
		}

		public Task UpdateAsync(T model)
		{
			throw new NotImplementedException();
		}

		public T GetSingleByCriteria(Func<T, bool> criteria)
		{
			return ListByCriteria(criteria).FirstOrDefault();
		}

		public T GetSingleByCriteria(Func<T, bool> criteria, params string[] includes)
		{
			try
			{
				using (var connection = new SqlConnection(connectionString).EnsureOpen())
				{

					var list = connection.Query<T>(criteria).FirstOrDefault();
					return list;
				}

			}
			catch (Exception e)
			{
				return null;
			}
		}

		public bool Any(Func<T, bool> criteria)
		{
			try
			{
				using (var connection = new SqlConnection(connectionString).EnsureOpen())
				{

					var list = connection.QueryAll<T>().Any(criteria);
					return list;
				}

			}
			catch (Exception e)
			{
				return false;
			}
		}

		public List<T> ListByCriteria(Func<T, bool> criteria, params string[] includes)
		{
			try
			{
				using (var connection = new SqlConnection(connectionString).EnsureOpen())
				{

					var list = connection.QueryAll<T>().Where(criteria).ToList();
					return list;
				}

			}
			catch (Exception e)
			{
				return null;
			}
		}

		public int CountByCriteria(Func<T, bool> criteria)
		{
			try
			{
				using (var connection = new SqlConnection(connectionString).EnsureOpen())
				{

					var list = connection.QueryAll<T>().Where(criteria).Count();
					return list;
				}

			}
			catch (Exception e)
			{
				return 0;
			}
		}
	}

}
