﻿using eKosovaInternal.Domain.Modules;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eKosovaInternal.Infrastructure.Administrations
{
    public class ModuleRepository : IModuleRepository
    {
        private readonly IRepository<Module> _repository;
        public ModuleRepository(IRepository<Module> repository)
        {
            _repository = repository;
        }
        public IEnumerable<Module> GetModules() => _repository.GetAll();
        public IEnumerable<Module> GetModules(Func<Module, bool> criteria) {
           return _repository.ListByCriteria(criteria).OrderByDescending(d => d.ID);
        } 

        //public IEnumerable<Module> GetModulesWithAuthorization()
        //{
        //    string query = "EXEC [dbo].[SelectModuleWithAuthorization]";
        //    var list = _repository.GetAll(query);
        //    return list;
        //}

        //public IEnumerable<Module> GetModulesWithUserAuthorization() => _repository.ListAll("UserAuthorizations").Where(_ => _.IsPublic == false).OrderBy(d => d.Title);
    }
}
