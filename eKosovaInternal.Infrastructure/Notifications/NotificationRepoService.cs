﻿using eKosovaInternal.Domain.Notifications;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Infrastructure.Notifications
{
    public class NotificationRepoService : INotificationRepoService
    {
        private readonly IRepository<NotificationQueue> _notificationRepository;
        public NotificationRepoService(IRepository<NotificationQueue> notificationRepository)
        {
            _notificationRepository = notificationRepository;
        }
       
        public void AddNotificationQueue(NotificationQueue _)
        {
            string query = "EXEC [dbo].[InsertNotificationQueue] @Message, @ToEmail, @ToPhone, @Identification, @IDNotificationQueueType";
            object parameters = new { _.Message, _.ToEmail, _.ToPhone, _.Identification, _.IDNotificationQueueType };
            _notificationRepository.Create(_,query, parameters);
        }
    }
}
