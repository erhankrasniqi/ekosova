﻿using eKosovaInternal.Domain.Hierarchies;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.Hierarchies
{
	public class HierarchyRepoService : IHierarchyRepoService
	{
		private readonly IRepository<Hierarchy> _repository;

		public HierarchyRepoService(IRepository<Hierarchy> repository)
		{
			_repository = repository;
		}

		public IList<Hierarchy> GetAll()
		{
			return _repository.GetAll().ToList();
		}
	}
}
