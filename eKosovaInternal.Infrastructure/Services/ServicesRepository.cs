﻿using eKosovaInternal.Domain.DynamicForms;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.Services
{
    public class ServicesRepository : IServicesRepository
    {
        private readonly IRepository<ServiceCategory> _serviceRepository;
        public ServicesRepository(IRepository<ServiceCategory> serviceRepository)
        {
            _serviceRepository = serviceRepository;
        }

        public IEnumerable<ServiceCategory> GetServiceCategories()
        {
           return _serviceRepository.GetAll();
        }
    }
}
