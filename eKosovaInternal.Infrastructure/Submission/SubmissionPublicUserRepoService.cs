﻿using eKosovaInternal.Domain.Edergesa;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Domain.Submission;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.Edergesa
{
   public class SubmissionPublicUserRepoService : ISubmissionPublicUserRepoService
    {
        private readonly IRepository<SubmissionPublicUser> _apublicuserRepository;
        private readonly IRepository<HierarchySubmissionType> _hierarchySubmissionTypeRepository;
        private readonly IRepository<InsertSubmission> _submissionRepository;
        private readonly IRepository<InsertSubmissionDocument> _documentinsertRepository;
        

        public SubmissionPublicUserRepoService(IRepository<SubmissionPublicUser> apublicuserRepository, IRepository<HierarchySubmissionType> hierarchySubmissionTypeRepository, IRepository<InsertSubmission> submissionRepository, IRepository<InsertSubmissionDocument> documentinsertRepository)
        {
            _apublicuserRepository = apublicuserRepository;
            _hierarchySubmissionTypeRepository = hierarchySubmissionTypeRepository;
            _submissionRepository = submissionRepository;
            _documentinsertRepository = documentinsertRepository;
        }
  
        public IEnumerable<SubmissionPublicUser> GetPublicUserByPersonalNumber(int personalNumber)
        {
            var query = "EXEC [dbo].[SelectPublicUserByID] @PersonalNumber";
            object parameters = new
            {
                PersonalNumber = personalNumber
            }; 
            return _apublicuserRepository.GetAll(query, parameters);
        }

        public IEnumerable<HierarchySubmissionType> GetHierarchyList()
        {
            var query = "EXEC [dbo].[SelectHiearchySubmissionType]";
           
            return _hierarchySubmissionTypeRepository.GetAll(query);
        }

        public void InserSubmission(InsertSubmission _)
        {
            string query = "EXEC [dbo].[InsertSubmission] @Title, @Subject,  @HierarchyCategory, @Content, @PersonalNumber";
            object parameters = new { _.Title, _.Subject, _.HierarchyCategory, _.Content, _.PersonalNumber };
            _submissionRepository.Create(_, query, parameters);
        }

        public void InserSubmissionDocument(InsertSubmissionDocument _)
        {
            string query = "EXEC [dbo].[InsertSubmissionDocument] @IDSubmission, @Path,  @Size, @ExtensionType, @Name";
            object parameters = new { _.IDSubmission, _.Path, _.Size, _.Extensiontype, _.Name };
            _documentinsertRepository.Create(_, query, parameters);
        }
    }
}
