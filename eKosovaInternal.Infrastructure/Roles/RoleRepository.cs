﻿using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Domain.Roles;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace eKosovaInternal.Infrastructure.Roles
{
    public class RoleRepository : IRoleRepository
    {
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<RoleAuthorization> _roleAuthRepository;
        private readonly IRepository<UserAuthorization> _userAuthRepository;
        private readonly IRepository<RoleAuthorizationType> _roleAuthTypeRepository;

        public RoleRepository(IRepository<Role> roleRepository,
                              IRepository<RoleAuthorization> roleAuthRepository,
                              IRepository<UserAuthorization> userAuthRepository,
                              IRepository<RoleAuthorizationType> roleAuthTypeRepository)
        {
            _roleRepository = roleRepository;
            _roleAuthRepository = roleAuthRepository;
            _userAuthRepository = userAuthRepository;
            _roleAuthTypeRepository = roleAuthTypeRepository;
        }

        public IEnumerable<Role> GetRoles() => _roleRepository.ListByCriteria(_ => !_.IsDeleted).OrderByDescending(d => d.ID);

        public IEnumerable<Role> GetRolesWithCriteria(Func<Role, bool> criteria) => _roleRepository.ListByCriteria(criteria).OrderByDescending(d => d.ID);

        public Role GetRole(int id) => _roleRepository.GetSingleByCriteria(_ => _.ID == id && !_.IsDeleted);

        public IEnumerable<RoleAuthorizationType> GetRoleAuthTypes() => _roleAuthTypeRepository.GetAll();

        public void AddRoleAuthorization(RoleAuthorization _)
        {
            string query = "EXEC [dbo].[InsertRoleAuthorization] @ID, @IDRole, @IDRoleAuthorizationType, @IDModule, @EntryUser, @EntryDate, @IDEntryUser";
            object parameters = new { _.ID, _.IDRole, _.IDRoleAuthorizationType, _.IDModule, _.EntryUser, _.EntryDate, _.IDEntryUser };
            _roleAuthRepository.Create(_,query, parameters);
        }
        public void AddRangeRoleAuthorization(List<RoleAuthorization> _)
        {
            string query = "EXEC [dbo].[InsertRoleAuthorization] @IDRole, @IDRoleAuthorizationType, @IDModule, @EntryUser, @EntryDate, @IDEntryUser";
            object parameters;
            foreach (var item in _) { 
                parameters = new { item.IDRole, item.IDRoleAuthorizationType, item.IDModule, item.EntryUser, item.EntryDate, item.IDEntryUser };
                _roleAuthRepository.Create(item, query, parameters);
            }
        }
        public RoleAuthorization GetRoleAuthorization(int ID) {
            string query = "EXEC [dbo].[SelectRoleAuthorizationById] @ID";
            object parameters = new { ID };
            return _roleAuthRepository.ReadById(query, parameters);
        }
        public IEnumerable<RoleAuthorization> GetRoleAuthorizationByRole(int IDRole)
        {
            string query = "EXEC [dbo].[SelectRoleAuthorizationByRoleId] @IDRole";            
            object parameters = new { IDRole };
            return _roleAuthRepository.GetAll(query, parameters);
        }

        public IEnumerable<UserAuthorization> GetRoleAuthorizationByUser(int IDInternalUser) {

            string query = "EXEC [dbo].[SelectRoleAuthorizationByInternalUser] @IDInternalUser";
            object parameters = new { IDInternalUser };
            return _userAuthRepository.GetAll(query, parameters);
        }

        public RoleAuthorizationType GetRoleAuthTypeByRoleAndModule(int IDRole, int IDModule)
        { 
            string query = "EXEC [dbo].[SelectRoleAuthorizationTypeByRoleIdAndModule] @IDRole, @IDModule";
            object parameters = new { IDRole, IDModule };
            return _roleAuthTypeRepository.ReadById(query, parameters);
        }
           

    }
}
