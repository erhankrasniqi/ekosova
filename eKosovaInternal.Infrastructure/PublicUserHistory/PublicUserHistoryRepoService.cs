﻿using eKosovaInternal.Domain.PublicUserHistory;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.PublicUserHistory
{
    public class PublicUserHistoryRepoService : IPublicUserHistoryRepoService
    {
        private readonly IRepository<Domain.PublicUserHistory.PublicUserHistory> _userHistoryRepository;

        public PublicUserHistoryRepoService(IRepository<Domain.PublicUserHistory.PublicUserHistory> userHistoryRepository)
        {
            _userHistoryRepository = userHistoryRepository;
        }

        public IEnumerable<Domain.PublicUserHistory.PublicUserHistory> GetAllPublicUserHistory(int publicUserID)
        {
            var query = "EXEC [dbo].[SelectPublicUserHistory] @PublicUserID";
            object parameters = new
            {
                PublicUserID = publicUserID
            };
            return _userHistoryRepository.GetAll(query, parameters);
        }

         
    }
}
