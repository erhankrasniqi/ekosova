﻿using eKosovaInternal.Domain.InternalUsers;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.InternalUsers
{
	public class InternalUserRepoService : IInternalUserRepoService
	{
		private readonly IRepository<InternalUser> _repository;

		public InternalUserRepoService(IRepository<InternalUser> repository)
		{
			_repository = repository;
		}

		public async Task<InternalUser> Add(InternalUser _)
		{
			var query = "EXEC [dbo].[InsertInternalUser] @First, @Last, @Account, @Email, @PersonalNumber, @PasswordHash, @PasswordSalt, @PhoneNumber, @IDHierarchy, @IDRole, @IDUserAuthorizationType, @IDActiveDirectoryDomain, @Identification";
			object parameters = new
			{
				_.First,
				_.Last,
				_.Account,
				_.Email,
				_.PersonalNumber,
				_.PasswordHash,
				_.PasswordSalt,
				_.PhoneNumber,
				_.IDHierarchy,
				_.IDRole,
				_.IDUserAuthorizationType,
				_.IDActiveDirectoryDomain,
				Identification = Guid.NewGuid().ToString()
			};
			return await _repository.Create(_, query, parameters);
		}

		public IEnumerable<InternalUser> GetActive()
		{
			throw new NotImplementedException();
		}

		public IList<InternalUser> GetAll()
		{
			return _repository.GetAll().ToList();
		}

		public async Task<InternalUser> GetById(int id)
		{
			var query = "EXEC [dbo].[SelectInternalUser] @ID";
			object parameters = new
			{
				ID = id
			};
			return await _repository.Read(query, parameters);
		}

		public async Task<InternalUser> GetByPersonalNumber(string personalNumber)
		{
			var query = "EXEC [dbo].[SelectInternalUserPersonalNumber] @PersonalNumber";
			object parameters = new
			{
				PersonalNumber = personalNumber
			};
			return await _repository.Read(query, parameters);
		}

		public async Task<InternalUser> GetByIdentification(string identification)
		{
			var query = "EXEC [dbo].[SelectInternalUserIdentification] @Identification";
			object parameters = new
			{
				Identification = identification
			};
			return await _repository.Read(query, parameters);
		}

		public void Update(InternalUser _)
		{
			var query = "EXEC [dbo].[UpdateInternalUser] @ID, @Email, @PasswordHash, @PasswordSalt, @PhoneNumber, @IDHierarchy, @IsActive, @IDRole,@IDUserAuthorizationType, @IDActiveDirectoryDomain, @AccessToken, @RefreshToken";
			_repository.Update(query, _);
		}

		public async Task<InternalUser> GetByAccount(string account)
		{
			var query = "EXEC [dbo].[SelectInternalUserAccount] @Account";
			object parameters = new
			{
				Account = account
			};
			return await _repository.Read(query, parameters);
		}
	}
}
