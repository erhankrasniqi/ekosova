﻿using eKosovaInternal.Domain.Logs;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace eKosovaInternal.Infrastructure.Logs
{
    public class LogRepository : ILogRepository
    {
        private readonly IRepository<LogUserAuthorization> _logUserAuthorizationRepository;
        private readonly IRepository<LogFailedAuthentication> _logFailedAuthenticationRepository;
        private readonly IRepository<LogDataChange> _logDataChangeRepository;
        private readonly IRepository<LogUserAuthorizationStatus> _logUserAuthorizationStatusRepository;
        private readonly IRepository<LogUserActivityStatus> _logUserActivityStatusRepository;
        private readonly IRepository<LogUserActivity> _logUserActivityRepository;
        private readonly IRepository<Tables> _logTableRepository;
        public LogRepository(IRepository<LogUserAuthorization> logUserAuthorizationRepository,
                             IRepository<LogFailedAuthentication> logFailedAuthenticationRepository,
                             IRepository<LogDataChange> logDataChangeRepository,
                             IRepository<LogUserAuthorizationStatus> logUserAuthorizationStatusRepository,
                             IRepository<Tables> logTableRepository,
                             IRepository<LogUserActivityStatus> logUserActivityStatusRepository,
                             IRepository<LogUserActivity> logUserActivityRepository)
        {
            _logUserAuthorizationRepository = logUserAuthorizationRepository;
            _logFailedAuthenticationRepository = logFailedAuthenticationRepository;
            _logDataChangeRepository = logDataChangeRepository;
            _logUserActivityStatusRepository = logUserActivityStatusRepository;
            _logUserAuthorizationStatusRepository = logUserAuthorizationStatusRepository;
            _logUserActivityRepository = logUserActivityRepository;
            _logTableRepository = logTableRepository;
        }

        # region LogUserAuthorization
        public void AddLogUserAuthorization(LogUserAuthorization _)
        {
            string query = "EXEC [dbo].[InsertLogUserAuthorization] @IDUser,@ComputerName,@IPAddress,@IDLogBrowserType,@IDLogOperatingSystemType,@IsMobileDevice,@IDLogUserAuthorizationStatus, @EntryUser";
            object parameters = new { _.IDUser, _.ComputerName, _.IPAddress, _.IDLogBrowserType, _.IDLogOperatingSystemType, _.IsMobileDevice, _.IDLogUserAuthorizationStatus, _.EntryUser};
            _logUserAuthorizationRepository.Create(_, query, parameters);
        }

        public IEnumerable<LogUserAuthorization> GetLogUserAuthorizationsByIdUser(int IDUser) {
            string query = "EXEC [dbo].[SelectLogUserAuthorizationsById] @IDUser";
            object parameters = new { IDUser };
            return _logUserAuthorizationRepository.GetAll(query, parameters).OrderByDescending(d => d.ID);
        }
        public IEnumerable<LogUserAuthorization> GetLogUserAuthorizations() {
            string query = "EXEC [dbo].[SelectLogUserAuthorizations]";
            return _logUserAuthorizationRepository.GetAll(query).OrderByDescending(d => d.ID);
        }

        #endregion



        # region LogFailedAuthentication
        public void AddLogFailedAuthentication(LogFailedAuthentication _)
        {
            string query = "EXEC [dbo].[InsertLogFailedAuthentication] @Account,@ComputerName,@IPAddress,@IDLogBrowserType,@IDLogOperatingSystemType,@IsMobileDevice";
            object parameters = new { _.Account, _.ComputerName, _.IPAddress, _.IDLogBrowserType, _.IDLogOperatingSystemType, _.IsMobileDevice};
            _logFailedAuthenticationRepository.Create(_, query, parameters);
        }

        public IEnumerable<LogFailedAuthentication> GetLogFailedAuthentications(DateTime? EntryDate) {
            string query = "EXEC [dbo].[SelectLogFailedAuthentications @EntryDate]";
            object parameters = new { EntryDate };
            return _logFailedAuthenticationRepository.GetAll(query,parameters).OrderByDescending(d => d.ID);
        }


        public IEnumerable<LogFailedAuthentication> GetLogFailedAuthenticationsAll()
        {
            return _logFailedAuthenticationRepository.GetAll();
        }

        #endregion


        # region LogDataChange
        public void AddLogDataChange(LogDataChange _)
        {
            string query = "EXEC [dbo].[InsertLogDataChange] @Account,@ComputerName,@IPAddress,@IDLogBrowserType,@IDLogOperatingSystemType,@IsMobileDevice";
            object parameters = new { _.IDEntryUser, _.EntryUser, _.Before, _.After, _.IDTable, _.ComputerName, _.IPAddress, _.IDLogBrowserType, _.IDLogOperatingSystemType, _.IsMobileDevice, _.IDLogDataChangeStatus };
            _logDataChangeRepository.Create(_, query, parameters);
        }

        public IEnumerable<LogDataChange> GetLogDataChanges(Func<LogDataChange, bool> criteria) => _logDataChangeRepository.ListByCriteria(criteria, "IdLogBrowserTypeNavigation", "IdLogDataChangeStatusNavigation", "IdLogOperatingSystemTypeNavigation").OrderByDescending(d => d.ID);

        #endregion


        #region LogUserActivity
        public void AddLogUserActivity(LogUserActivity _)
        {
            string query = "EXEC [dbo].[InsertLogUserActivity] @IDUser,@ComputerName,@IPAddress,@IDLogBrowserType,@IDLogOperatingSystemType,@IsMobileDevice, @IDLogUserActivityStatus, @IDModule, @IsPublic, @URL, @EntryUser";
            object parameters = new { _.IDUser, _.ComputerName, _.IPAddress, _.IDLogBrowserType, _.IDLogOperatingSystemType, _.IsMobileDevice, _.IDLogUserActivityStatus, _.IDModule, _.IsPublic, _.URL, _.EntryUser };
            _logUserActivityRepository.Create(_, query, parameters);
        }

        public IEnumerable<LogUserActivity> GetLogUserActivitiesByIdUser(int IDUser) {
            string query = "EXEC [dbo].[SelectLogUserActivitiesByIdUser] @IDUser";
            object parameters = new { IDUser };
            return _logUserActivityRepository.GetAll(query, parameters).OrderByDescending(d => d.ID);
        }
        public IEnumerable<LogUserActivity> GetLogUserActivities() {
            string query = "EXEC [dbo].[SelectLogUserActivities]";
            return _logUserActivityRepository.GetAll(query).OrderByDescending(d => d.ID);
        }
        public IEnumerable<LogUserAuthorizationStatus> GetLogUserAuthorizationStatus() {
            string query = "EXEC [dbo].[SelectLogUserAuthorizationStatus]";
            return _logUserAuthorizationStatusRepository.GetAll(query).OrderByDescending(d => d.ID);
        }
        public IEnumerable<LogUserActivityStatus> GetLogUserActivityStatus() {
            string query = "EXEC [dbo].[SelectLogUserActivityStatus]";
            return _logUserActivityStatusRepository.GetAll(query).OrderByDescending(d => d.ID);
        }

        public int GetTableByName(string TableName)
        {
            string query = "EXEC [dbo].[SelectTablesByTitle] @Title";
            object parameters = new { TableName };
            var table = _logTableRepository.GetAll(query, parameters).FirstOrDefault();

            if (table == null)
            {
                string insertQuery = "EXEC [dbo].[InsertTable] @Title";
                var model = _logTableRepository.Add(insertQuery,parameters);
                return model.ID;
            }
            return table.ID;
        }
        public IEnumerable<Tables> GetTables() {
            string query = "EXEC [dbo].[SelectTables]";
            return _logTableRepository.GetAll(query);
        }

        #endregion


    } 
}
