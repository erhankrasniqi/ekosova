﻿using eKosova.Domain.DynamicForms;
using eKosovaInternal.Domain.DynamicForms;
using eKosovaInternal.Domain.SeedWork;
using eKosovaInternal.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.DynamicForms
{
    public class DynamicFormsRepository : IDynamicFormsRepository
    {

        private readonly IRepository<DynamicForm> _dynamicFormRepository;
        private readonly IRepository<DynamicFormQuestions> _dynamicFormQuestionsRepository;
        private readonly IRepository<DynamicFormQuestionsOptions> _dynamicFormQuestionsOptionsRepository;
        private readonly IRepository<DynamicFormGridXQuestions> _dynamicFormQuestionsGridXRepository;
        private readonly IRepository<DynamicFormGridYQuestions> _dynamicFormQuestionsGridYRepository;
        private readonly IRepository<DynamicFormGridXY> _dynamicFormQuestionsGridXYRepository;

        public DynamicFormsRepository(IRepository<DynamicForm> dynamicFormRepository, IRepository<DynamicFormQuestions> dynamicFormQuestionsRepository, IRepository<DynamicFormQuestionsOptions> dynamicFormQuestionsOptionsRepository, IRepository<DynamicFormGridXQuestions> dynamicFormQuestionsGridXRepository, IRepository<DynamicFormGridYQuestions> dynamicFormQuestionsGridYRepository,
            IRepository<DynamicFormGridXY> dynamicFormQuestionsGridXYRepository)
        {
            _dynamicFormRepository = dynamicFormRepository;
            _dynamicFormQuestionsRepository = dynamicFormQuestionsRepository;
            _dynamicFormQuestionsOptionsRepository = dynamicFormQuestionsOptionsRepository;
            _dynamicFormQuestionsGridXRepository = dynamicFormQuestionsGridXRepository;
            _dynamicFormQuestionsGridYRepository = dynamicFormQuestionsGridYRepository;
            _dynamicFormQuestionsGridXYRepository = dynamicFormQuestionsGridXYRepository;
        }

        public async Task<int> AddDynamicForm(DynamicForm model)
        {
            string query = "EXEC [dbo].[InsertDynamicForm] @Title, @Description, @IsActive, @IsDeleted, @IDInternalUser, @IDServiceCategory";
            model.IDInternalUser = 5;
            object parameters = new { model.Title, model.Description, model.IsActive, model.IsDeleted, model.IDInternalUser, model.IDServiceCategory };
            var response = await _dynamicFormRepository.Create(model, query, parameters);
            return response.ID;
        }

        public async Task<int> AddQuestion(DynamicFormQuestions model)
        {
            string query = "EXEC [dbo].[InsertDynamicFormQuestion] @IDDynamicForm, @Title, @Description, @IsRequired, @Section, @IDDynamicFormQuestionType, @ShowQuestion, @HasAnswer, @IDDynamicFormQuestionParent, @LevelOfQuestion";
            object parameters = new { model.IDDynamicForm, model.Title, model.Description, model.IsRequired, model.Section, model.IDDynamicFormQuestionType, model.ShowQuestion, model.HasAnswer, model.IDDynamicFormQuestionParent, model.LevelOfQuestion };
            var response = await _dynamicFormQuestionsRepository.Create(model, query, parameters);
            return response.ID;
        }

        public async void AddQuestionOptions(DynamicFormQuestionsOptions model)
        {
            string query = "EXEC [dbo].[InsertDynamicFormQuestionOptions] @Title, @IDDynamicFormQuestion";
            object parameters = new {  model.Title, model.IDDynamicFormQuestion };
            await _dynamicFormQuestionsOptionsRepository.Create(model, query, parameters);
        }

        public async Task<int> AddDynamicFormQuestionGridX(DynamicFormGridXQuestions model)
        {
            string query = "EXEC [dbo].[InsertDynamicFormQuestionGridX] @Title";
            object parameters = new { model.Title };
            var response = await _dynamicFormQuestionsGridXRepository.Create(model, query, parameters);
            return response.ID;
        }

        public async Task<int> AddDynamicFormQuestionGridY(DynamicFormGridYQuestions model)
        {
            string query = "EXEC [dbo].[InsertDynamicFormQuestionGridY] @Title";
            object parameters = new { model.Title };
            var response = await _dynamicFormQuestionsGridYRepository.Create(model, query, parameters);
            return response.ID;
        }

        public async void AddDynamicFormGridXY(DynamicFormGridXY model)
        {
            string query = "EXEC [dbo].[InsertDynamicFormQuestionGridXY] @IDGrid_X_Questions, @IDGrid_Y_Questions, @IDDynamicFormQuestions";
            object parameters = new { model.IDGrid_X_Questions, model.IDGrid_Y_Questions, model.IDDynamicFormQuestions };
            await _dynamicFormQuestionsGridXYRepository.Create(model, query, parameters);
        }

        public async Task<IEnumerable<DynamicForm>> GetDynamicForms()
        {
            string query = "EXEC [dbo].[SelectDynamicForms]";
            return await _dynamicFormRepository.GetAllQuery(query);
        }

    }
}
