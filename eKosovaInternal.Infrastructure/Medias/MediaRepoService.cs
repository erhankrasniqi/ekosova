﻿using eKosovaInternal.Domain.Medias;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eKosovaInternal.Infrastructure.Medias
{
   public class MediaRepoService : IMediaRepoService
    {
        private readonly IRepository<Media> _mediaRepository;

        public MediaRepoService(IRepository<Media> mediaRepository)
        {
            _mediaRepository = mediaRepository;
        }

        void IMediaRepoService.AddMedia(Media _)
        {
            throw new NotImplementedException();
        }

        IEnumerable<Media> IMediaRepoService.GetActiveMedias()
        {
            var mediaList = _mediaRepository.GetAll(); //GetAll().Where(c=> c.Status == true)
            return mediaList;
        }

        IEnumerable<Media> IMediaRepoService.GetAllMedias()
        {
            var mediaList = _mediaRepository.GetAll(); 
            return mediaList;
        }

        Media IMediaRepoService.GetMediaById(int id)
        {
            throw new NotImplementedException();
        }

        void IMediaRepoService.UpdateMedia(Media _)
        {
            throw new NotImplementedException();
        }
    }
}
