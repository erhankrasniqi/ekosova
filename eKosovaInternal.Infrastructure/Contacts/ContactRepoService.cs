﻿using eKosovaInternal.Domain.Contacts;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.Contacts
{
    public class ContactRepoService : IContactRepoService
    {
        private readonly IRepository<Contact> _contactRepository;
        public ContactRepoService(IRepository<Contact> contactRepository)
        {
            _contactRepository = contactRepository;
        }
        public void AddContact(Contact _)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Contact> GetActiveContacts()
        {
            var contactList = _contactRepository.GetAll().Where(c => c.Status == false);
            return contactList;
        }

        public IEnumerable<Contact> GetAllContacts()
        {
            var contactList = _contactRepository.GetAll();
            return contactList;
        }

        public Task<Contact> GetContactById(int ID)
        {
            string query = "EXEC [dbo].[SelectContactById] @ID";
            object param = new { ID };
            return _contactRepository.Read(query, param);

        }

        public Contact UpdateContact(Contact _)
        {
            string query = "EXEC [dbo].[UpdateContact] @ID, @Answer, @IDInternalUser";
            object param = new { _.ID, _.Answer, _.IDInternalUser };
            return _contactRepository.Update(query, param);
        }
    }
}
