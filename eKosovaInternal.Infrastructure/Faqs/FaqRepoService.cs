﻿using eKosovaInternal.Domain.Faqs;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.Faqs
{
   public class FaqRepoService : IFaqRepoService
    {
        private readonly IRepository<Faq> _faqRepository;

        public FaqRepoService(IRepository<Faq> faqRepository)
        {
            _faqRepository = faqRepository;
        }

        public void AddFaq(Faq _)
        {
            //throw new NotImplementedException();

            string query = "EXEC [dbo].[InsertFaq] @Question, @Answer, @IDInternalUser, @MediaVideo, @MediaVoice, @MediaDescription";
            object parameters = new { _.Question, _.Answer, _.IDInternalUser, _.MediaVideo, _.MediaVoice, _.MediaDescription};
            _faqRepository.Create(_, query, parameters);
        }

        public void DeleteFaq(int id)
        {
            string query = "EXEC [dbo].[DeleteFaq] @ID";
            object parameters = new { id };
            _faqRepository.Update(query, parameters);
        }
        //public void DeleteFaq(int id)
        //{
        //    string query = "EXEC [dbo].[DeleteFaq] @ID";
        //    object parameters = new { @id };
        //    _faqRepository.Update(query, parameters);
        //}

        public IEnumerable<Faq> GetActiveFaqs()
        {
            var faqList = _faqRepository.GetAll();
            return faqList;
        }

        public IEnumerable<Faq> GetAllFaqs()
        {
            var faqList = _faqRepository.GetAll();
            return faqList;
        }

        public Faq GetFaqByID(int id)
        {
            throw new NotImplementedException();
        }

		public void UpdateFaq(UpdateFaqModel _)
		{
			string query = "EXEC [dbo].[UpdateFaq] @ID, @Question, @Answer, @IDInternalUser";
			object parameters = new { _.ID, _.Question, _.Answer, IDInternalUser = 5 };
			_faqRepository.Update(query, parameters);
		}
	}
}
