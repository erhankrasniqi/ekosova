﻿using eKosovaInternal.Domain.PublicUsers;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.PublicUsers
{
    public class PublicUsersRepoService : IPublicUsersRepoService
    {
        private readonly IRepository<eKosovaInternal.Domain.PublicUsers.PublicUser> _publicusersRepository;

        public PublicUsersRepoService(IRepository<eKosovaInternal.Domain.PublicUsers.PublicUser> publicusersRepository)
        {
            _publicusersRepository = publicusersRepository;
        }

        public void AddPublicUser(PublicUser _)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PublicUser> GetActivePublicUser()
        {
            var usersList = _publicusersRepository.GetAll(); //.Where(c => c.ID == 73);
            return usersList;
        }

        public IEnumerable<PublicUser> GetAllPublicUser()
        {
            var query = "EXEC [dbo].[SelectApplications]"; 
            return _publicusersRepository.GetAll(query); 

            //var usersList = _publicusersRepository.GetAll();
            //    //.Where(c => c.IsVerified == null && c.IsActive == true);
            //    return usersList;

        }

        public async Task<PublicUser> GetPublicUserByPersonalNumber(string identification)
        {
            var query = "EXEC [dbo].[SelectPublicUserPersonalNumber] @PersonalNumber";
            object parameters = new
            {
                PersonalNumber = identification
            };
            return await _publicusersRepository.Read(query, parameters);
        }

        async  Task<PublicUser> IPublicUsersRepoService.GetPublicUserById(int id)
        {
            var query = "EXEC [dbo].[[SelectUserbyid] @ID";
            object parameters = new
            {
                ID = id
            };
            return await _publicusersRepository.Read(query, parameters);
        }



        //PublicUser IPublicUsersRepoService.UpdatePublicUser(PublicUser _)
        //{
        //    string query = "EXEC [dbo].[UbdateAndVerifyApplications]  @ID, @IDFront, @IDBack, @IDSelfi";
        //    object parameters = new { _.ID, _.PathIDFront, _.PathIDBack, _.PathSelfie };
        //    return _publicusersRepository.Update(query, parameters);
        //}

        void IPublicUsersRepoService.UpdatePublicUser(UpdatePublicUser _)
        {
            string query = "EXEC [dbo].[UbdateAndVerifyApplications]  @ID, @IDFront, @IDBack, @IDSelfie, @Reason";
            object parameters = new { _.ID, _.IDFront, _.IDBack, _.IDSelfie, _.Reason};
            _publicusersRepository.Update(query, parameters);
        }
    }
}
