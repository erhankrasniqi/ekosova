﻿using eKosovaInternal.Domain.Helps;
using eKosovaInternal.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eKosovaInternal.Infrastructure.Helps
{
    public class HelpRepoService : IHelpRepoService
    {
        private readonly IRepository<Help> _helpRepository;
        public HelpRepoService(IRepository<Help> helpRepository)
        {
            _helpRepository = helpRepository;
        }
        public void AddHelp(Help _)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Help> GetActiveHelps()
        {
            var helpList = _helpRepository.GetAll().Where(c => c.Status == false);
            return helpList;
        }

        public IEnumerable<Help> GetAllHelps()
        {
            var helpList = _helpRepository.GetAll();
            return helpList;
        }

        public Help GetHelpById(int id)
        {
            throw new NotImplementedException();
        }

        public Help UpdateHelp(Help _)
        {
            string query = "EXEC [dbo].[UpdateHelp] @ID, @Answer, @IDInternalUser";
            object parameters = new { _.ID, _.Answer, _.IDInternalUser };
            return _helpRepository.Update(query, parameters);
        }

    }
}
