﻿using eKosovaInternal.Domain.Emails;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace eKosovaInternal.Infrastructure.Emails
{
    public class EmailSender : IEmailSender
    {
        public async Task SendEmailAsync(MailRequest emailInfo)
        {
            await Execute(emailInfo);
        }

        public async Task<string> Execute(MailRequest emailInfo)
        {
            //var settings = _settingsDictonaryRepository.GetSettingsDictionariesByKeyAndType(1, null).ToList();

            var emailSettings = new EmailSettings
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EmailUsername = "ekosova.test@gmail.com",
                EmailPassword = "eKosova.135",
                FromEmail = "ekosova.test@gmail.com"
            };

            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(emailSettings.FromEmail);
            email.To.Add(MailboxAddress.Parse(emailInfo.ToEmail));
            email.Subject = emailInfo.Subject;    

            email.Body = new TextPart("plain")
            {
                Text = emailInfo.Body
            };

            try
            {

                using (var smtp = new SmtpClient())
                {
                    smtp.Connect(emailSettings.Host, emailSettings.Port);
                    smtp.AuthenticationMechanisms.Remove("XOAUTH2");
                    smtp.Authenticate(emailSettings.FromEmail, emailSettings.EmailPassword);
                    //smtp.EnableSsl = true;
                    await smtp.SendAsync(email);
                    smtp.Disconnect(true);
                }

                //_saveLog.LogInformation("Email with subject \"" + emailInfo.Subject + "\" on date " + DateTime.Now.ToString(@"dd.MM.yyyy HH:mm:ss"));

            }
            catch (Exception ex)
            {
                //_saveLog.LogError(ex, "Failed to sent eamil!");
                return ex.Message;
            }

            return null;
        }

    }
}
