﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eKosovaInternal.Infrastructure.Emails
{
    public class EmailSettings
    {
        public string EmailUsername { get; set; }
        public string EmailPassword { get; set; }
        public string FromEmail { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
